#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cstdio>
#include <cmath>
#include <vector>
#include <random>
#include <complex>
#include <bitset>
#include <ctime>
#include <boost/any.hpp>

#include "./class/statMod.h"
#include "./class/network.h"

const char * argv0; /**< The program name */

/* ###### protoype function ###### */
void printVector(std::vector<auto> v, std::string = "");
void printVector(std::vector<auto> v, std::string s);
void printMatrix(std::vector<auto> v);
int mc_cal(size_t nbrOfSamples, \
           std::vector<Tensor>                   Tlist, \
           std::vector< std::vector<size_t> >    net, \
           std::vector<size_t>                   drawingOrder, \
           std::vector<size_t>                   fromWhichTensor, \
           std::vector<char>                     drawingType, \
           std::vector<size_t>                   tracingID);

/* ############################### */


int main(int argc, char **argv) {
        argv0 = argv[0];  // Set Program Name
        int c;
        int nmode = 0;
        size_t nbrOfSamples;
        double samplefraction = 1;

        while ((c = getopt(argc, argv, "N:n:")) != -1) {
                switch (c) {
                case 'n':   // caseinsensitiv mode
                        if (nmode == 0) {
                                nmode = 1;
                        } else {
                                exit(EXIT_FAILURE);
                        }
                        if (optarg == NULL) exit(EXIT_FAILURE);
                        nbrOfSamples = strtol(optarg, NULL, 0);

                        break;
                case 'N':   // specify output File
                        if (nmode == 0) {
                                nmode = 2;
                        } else {
                                exit(EXIT_FAILURE);
                        }
                        if (optarg == NULL) exit(EXIT_FAILURE);
                        samplefraction = strtol(optarg, NULL, 0);
                        break;
                case '?': // Error on wrong Arguments
                        break;
                default:   // Error on wrong Arguments
                        break;
                }
        }

        if ((argc-optind) == 0) {  // no more arguments supplied
                exit(EXIT_FAILURE);
        }

        std::string conffile = argv[optind];

        std::vector<Tensor> Tlist;
        std::string filename;
        Tlist.push_back(Tensor("C1", filename, 3, false));
        Tlist.push_back(Tensor("C2", filename, 3, true));
        Tlist.push_back(Tensor("C3", filename, 3, false));
        Tlist.push_back(Tensor("C4", filename, 3, true));

        std::vector< std::vector<size_t> > net;  // 3  CONJG( C(f,a,i))


        std::vector<size_t> drawingOrder;
        std::vector<size_t> fromWhichTensor;
        std::vector<char> drawingType;
        std::vector<size_t> tracingID;

        mc_cal(534l*534l*64l*64l, Tlist, net, drawingOrder, fromWhichTensor, drawingType, tracingID);
}


void printVector(std::vector<auto> v, std::string s) {
        if (s != "") {
                std::cout << s << ":  ";
        }
        std::cout << "{ ";
        for (size_t i = 0; i < v.size(); i++) {
                std::cout << v.at(i) << ' ';
        }
        std::cout << "}\n";
}

void printMatrix(std::vector<auto> v) {
        for (size_t i = 0; i < v.size(); i++) {
                printVector(v.at(i));
        }
        std::cout << '\n';
}

int mc_cal(size_t nbrOfSamples, \
           std::vector<Tensor>                   Tlist, \
           std::vector< std::vector<size_t> >    net, \
           std::vector<size_t>                   drawingOrder, \
           std::vector<size_t>                   fromWhichTensor, \
           std::vector<char>                     drawingType, \
           std::vector<size_t>                   tracingID) {
        StatMod stat;


        TensorNet Tnet(net, Tlist);


        printVector(drawingOrder, "drawingOrder   ");
        printVector(fromWhichTensor, "fromWhichTensor");
        printVector(drawingType, "drawingType    ");
        printVector(tracingID, "tracingID      ");

        std::cout << "nbrOfSamples = " << nbrOfSamples << '\n';
        int howManyOut = 100;
        int divider = nbrOfSamples/howManyOut;
        for (size_t i = 0; i < nbrOfSamples; i++) {
                std::complex<double> temp = Tnet.sampleTNet(drawingOrder, fromWhichTensor, drawingType, tracingID);
                stat.addOpt(temp);

                if ( ((i+1) % (divider) == 0 && i != 0) || i == 1 ) {
                        std::cout << (static_cast<int> ((i+1) / (divider))*(100/howManyOut)) << "%: ";
                        stat.mean_varOpt();
                        stat.var_of_varOpt();
                }
        }

        /*
              if (Tnet.maxi > 1000000000) {
                      Tnet.calcVal();
              }
         */
        return 0;
}
