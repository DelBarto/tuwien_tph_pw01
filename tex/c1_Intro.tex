\chapter{Introduction}
\label{ch:intro}

The Hatree--Fock (HF) approximation yields a set of orthonormal spin orbitals $\braket{\mathbf{x} | p} = \psi_{p}(\mathbf{x})$ and the corresponding eigenvalues $\epsilon_{p}$ with $p \in \mathbb{N}$.
The $N_{\mathrm{o}}$ spin orbitals with the lowest eigenvalue are called \emph{occupied} orbitals, where $N_{\mathrm{o}}$ refers to the number of electrons present in the system.
The rest is called \emph{virtual} or \emph{unoccupied} orbitals.
We will label occupied orbitals with $i,j$ and virtual orbitals with $a,b$.
In principle there is an infinite number of virtual orbitals however in practice one only uses a finite number $N_{\mathrm{v}}$.
This approximation covers roughly 98\% of total energy.
The remaining part is called correlation energy and often plays a crucial role when considering the difference of the total energies for stability calculations.
The correlation energy can be calculated in various ways.
One of these ways is the second-order M{\o}ller--Plesset (MP2) perturbation theory.
For closed shell systems the MP2 correlation energy reads

%\begin{equation}
%E^{\mathrm{MP2}} = -\dfrac{1}{\Omega_k^4} \int \!\!\!\!\! \int \!\!\!\!\! \int \!\!\!\!\! \int_{\Omega_k} d\mathbf{k}_{i} \, d\mathbf{k}_{j} \, d\mathbf{k}_{a} \, d\mathbf{k}_{b} \, \sum_{i,j,a,b} \dfrac{\left. \tilde{V} \right.^{a b}_{i j} \left( 2 \left. \tilde{V}^{*} \right.^{ij}_{ab} - \left. \tilde{V}^{*} \right.^{ij}_{ba} \right) }{\epsilon_{a} + \epsilon_{b} - \epsilon_{i} - \epsilon_{j}}
%\label{eq:Emp2}
%\end{equation}
%\dfrac{\tilde{V}^{a b}_{\ i j} \left(2 \tilde{V}^{ab}_{ij} - \tilde{V}^{ba}_{ij}}{\epsilon_{a} + \epsilon_{b} - \epsilon_{i} - \epsilon_{j}}
%with $\Omega_k$ the volume of the first Brillouin zone and the Coulomb integral.

\begin{equation}
	E^{\mathrm{MP2}} = \sum_{ijab} \dfrac{\left. \tilde{V} \right.^{ab}_{ij} \left( 2 \left. \tilde{V} \right.^{ij}_{ab} - \left. \tilde{V} \right.^{ji}_{ab} \right)}{-\Delta^{ab}_{ij}}
\label{eq:Emp2}
\end{equation}

with $\Delta^{ab}_{ij} = \epsilon_{a} + \epsilon_{b} - \epsilon_{i} - \epsilon_{j}$ ,  $ \sum_{ijab} =  \sum_{i,j=1}^{N_{\mathrm{o}}}  \sum_{ab=N_{\mathrm{o}}+1}^{N_{\mathrm{o}}+N_{\mathrm{v}}}$ and the two-electron integral

\begin{equation}
\left. \tilde{V} \right.^{ab}_{ij}  = \int d\mathbf{x'} \, d\mathbf{x} \dfrac{\braket{i | \mathbf{x}} \braket{\mathbf{x} | a} \braket{j | \mathbf{x'}}\braket{\mathbf{x'} | b} }{\lvert \mathbf{r} - \mathbf{r'} \rvert} ,
\label{eq:Vabij}
\end{equation}
with $\mathbf{x} = (\sigma, \mathbf{r})$.
$\sigma$ represents the spin orientation $\{\uparrow,\downarrow\}$, $\mathbf{r}$ the spatial coordinate and $\int d\mathbf{x} = \sum_{\sigma} \int d\mathbf{r}$.
The second-order energy of Eq.~\eqref{eq:Emp2} can be obtained from the second-order \emph{Hugenholtz diagram}, shown in Figure~\ref{fig:H_ord2}, where a dot represents the \emph{antisymmetrized} two-electron interaction $\bra{ij}\ket{ab} = \left.\tilde{V}\right.^{ab}_{ij} - \left.\tilde{V}\right.^{ba}_{ij}$ and reads $E^{\mathrm{MP2}} = \frac{1}{4} \sum_{i,j = 1}^{2N_{0}} \sum_{a,b=2(N_{0}+1)}^{2(N_{\mathrm{o}}+N_{\mathrm{v}})} \frac{\bra{ij}\ket{ab}\bra{ab}\ket{ij}}{-\Delta^{ab}_{ij}}$.
How this is done is further discussed in the Appendix~\ref{app:HugenholtzDiag} and only mentioned here to better understand the relation to the final diagrams, called \emph{tensor networks}.


\begin{figure}
	\centering

	\begin{tikzpicture}[-,>=stealth',shorten >=0pt,auto,node distance=3cm,
	                    semithick]
		  \tikzstyle{every state}=[fill=none,draw=black,rectangle,text=black]

				\node[fill=black,regular polygon, regular polygon sides=3,inner sep=0.8pt] at (-0.823cm,1.5cm) {};
				\node[fill=black,regular polygon, regular polygon sides=3,inner sep=0.8pt, rotate=180] at (-0.502cm,1.5cm) {};
				\node[fill=black,regular polygon, regular polygon sides=3,inner sep=0.8pt, rotate=180] at (0.502cm,1.5cm) {};
				\node[fill=black,regular polygon, regular polygon sides=3,inner sep=0.8pt] at (0.823cm,1.5cm) {};

			  	\path 	(0,0)	edge		[bend right=35]		node				{$i$}	(0,3)
			  			(0,3)	edge		[bend right=35]		node				{$j$}	(0,0)
			  			(0,3)	edge		[bend left=70]		node				{$a$}	(0,0)
			  			(0,0)	edge		[bend left=70]		node				{$b$}	(0,3);

				\filldraw
						(0,3) circle (2pt)
						(0,0) circle (2pt);
	\end{tikzpicture}

\caption{Hugenholtz diagram of MP2}
\label{fig:H_ord2}
\end{figure}





Expanding Equation~\eqref{eq:Emp2} as in
\begin{align}
E^{\mathrm{MP2}} &= 2 \sum_{ijab} \dfrac{\left. \tilde{V} \right.^{ab}_{ij} \left. \tilde{V} \right.^{ij}_{ab}}{-\Delta^{ab}_{ij}} - \sum_{ijab} \dfrac{\left. \tilde{V} \right.^{ab}_{ij} \left. \tilde{V} \right.^{ji}_{ab} }{-\Delta^{ab}_{ij}}
&= E^{\mathrm{MP2,1}} - E^{\mathrm{MP2,2}}
\label{eq:Emp2exp}
\end{align}
will result in two terms, where the first term is called \emph{direct} term and second one \emph{exchange} term.
These two terms can also be visualized by transforming the Hugenholtz diagram into \emph{Goldstone diagrams}, where the two-electron interaction is now represented by a dashed line, called \emph{interaction line}.
This transformation can be seen as ``pulling apart'' the dot so that there is only one particle (virtual) and hole (occupied) state left on each end of the interaction line.
Since there are two possible ways to do so we are left with two second-order Goldstone diagrams which correspond to the direct and exchange term.
One can also obtain the second-order energy from the Goldstone diagram which we discuss in Appendix~\ref{app:GoldstoneDiag}.

\begin{figure}
	\centering
	\begin{subfigure}{0.47\textwidth}
		\centering
		\begin{tikzpicture}[-,>=stealth',shorten >=0pt,auto,node distance=3cm,
		                    semithick]
			\tikzstyle{every state}=[fill=none,draw=black,rectangle,text=black]

			\node[fill=black,regular polygon, regular polygon sides=3,inner sep=0.8pt] at (-0.44cm,1.5cm) {};
			\node[fill=black,regular polygon, regular polygon sides=3,inner sep=0.8pt, rotate=180] at (0.44cm,1.5cm) {};
			\node[fill=black,regular polygon, regular polygon sides=3,inner sep=0.8pt] at (3.44cm,1.5cm) {};
			\node[fill=black,regular polygon, regular polygon sides=3,inner sep=0.8pt, rotate=180] at (2.56cm,1.5cm) {};

		  	\path	(0,3)	edge		[bend left]	node		{$i$}	(0,0)
		  			(0,0)	edge		[bend left]	node		{$a$}	(0,3)
		  			(3,3)	edge		[bend left]	node		{$b$}	(3,0)
		  			(3,0)	edge		[bend left]	node		{$j$}	(3,3);

		  	\draw 	[dashed] (0,0) -- (3,0)
					[dashed] (0,3) -- (3,3);

		\end{tikzpicture}
		\caption{direct term}
	\end{subfigure}
	\begin{subfigure}{0.47\textwidth}
		\centering
		\begin{tikzpicture}[-,>=stealth',shorten >=0pt,auto,node distance=3cm,
		                    semithick]
			\tikzstyle{every state}=[fill=none,draw=black,rectangle,text=black]

			\node[fill=black,regular polygon, regular polygon sides=3,inner sep=0.8pt] at (-0.44cm,1.5cm) {};
			\node[fill=black,regular polygon, regular polygon sides=3,inner sep=0.8pt] at (3.44cm,1.5cm) {};

			\node[fill=black,regular polygon, regular polygon sides=3,inner sep=0.8pt, rotate=-135] at (2.5cm,0.5cm) {};
			\node[fill=black,regular polygon, regular polygon sides=3,inner sep=0.8pt, rotate=135] at (0.5cm,0.5cm) {};

		  	\path 	(0,3)	edge					node		[pos=0.80]	{$j$}	(3,0)
		  			(0,0)	edge		[bend left]	node					{$a$}	(0,3)
		  			(3,3)	edge		[bend left]	node					{$b$}	(3,0)
		  			(0,0)	edge					node		[pos=0.20]	{$i$}	(3,3);

		  	\draw 	[dashed] (0,0) -- (3,0)
					[dashed] (0,3) -- (3,3);

		\end{tikzpicture}
		\caption{exchange term}
	\end{subfigure}
\caption{All second order Goldstone diagrams in M{\o}ller--Plesset perturbation theory.}
\label{fig:G_ord2}
\end{figure}


Writing the function $\frac{1}{x}$ as a Laplace transform of the following form,
\begin{align*}
\dfrac{1}{ - \Delta^{ab}_{ij} } &= -\int_0^\infty d\tau e^{ {-\Delta^{ab}_{ij}} \, \tau } \\
& \approx -\sum_{n} w_{n} e^{ {-\Delta^{ab}_{ij} } \, \tau_n}
\end{align*}
helps to linearize the Goldstone diagram. This integral is well behaved and can therefore be numerically well approximated using only a few quadrature points $\tau_{n}$. This lets us cast the correlation energy into
\begin{equation}
	E^{\mathrm{MP2}} = \sum_{ijab} \sum_{n} w_{n} e^{- \Delta^{ab}_{ij} \tau_{n}} \left. \tilde{V} \right.^{ab}_{ij} \left( 2 \left. \tilde{V} \right.^{ij}_{ab} - \left. \tilde{V} \right.^{ji}_{ab} \right)\text{.}
\label{eq:Emp2n}
\end{equation}


Since these tensors are fairly large, $\tilde{V}$ is often approximately factorized into two significantly smaller tensors by using a Cholesky decomposition. One way of decomposition is of the form $\left. \tilde{V} \right.^{ab}_{ij} \approx \sum_{g} \left. \tilde{\Gamma}^{*} \right.^{ag}_{i} \left. \tilde{\Gamma} \right. ^{b}_{jg}$ \cite{doi:10.1063/1.4977994}, resulting in

\begin{align}
	E^{\mathrm{MP2,1}}
			= 2 \sum_{ijab} \sum_{g,f} \sum_{n} w_{n} e^{-\Delta^{ab}_{ij} \tau_{n}}
						\left. \tilde{\Gamma}^{*} \right.^{ag}_{i}
						\left. \tilde{\Gamma}     \right.^{b}_{jg}
						\left. \tilde{\Gamma}^{*} \right.^{if}_{a}
						\left. \tilde{\Gamma}     \right.^{j}_{bf}
\label{eq:Emp21_tildegamma}
\end{align}
and
\begin{align}
	E^{\mathrm{MP2,2}}
			= \sum_{ijab} \sum_{g,f} \sum_{n} w_{n} e^{- \Delta^{ab}_{ij} \tau_{n}}
						\left. \tilde{\Gamma}^{*} \right.^{ag}_{i}
						\left. \tilde{\Gamma}     \right.^{b}_{jg}
						\left. \tilde{\Gamma}^{*} \right.^{jf}_{a}
						\left. \tilde{\Gamma}     \right.^{i}_{bf}
			\text{.}
\label{eq:Emp22_tildegamma}
\end{align}
Using Equation~\eqref{eq:Emp21_tildegamma}~and~\eqref{eq:Emp22_tildegamma} we can draw a linear diagram, for the direct term of MP2:

\begin{center}
\begin{tikzpicture}[-,>=stealth',shorten >=0pt,auto,node distance=5cm,
                    semithick]
	\tikzstyle{every state}=[fill=none,draw=black,rectangle,text=black]

	\node[state] (A)		at 	(0,0)	{$\left. \tilde{\Gamma}^{*} \right.^{ag}_{i}$};
	\node[state] (B)		at	(5,0)	{$\left. \tilde{\Gamma}     \right.^{b}_{jg} $};
	\node[state] (C) 	at 	(0,5)	{$\left. \tilde{\Gamma}^{*} \right.^{if}_{a}$};
	\node[state] (D) 	at	(5,5)	{$\left. \tilde{\Gamma}     \right.^{j}_{bf}$};
	\node[state] (NI)	at	(-3,3.5)	{$e^{\tau_{n}\epsilon_{i}}$};
	\node[state] (NA)	at	(-2,2)	{$e^{-\tau_{n}\epsilon_{a}}$};
	\node[state] (NJ)	at	(2,3.5)	{$e^{\tau_{n}\epsilon_{j}}$};
	\node[state] (NB)	at	(3,2)	{$e^{-\tau_{n}\epsilon_{b}}$};
	\node[state] (WN)	at	(-4,1)	{$w_{n}$};

  	\path	(A)		edge 				node 			{$g$} 	(B)
      	    			edge [bend right] 	node	 [pos=0.4]	{$a$} 	(C)
       	     		edge [bend left] 	node [pos=0.6] 	{$i$} 	(C)
       	 	(B) 		edge [bend right]	node [pos=0.4]	{$b$} 	(D)
	   	    	 		edge [bend left]		node	 [pos=0.6]	{$j$} 	(D)
       	 	(C) 		edge 			    	node 			{$f$} 	(D)
       	 	(NI)		edge 				node 			{}		(-0.73,3.5)
       	 	(NA)		edge 				node 			{}		(0.84,2)
       	 	(NJ)		edge 				node 			{}		(4.3,3.5)
       	 	(NB)		edge 				node 			{}		(5.85,2)
       	 	(NI)		edge 				node 			{}		(-3,1)
       	 	(NA)		edge 				node 			{}		(-2,1)
       	 	(NJ)		edge 				node 			{}		(2,1)
       	 	(NB)		edge 				node 			{}		(3,1)
       	 	(3,1)	edge 				node [pos=0.85]	{$n$}	(WN);

	\filldraw
		(-0.73,3.5) circle (2pt)
		(0.84,2) circle (2pt)
		(4.26,3.5) circle (2pt)
		(5.85,2) circle (2pt)
		(-3,1) circle (2pt)
       	(-2,1) circle (2pt)
       	(2,1) circle (2pt);

\end{tikzpicture}
\end{center}


To simplify our diagram we can distribute the weights from the numerical quadrature equally, as in
\begin{align}
\left(\left. V \right. ^{ab}_{ij}\right)_{n} = w^{\frac{1}{2}}_{n} e^{\frac{1}{2} \Delta^{ab}_{ij} \tau_{n}} \left.\tilde{V} \right. ^{ab}_{ij}
\qquad \text{or} \qquad
\left(\left. \Gamma \right. ^{j}_{bf}\right)_{n} = w^{\frac{1}{4}}_{n} e^{-\frac{1}{2} (\epsilon_{a} - \epsilon_{i}) \tau_{n}} \left.\tilde{\Gamma} \right. ^{j}_{bf} \text{.}
\end{align}
For now, we are only interested in a specific $n$ and we will further omit the index, which results in
\begin{align}
	E^{\mathrm{MP2,1}} = 2 \sum_{ijab} \left.V\right.^{ab}_{ij} \left.V\right.^{ij}_{ab}
						= 2 \sum_{ijab} \sum_{g,f}
								\left. \Gamma^{*} \right.^{ag}_{i}
								\left. \Gamma     \right.^{b}_{jg}
								\left. \Gamma^{*} \right.^{if}_{a}
								\left. \Gamma     \right.^{j}_{bf}
\label{eq:Emp21_gamma}
\end{align}
\begin{align}
	E^{\mathrm{MP2,2}} 	= \sum_{ijab} \left.V\right.^{ab}_{ij} \left.V^{*}\right.^{ij}_{ba}
						= \sum_{ijab} \sum_{g,f}
								\left. \Gamma^{*} \right.^{ag}_{i}
								\left. \Gamma     \right.^{b}_{jg}
								\left. \Gamma^{*} \right.^{jf}_{a}
								\left. \Gamma     \right.^{i}_{bf}
\label{eq:Emp22_gamma}
\end{align}
Its corresponding diagrams, are so called \emph{tensor networks} or \emph{wiring diagrams}.


\begin{figure}
	\centering
	\begin{subfigure}{0.47\textwidth}
		\centering
		\begin{tikzpicture}[-,>=stealth',shorten >=1pt,auto,node distance=3cm,
		                    semithick]
		  \tikzstyle{every state}=[fill=none,draw=black,rectangle,text=black]

		  \node[state] (A)	[]					{$\left. \Gamma^{*} \right.^{if}_{a} $};
		  \node[state] (B)	[right of=A] 		{$\left. \Gamma     \right.^{j}_{bf} $};
		  \node[state] (C) 	[below of=A]		 	{$\left. \Gamma^{*} \right.^{ag}_{i} $};
		  \node[state] (D) 	[right of=C] 		{$\left. \Gamma     \right.^{b}_{jg} $};

		  \path (A) edge 				node {$f$} (B)
		            edge [bend right] 	node {$a$} (C)
		            edge [bend left] 	node {$i$} (C)
		        (B) edge [bend right]	node {$b$} (D)
			        edge [bend left]		node {$j$} (D)
		        (C) edge 			    	node {$g$} (D);
		\end{tikzpicture}
		\caption{$E^{\mathrm{MP2,1}}$}
	\end{subfigure}
	\begin{subfigure}{0.47\textwidth}
		\centering
		\begin{tikzpicture}[-,>=stealth',shorten >=1pt,auto,node distance=3cm,
		                    semithick]
		  \tikzstyle{every state}=[fill=none,draw=black,rectangle,text=black]

		  \node[state] (A)	[]					{$\left. \Gamma^{*} \right.^{jf}_{a}$};
		  \node[state] (B)	[right of=A] 		{$\left. \Gamma     \right.^{i}_{bf}$};
		  \node[state] (C) 	[below of=A]		 	{$\left. \Gamma^{*} \right.^{ag}_{i}$};
		  \node[state] (D) 	[right of=C] 		{$\left. \Gamma     \right.^{b}_{jg}$};

		  \path (A) edge 				node {$g$} (B)
		            edge [bend right] 	node {$a$} (C)
		            edge 			 	node [pos=0.8] {$j$} (D)
		        (B) edge [bend left]		node {$b$} (D)
		        (C) edge 			    	node {$f$} (D)
			        edge 				node [pos=0.2] {$i$} (B);
		\end{tikzpicture}
		\caption{$E^{\mathrm{MP2,2}}$}
	\end{subfigure}

\caption{tensor network representations of Equations \eqref{eq:Emp21_gamma} and \eqref{eq:Emp22_gamma}}
\label{fig:tennet}
\end{figure}
