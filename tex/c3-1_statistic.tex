\section{Statistics}

In this section we briefly discuss the statistical methods used to analyze
the results and the statistical test used to verify them.

\subsection{Interval estimation}
\label{subsec:interval_est}

From the $M$ sampled values of the stochastic function a confidence interval
is estimated, containing the true contraction value with a probability of
at least $99\%$.

For an estimator of the mean and standard deviation the law of large numbers
can be used, which states that the sample average of a repeated experiment
where each realization is independent is normal distributed. \cite{book:11854}
The mean is estimated by the sample mean $\braket{X}$ and
the variance $\sigma_{X}^{2}$ is estimated by the sample variance $S_X$.
For large $M$ Student's $t$-distribution $t_{n-1;0.995}$
approximates the normal distribution well enough so that their quantiles
are approximately equivalent.
Requiring a coverage probability of $0.99$ that the true contraction value lies
within the estimated interval, we use the $0.995$ quantile, of
Eq.~\eqref{eq:intervalEstimation} which is
$t_{\infty, 0.995} = z_{0.995} = 2.576$. \cite{AC08993515}
The conffidence intervall is given by:

\begin{align}
\left[ \braket{ X } - t _ { M - 1 ; 0.995 } \sqrt { \frac { S _ { X } ^ { 2 } } { M } } , \braket{ X } + t _ { M - 1 ; 0.995 } \sqrt { \frac { S _ { X } ^ { 2 } } { M } } \right]
\label{eq:intervalEstimation}
\end{align}
%
% \begin{align}
% \left[ \braket{ X } - t _ { M - 1 ; \frac { 1 + \gamma } { 2 } } \sqrt { \frac { S _ { X } ^ { 2 } } { M } } , \braket{ X } + t _ { M - 1 ; \frac { 1 + \gamma } { 2 } } \sqrt { \frac { S _ { X } ^ { 2 } } { M } } \right]
% \label{eq:intervalEstimation}
% \end{align}

%To make an estimation of the sample variance
%$$\left[ \frac { ( n - 1 ) S _ { n } ^ { 2 } } { \chi _ { n - 1 ; \frac { 1 + \gamma } { 2 } } ^ { 2 } } , \frac { ( n - 1 ) S _ { n } ^ { 2 } } { \chi _ { n - 1 ; \frac { 1 - \gamma } { 2 } } ^ { 2 } } \right]$$
%is used, where as ${ \chi _ { n - 1 ; \frac { 1 - \gamma } { 2 } } ^ { 2 } }$ referes to the $\chi^{2}$-distribution.
%
%
%distribution of variance
%$$ \mu^{2} = |\mu|^{2} = \mu \mu^{*}$$
%    $$ \mu^{4} = \dfrac{1}{M} \sum^{M}_{i}   \left(
%    					|x_{i}|^{4} -
%    					4 |x_{i}|^{2} \Re \left \{ x_{i} \mu^{*} \right \}+
%    					4|x_{i}|^{2} |\mu|^{2} +
%					2 \Re \left \{ x_{i}^{2} \left. \mu^{*} \right.^{2} \right \} -
%    					3 |\mu|^{4} \right) $$
%    $$\mathbb{V}\left[ S^{2} \right] = \dfrac{1}{M} \left( \mu^4 - (S^2)^2 \right)$$



\subsection{Sample Variance Distribution}

The variance $\sigma_{X}^{2}$ is usually unknown as it takes to long
to calculate deterministically.
Instead we are using the sample variance $S_{X}^{2}$
to estimate the variance $\sigma_X^2$.

\begin{align}
\mathbb{V}(X) = S_{X}^{2} = \frac{M}{M-1} \underbrace{\left(\frac{1}{M} \sum_{i=1}^{M} |X_{i}-\braket{X} |^{2}\right)}_{\text{biased sample variance}} = \frac{1}{M-1} \sum_{i=1}^{M}\left|X_{i}-\braket{X}\right|^{2}
\label{eq:sampleVarianceDistribution}
\end{align}
The prefactor $\frac{M}{M-1}$ is the correction term to turn a
biased estimation into an unbiased estimation of the variance $S_{X}^{2}$
and is called \emph{Bessel's correction}.
This correction is due to the remaining degrees of freedom,
since the mean is unknown as well and used to determine the sample variance.
\cite{ADictionaryofStatistics}

Since the employed sample sizes $M \sim 10^{9}$  will be fairly large,
we can neglected the Bessel's correction in good conscience.
A similar transformation as in Eq.~\eqref{eq:var} can be applied to
the sample variance  of a complex observable, here $X$, as follows.
\begin{multline}
S_{X}^{2} = \frac{1}{M} \sum_{i=1}^{M}\left( |X_{i}|^{2} - X_{i}^{*} \braket{X} - X_{i} \braket{X}^{*} + |\braket{X}|^{2} \right) \\
	= \frac{1}{M} \left( \sum_{i=1}^{M} |X_{i}|^{2} - \braket{X} \sum_{i=1}^{M}X_{i}^{*}  - \braket{X}^{*} \sum_{i=1}^{M} X_{i}  + \sum_{i=1}^{M} |\braket{X}|^{2} \right) \\
	= \frac{1}{M} \left( \sum_{i=1}^{M} |X_{i}|^{2} - M \underbrace{\braket{X} \braket{X}^{*}}_{= |\braket{X}|^{2}} - M \underbrace{\braket{X}^{*} \braket{X}}_{= |\braket{X}|^{2}}  + M |\braket{X}|^{2} \right) \\
	= \frac{1}{M} \left( \sum_{i=1}^{M} |X_{i}|^{2} - M |\braket{X}|^{2} \right) = \frac{1}{M} \sum_{i=1}^{M}  \left( |X_{i}|^{2} - |\braket{X}|^{2} \right) \text{,}
\label{eq:sampleVariance}
\end{multline}
again disregarding Bessel's correction.

$S^{2}_{X}$ is an estimator and thus a stochastic variable whose can again
be estimated.
\begin{align}
S^2_{S_{X}^{2}} = \frac{1}{M} \left( \frac{1}{M} \sum_{i=1}^{M} \left( |X_{i}|^{4} - 4  |X_{i}|^{2} \Re (X_{i}  \braket{X}^{*})+ 4 |X_{i}|^{2} |\braket{X}|^{2} +\right.\right. \nonumber\\
\left. \left. + 2 \Re ( X_{i}^{2} (\braket{X}^{*})^{2} ) - 3 |\braket{X}|^{4} \right) - \left(S_{X}^{2}\right)^{2} \right) \text{\footnotemark}
\label{eq:varofsamplevar}
\end{align}
\footnotetext{$\Re(z)=(z+z^{*})/2$ denotes the real part of a complex number.}
This helps to avoid underestimating the size of the confidence interval.
Eq.~\eqref{eq:varofsamplevar} can be derived in a similar way as the variance by expanding the definition of the variance
\begin{align}
S^2_{S_{X}^{2}} &= S_{X}^{4} - \left( S_{X}^{2} \right)^{2} =  \frac{1}{M^{2}} \left( \sum_{i=1}^{M}  \left( |X_{i} - \braket{X}|^{2} \right) \right)^{2} - \left( S_{X}^{2} \right)^{2} \text{.}
\end{align}



\cite{book:981959, book:13960}

\newpage

\subsection{T-Test}
\label{subsec:t-test}

To verify the implemented sampling methods we apply the T-test for a small
tensor network whose exact contraction value $\mu$ can be computed.
This test requires the mean to be normal distributed, which is the case as we
stated before in \ref{subsec:interval_est}.
For a given sample mean $\braket{X}$ and sample variance $S^2_X$, found from a stochastic experiment, we evaluate

\begin{align}
	T = \frac { \braket{ X } - \mu} { \sqrt { S_{X} ^ { 2 } / M } } \text{.}
\end{align}

We accept the assumption that the estimated mean approximates the exact mean if
$-t _ { M - 1 ; \frac{1-\gamma}{2} } < T < t _ { M - 1 ; \frac{1-\gamma}{2} }$
and reject if otherwise.
Where $t _ { M - 1 ; \frac{1-\gamma}{2} }$ denotes the $99.5\%$ quantiles
of Student's $t$-distribution.
\cite{100stattests}
