%\section{Abbreviation}
%
%Before we look at some examples we introduce an abbreviation to distinguish between different drawing sequences.
%Since we are inspecting a tensor network as shown in Figure~\ref{fig:tennet} (a) the abbreviation consists of six characters (the number of indices) where every character refers to the distribution function, with the order of indices being $\{ \splitatcommas{g,a,i,f,b,j} \}$.
%These characters can either be $u$, for uniform drawing, $t$, for tracing or $c$, for compound drawing.
%The abbreviation of our two examples will be therefor \emph{uccucc} and \emph{tttccc}.
%Note that this does not result in unique names but in our case it is sufficient enough.

\section{Findings}

With the stochastic evaluation of the contraction value we were able to reduce
the estimated computing time of $25$ hours to about $15$ minutes with some
drawing sequences reaching an accuracy of three significant digits while using
about $0.4 \%$ of the total number of index combinations.
The total number of index combinations $N$ is roughly $300$ billion in our example.
%This stochastic evaluation of the contraction value was applied on a tensor network from the direct term of the MP2 perturbation theory, as seen in Figure~\ref{fig:tennet} (a).


\subsubsection{Method 1: uniform and compound drawing}
For the first method a simple drawing sequence was chosen, which means that
the determination of an index is less dependent on predefined indices.
The chosen PDF is
$\gamma_{G,A,I}(g) \gamma_{g}(a, i) \gamma_{F,B,J}(f) \gamma_{f}(b, j)$.
The intention behind this is to see how much we can improve the results with
a more sophisticated drawing sequence.
The sampling order of the drawing sequence is starting from left to right and
can be read as:
\begin{center}
	\begin{tabular}{cccl}
		1. & draw & $g$ & uniformly sampled, with PDF $\gamma_{G,A,I}(g) \propto 1$ \\
		2. & draw & $a$ and $i$ & from compound drawing, with PDF $\gamma_{g}(a, i) \propto |\Gamma^{a}_{ig}|$ \\
		3. & draw & $f$ & uniformly sampled, with PDF $\gamma_{F,B,J}(f) \propto 1$ \\
		4. & draw & $b$ and $j$ & from compound drawing, with PDF $\gamma_{f}(b, j) \propto |\Gamma^{b}_{jf}|$. \\
	\end{tabular}
\end{center}
The contraction value $\braket{E^\mathrm{EMP2,1}}$ for different runs as a
function of the relative number of samples compared to the number of total
can be found in the Figures ~\ref{subfig:mean-uccucc} and
\ref{subfig:mean_lim-uccucc}.
The sampling variance $S^{2}_{E^\mathrm{EMP2,1}}$  including the confidence
intervals, as described in Eq.~\ref{eq:intervalEstimation}, as a function
of the relative number of samples compared to the number of total can be found
in the Figures~\ref{subfig:meanIntervallUnderest-uccucc-3} and
\ref{subfig:meanIntervallEstZoomLim-uccucc-3}.
Values for these results are given in Tab.~\ref{tab:results}.
For these results a T-test was carried out, seen in Fig.~\ref{subfig:t-Test-uccucc},
whereby our assumption that the estimated contraction value corresponds
to the actual contraction value can be accepted.

%The results for the estimation of the contraction value can be found in Figure~\ref{subfig:mean-uccucc} and \ref{subfig:mean_lim-uccucc}.
%Whereas Figure~\ref{subfig:meanIntervallUnderest-uccucc-3} shows the confidence intervals,as described in Eq.~\eqref{eq:intervalEstimation}
%for each estimated contraction value.
%For Fig.~\ref{subfig:meanIntervallEstZoomLim-uccucc-3} the the variance of the sample variance has been taken into account for
%calculating the confidence interval.
%
%
%The estimated contraction value and the sample variance can be seen in Figure~\ref{subfig:mean-uccucc} and \ref{subfig:meanIntervallEst-uccucc}.
%For these values a T-test was carried out where the result suggests that the algorithm works as intended.
%This can be seen in Figure~\ref{subfig:t-Test-uccucc}.


\subsubsection{Method 2: tracing and compound drawing}

This method, unlike method 1, does not rely on any predetermined
uniformly sampled indices, which means every indices is importance sampled.
The PDF used for method~2 is
$\gamma_{G,A}(i) \gamma_{F,i}(a) \gamma_{a,I}(g) \gamma_{g}(b,j) \gamma_{b,j}(f)$
and can be read as:

\begin{center}
	\begin{tabular}{cccl}
		1. & draw & $i$ & from tracing over $g$ and $a$, with PDF $\gamma_{G,A}(i) \propto \sum_{g,a} |\Gamma^{ag}_{i}|$\\
		2. & draw & $a$ & from tracing over $f$, with PDF $\gamma_{F,i}(a) \propto \sum_{f} | \left. \Gamma \right.^{af}_{i}|$\\
		3. & draw & $g$ & from tracing over $i$, with PDF $\gamma_{a,I}(g) \propto \sum_{i} | \left. \Gamma \right.^{a}_{ig}|$\\
		4. & draw & $b$ and $j$ & from compound drawing, with PDF $\gamma_{g}(b,j) \propto |\left. \Gamma \right.^{bg}_{j}|$\\
		5. & draw & $f$ & from compound drawing, with PDF $\gamma_{b,j}(f) \propto |\left. \Gamma \right.^{bf}_{j}|$.
	\end{tabular}
\end{center}
Analogous graphs, as for method 1, for the contraction value and the confidence interval
as a function of the number of sampls can be found in the
Figures~\ref{subfig:mean-ttTccc} and \ref{subfig:mean_lim-ttTccc}.
%, \ref{subfig:meanIntervallUnderest-ttTccc-3} and \ref{subfig:meanIntervallEstlim-ttTccc-3}.
From the performed T-test, with the results seen in
Fig.~\ref{subfig:t-Test-ttTccc}, we can assume that our estimated
contraction value corresponds to the true contraction value.


We can see that the estimated contraction value converges faster to
the exact contraction value compared to method 1, which can be seen by comparing
Fig.~\ref{subfig:mean_lim-uccucc} and \ref{subfig:mean_lim-ttTccc}.
The reason for this is the smaller variance.
This also results in a smaller confidence interval,
which can be seen in Fig.~\ref{subfig:meanIntervallUnderest-uccucc-ttTccc-2}
and \ref{subfig:meanIntervallEstZoomLim-uccucc-ttTccc-2},
and improves the overall quality of our estimation.

For the calculated values in Table \ref{tab:results} a tensor network with
the exact contraction value $ \mu = 1.96197 \cdot 10^{-13}$ in arbitrary
units and a sample size of $M$ of roughly 1 billion was used.
This is about $0.4 \%$ of the total number of index combinations.

\vspace*{3mm}
\begin{table}
	\centering
	\scalebox{0.9}{
		\makebox[\linewidth][c]{
			\bgroup
				\def\arraystretch{1.15}%  1 is the default, change whatever you need
				\begin{tabular}{c|c|c|c|c}
					 &
					 	$ \braket{E^\mathrm{EMP2,1}} \ \left[ 10^{-13} \right] $ &
					 	$\braket{E^\mathrm{EMP2,1}} - \mu \ \left[ 10^{-16} \right]$ &
					 	$S^{2}_{E^\mathrm{EMP2,1}} \ \left[ 10^{-21} \right]$ &
					  	$t_{M-1; \frac{1+\gamma}{2}} \sqrt{\frac{S^{2}_{E^\mathrm{EMP2,1}}}{M}} \ \left[ 10^{-16} \right]$ \\

					\hline \hline
					\multirow{3}{*}{\rotatebox{90}{{\footnotesize Method 1}}}
					 & 1.96547 & 3.5 	& 3.15 	& 42.304\\
					\cline{2-5}
					 & 1.97727 & 15.3 	& 3.183 	& 42.527\\
					\cline{2-5}
					 & 1.97326 & 11.29 	& 3.189	& 42.564\\

					\hline \hline
					\multirow{3}{*}{\rotatebox{90}{{\footnotesize Method 2}}}
					 & 1.96186 & -0.11 & 0.0968273 & 7.417\\
					\cline{2-5}
					 & 1.96107 & -0.9 & 0.0965759 & 7.407\\
					\cline{2-5}
					 & 1.96233 & 0.36 & 0.0965554 & 7.406\\

					\hline \hline
					\multirow{3}{*}{\rotatebox{90}{{\footnotesize Method 3}}}
					 & 1.9624 & 0.43 & 0.112722 & 8.00256\\
					\cline{2-5}
					 & 1.96281 & 0.84 & 0.112734 & 8.00299\\
					\cline{2-5}
					 & 1.96118 & -0.79 & 0.11234 & 7.98899\\

					\hline \hline
					\multirow{3}{*}{\rotatebox{90}{{\footnotesize Method 4}}}
					 & 1.96118 & -0.79 & 0.0607153 & 5.87319\\
					\cline{2-5}
					 & 1.96324 & 1.27 & 0.0608 & 5.87728\\
					\cline{2-5}
					 & 1.96584 & 3.87 & 0.0608224 & 5.87836\\
				\end{tabular}
			\egroup
		}
	}
	\caption{	Results of three independet runs using various drawing sequences,
	 			with the exact contraction value is
				$\mu = 1.96197 \cdot 10^{-13}$ in arbitrary units.
				The coverage probability is $\gamma = 0.99$.
				The PDFs drawing sequence used are:\\
			 	$\PDFt{Method 1} = \gamma_{G,A,I}(g) \gamma_{g}(a, i) \gamma_{F,B,J}(f) \gamma_{f}(b, j)$,\\
				$\PDFt{Method 2} = \gamma_{G,A}(i) \gamma_{F,i}(a) \gamma_{a,I}(g) \gamma_{g}(b,j) \gamma_{b,j}(f)$,\\
				$\PDFt{Method 3} = \gamma_{G,A,I}(a) \gamma_{F,a}(i) \gamma_{a, i}(g) \gamma_{g}(b, j) \gamma_{b, j}(f)$,\\
				$\PDFt{Method 4} = \gamma_{G,A,I}(i) \gamma_{F,i}(a) \gamma_{a, I}(g) \gamma_{g}(b, j) \gamma_{b, j}(f)$.
			}
	\label{tab:results}
\end{table}




%% Graphs of the two runs
% mean
\begin{figure}
	\begin{subfigure}[t]{0.49\textwidth}
		\includegraphics[width=\textwidth]{figures/uccucc/mean-uccucc.pdf}
		\caption{Method 1}
		\label{subfig:mean-uccucc}
	\end{subfigure}
	\begin{subfigure}[t]{0.49\textwidth}
		\includegraphics[width=\textwidth]{figures/uccucc/mean_lim-uccucc.pdf}
		\caption{Method 1, enlarged Fig.~\ref{subfig:mean-uccucc}}
		\label{subfig:mean_lim-uccucc}
	\end{subfigure}

	\begin{subfigure}[t]{0.49\textwidth}
		\includegraphics[width=\textwidth]{figures/ttTccc/mean-ttTccc.pdf}
		\caption{Method 2}
		\label{subfig:mean-ttTccc}
	\end{subfigure}
	\begin{subfigure}[t]{0.49\textwidth}
		\includegraphics[width=\textwidth]{figures/ttTccc/mean_lim-ttTccc.pdf}
		\caption{Method 2, enlarged Fig.~\ref{subfig:mean-ttTccc}}
		\label{subfig:mean_lim-ttTccc}
	\end{subfigure}

	% \begin{subfigure}[t]{0.49\textwidth}
    % 		\includegraphics[width=\textwidth]{figures/uccucc-ttTccc/mean-uccucc-ttTccc.pdf}
    % 		\caption{Method 1 and method 2. For better comparison.}
    % 		\label{subfig:mean-uccucc-ttTccc}
	% \end{subfigure}
	% \begin{subfigure}[t]{0.49\textwidth}
	% 	\includegraphics[width=\textwidth]{figures/uccucc-ttTccc/mean_lim-uccucc-ttTccc.pdf}
	% 	\caption{enlarged Fig.~\ref{subfig:mean-uccucc-ttTccc}}
	% 	\label{subfig:mean_lim-uccucc-ttTccc}
    % \end{subfigure}

	\caption{The estimated contraction value as a function of the relative
			number of samples used for $\braket{E^\mathrm{MP2,1}}$ compared the
			number of total terms to sum.
			The dashed line indicates the true mean
			$\mu  = 1.96197 \cdot 10^{-13}$ in arbitrary units.
	}
	\label{fig:mean}
\end{figure}



% Intervall estimation
\begin{figure}
	% \begin{subfigure}[t]{0.48\textwidth}
	% 	\includegraphics[width=\textwidth]{figures/uccucc/meanIntervallUnderest-uccucc-3.pdf}
	% 	\caption{Method 1}
	% 	\label{subfig:meanIntervallUnderest-uccucc-3}
	% \end{subfigure}
	% \begin{subfigure}[t]{0.48\textwidth}
	% 	\includegraphics[width=\textwidth]{figures/uccucc/meanIntervallEstZoomLim-uccucc-3.pdf}
	% 	\caption{Method 1, enlarged Fig.~\ref{subfig:meanIntervallUnderest-uccucc-3}}
	% 	\label{subfig:meanIntervallEstZoomLim-uccucc-3}
	% \end{subfigure}
	%
	% \begin{subfigure}[t]{0.48\textwidth}
    % 		\includegraphics[width=\textwidth]{figures/ttTccc/meanIntervallUnderest-ttTccc-3.pdf}
	% 	\caption{Method 2}
	% 	\label{subfig:meanIntervallUnderest-ttTccc-3}
    % \end{subfigure}
	% \begin{subfigure}[t]{0.48\textwidth}
    % 		\includegraphics[width=\textwidth]{figures/ttTccc/meanIntervallEstLim-ttTccc-3.pdf}
    % 		\caption{Method 2, enlarged Fig.~\ref{subfig:meanIntervallUnderest-ttTccc-3}}
	% 	\label{subfig:meanIntervallEstlim-ttTccc-3}
    % \end{subfigure}

	\begin{subfigure}[t]{0.48\textwidth}
    		\includegraphics[width=\textwidth]{figures/uccucc-ttTccc/meanIntervallUnderest-uccucc-ttTccc-2.pdf}
    		\caption{For a better comparison a single run from method 1 and 2 have been combined into one plot.}
    		\label{subfig:meanIntervallUnderest-uccucc-ttTccc-2}
	\end{subfigure}
	\begin{subfigure}[t]{0.48\textwidth}
    		\includegraphics[width=\textwidth]
    			{figures/uccucc-ttTccc/meanIntervallEstZoomLim-uccucc-ttTccc-2.pdf}
    		\caption{Fig.~\ref{subfig:meanIntervallUnderest-uccucc-ttTccc-2} enlarged}
		\label{subfig:meanIntervallEstZoomLim-uccucc-ttTccc-2}
	\end{subfigure}

	\caption{The estimated contraction value with the corresponding confidence
			intervals, estimated according to Eq.~\eqref{eq:intervalEstimation},
			as a function of the relative number of samples used for
			$\braket{E^\mathrm{MP2,1}}$ compared to the number of total terms to
			sum with the true mean $\mu  = 1.96197 \cdot 10^{-13} $ in
			arbitrary units.
			The diagram on the right side takes the variance of
			the sample variance into account and shows a gray line
			proportional to $1 / \sqrt{M}$, indicating that the estimate of the
			variance is well converged.
	}
	\label{fig:meanIntervall}
\end{figure}



\begin{figure}
	\centering
	\includegraphics[width=0.5\textwidth]{figures/uccucc-ttTccc/variance-uccucc-ttTccc-2.pdf}
	\caption{The estimated variance from method 1 and 2, according to Eq.~\eqref{eq:sampleVariance},
			as a function of the number of sampled used for $\braket{E^\mathrm{MP2,1}}$
			over the number of total entries in the tensor.
			Note that the $y$-axis is logarithmic.
  		}
	\label{fig:variance-uccucc-ttTccc}
\end{figure}


%T-test
\begin{figure}
	\centering
	\begin{subfigure}[t]{0.49\textwidth}
		\includegraphics[width=\textwidth]{figures/uccucc/t-Test-uccucc-3.pdf}
		\caption{Method 1}
		\label{subfig:t-Test-uccucc}
	\end{subfigure}
	\begin{subfigure}[t]{0.49\textwidth}
		\includegraphics[width=\textwidth]{figures/ttTccc/t-Test-ttTccc-3.pdf}
		\caption{Method 2}
		\label{subfig:t-Test-ttTccc}
	\end{subfigure}
	\caption{T-test: The fact that $T$ does not exceed the upper $t _ { M - 1 ; \frac{1-\gamma}{2} }$ or
			lower limit $-t _ { M - 1 ; \frac{1-\gamma}{2} }$, as described in \ref{subsec:t-test},
			suggests that the contraction value is correctly estimated.
			The probabilit of failure $\alpha$ used is equal to $0.01$.}
	\label{fig:t-Test}

\end{figure}
