\chapter{Results}
\label{ch:results}

The contraction of a tensor network is approximated by sampling a stochastic function such that its mean value coincides with the contraction value.

\section{Statistic}

\subsection{Interval estimation}
\label{subsec:interval_est}

From the samples drawn a confidence interval \eqref{eq:intervalEstimation} is estimated.

\begin{align}
\left[ \braket{ X } - t _ { M - 1 ; \frac { 1 + \gamma } { 2 } } \sqrt { \frac { S _ { X } ^ { 2 } } { M } } , \braket{ X } + t _ { M - 1 ; \frac { 1 + \gamma } { 2 } } \sqrt { \frac { S _ { X } ^ { 2 } } { M } } \right]
\label{eq:intervalEstimation}
\end{align}

For an estimator of the mean and standard deviation the law of large numbers can be used, which states that the sample average of a repeated experiment where each realization is independent is normal distributed. \cite{book:11854}
The variance $\sigma_{X}^{2}$ is estimated by the sample variance $S_X$.
For large $M$ the Student's t-distribution $t_{n-1;\frac{1+\gamma}{2}}$ approximates the normal distribution well enough so that their quantiles are approximately equivalent. 
Requiring a coverage probability of $0.99$ that the true contraction value lies within the estimated interval, we use the $0.995$ quantile, of Eq.~\eqref{eq:intervalEstimation} which is $t_{\infty, 0.995} = z_{0.995} = 2.576$. \cite{AC08993515}

%To make an estimation of the sample variance
%$$\left[ \frac { ( n - 1 ) S _ { n } ^ { 2 } } { \chi _ { n - 1 ; \frac { 1 + \gamma } { 2 } } ^ { 2 } } , \frac { ( n - 1 ) S _ { n } ^ { 2 } } { \chi _ { n - 1 ; \frac { 1 - \gamma } { 2 } } ^ { 2 } } \right]$$
%is used, where as ${ \chi _ { n - 1 ; \frac { 1 - \gamma } { 2 } } ^ { 2 } }$ referes to the $\chi^{2}$-distribution.
%
%
%distribution of variance
%$$ \mu^{2} = |\mu|^{2} = \mu \mu^{*}$$
%    $$ \mu^{4} = \dfrac{1}{M} \sum^{M}_{i}   \left(
%    					|x_{i}|^{4} -
%    					4 |x_{i}|^{2} \Re \left \{ x_{i} \mu^{*} \right \}+ 
%    					4|x_{i}|^{2} |\mu|^{2} + 
%					2 \Re \left \{ x_{i}^{2} \left. \mu^{*} \right.^{2} \right \} -	
%    					3 |\mu|^{4} \right) $$
%    $$\mathbb{V}\left[ S^{2} \right] = \dfrac{1}{M} \left( \mu^4 - (S^2)^2 \right)$$



\subsection{Sample Variance Distribution}

The variance $\sigma_{X}^{2}$ is usually unknown as it takes to long to calculate.
Instead we are using the sample variance $S_{X}^{2}$ to estimate the variance $\sigma_X^2$. 

\begin{align}
\mathbb{V}(X) = S_{X}^{2} = \frac{M}{M-1} \underbrace{\left(\frac{1}{M} \sum_{i=1}^{M} |X_{i}-\braket{X} |^{2}\right)}_{\text{biased sample variance}} = \frac{1}{M-1} \sum_{i=1}^{M}\left|X_{i}-\braket{X}\right|^{2} 
\label{eq:sampleVarianceDistribution}
\end{align}


The prefactor $\frac{M}{M-1}$ is the correction term to turn a biased estimation into an unbiased estimation of the variance $S_{X}^{2}$ and is called \emph{Bessel's correction}.
This correction can be understood as the remaining degrees of freedom, since the mean is unknown as well and used to determine the sample variance.  \cite{ADictionaryofStatistics}

Since the sample sizes $M \sim 10^{9}$  will be fairly large, we can neglected the Bessel's correction in good conscience. 
A similar transformation as in \eqref{eq:var} can be applied to the sample variance  of a complex observable, here $X$, to simplify the equation as follows.
\begin{multline}
S_{X}^{2} = \frac{1}{M} \sum_{i=1}^{M}\left( |X_{i}|^{2} - X_{i}^{*} \braket{X} - X_{i} \braket{X}^{*} + |\braket{X}|^{2} \right) \\
	= \frac{1}{M} \left( \sum_{i=1}^{M} |X_{i}|^{2} - \braket{X} \sum_{i=1}^{M}X_{i}^{*}  - \braket{X}^{*} \sum_{i=1}^{M} X_{i}  + \sum_{i=1}^{M} |\braket{X}|^{2} \right) \\
	= \frac{1}{M} \left( \sum_{i=1}^{M} |X_{i}|^{2} - M \underbrace{\braket{X} \braket{X}^{*}}_{= |\braket{X}|^{2}} - M \underbrace{\braket{X}^{*} \braket{X}}_{= |\braket{X}|^{2}}  + M |\braket{X}|^{2} \right) \\
	= \frac{1}{M} \left( \sum_{i=1}^{M} |X_{i}|^{2} - M |\braket{X}|^{2} \right) = \frac{1}{M} \sum_{i=1}^{M}  \left( |X_{i}|^{2} - |\braket{X}|^{2} \right) \text{,}
\label{eq:sampleVariance}
\end{multline}

again disregarding Bessel's correction.

Since $S^{2}_{X}$ is an estimator of the variance, one can calculate the variance of the sample variance itself. 
This helps to avoid underestimating the size of the confidence interval.
\begin{align}
\mathbb{V} \left(S_{X}^{2} \right) = \frac{1}{M} \left( \frac{1}{M} \sum_{i=1}^{M} \left( |X_{i}|^{4} - 4  |X_{i}|^{2} \Re (X_{i}  \braket{X}^{*})+ 4 |X_{i}|^{2} |\braket{X}|^{2} +\right.\right. \nonumber\\
\left. \left. + 2 \Re ( X_{i}^{2} (\braket{X}^{*})^{2} ) - 3 |\braket{X}|^{4} \right) - \left(S_{X}^{2}\right)^{2} \right) \text{\footnotemark}
\label{eq:varofsamplevar}
\end{align}
\footnotetext{$\Re(z)=(z+z^{*})/2$ denotes the real part of a complex number.}
Eq.~\eqref{eq:varofsamplevar} can be derived in a similar way as the variance by expanding the definition of the variance 
\begin{align}
\mathbb{V} (S_{X}^{2}) &= S_{X}^{4} - \left( S_{X}^{2} \right)^{2} =  \frac{1}{M^{2}} \left( \sum_{i=1}^{M}  \left( |X_{i} - \braket{X}|^{2} \right) \right)^{2} - \left( S_{X}^{2} \right)^{2} \text{.}
\end{align}



\cite{book:981959, book:13960}

\newpage

\subsection{T-Test}
\label{subsec:t-test}

To verify the correctness of the implemented sampling methods we apply the T-test for a small tensor network whose exact contraction value $\mu$ can be computed.
This test requires the mean be normal distributed, which is the case as we stated before in \ref{subsec:interval_est}. 
For a given sample mean $\braket{X}$ and sample variance $S^2_X$, found from a stochastic experiment, we evaluate

\begin{align}
	T = \frac { \braket{ X } - \mu} { \sqrt { S_{X} ^ { 2 } / M } } \text{.}
\end{align}

We accept the assumption that the estimated mean approximates the exact mean if $T > -t _ { M - 1 ; \frac{1-\gamma}{2} }$ and $T < t _ { M - 1 ; \frac{1-\gamma}{2} }$ and reject if otherwise. \cite{100stattests}

\section{Abbreviation}

Before we look at some examples we introduce an abbreviation to distinguish between different drawing sequences.
Since we are inspecting a tensor network as shown in Figure~\ref{fig:tennet} (a) the abbreviation consists of six characters (the number of indices) where every character refers to the distribution function, with the order of indices being $\{ \splitatcommas{g,a,i,f,b,j} \}$.
These characters can either be $u$, for uniform drawing, $t$, for tracing or $c$, for compound drawing.
The abbreviation of our two examples will be therefor \emph{uccucc} and \emph{tttccc}.
Note that this does not result in unique names but in our case it is sufficient enough.

\section{Findings}

With the stochastic evaluation of the contraction value we were able to reduce the estimated computing time of $25$ hours to about $15$ minutes with some drawing sequences reaching an accuracy of three significant digits while using about $0.00387$ times the total number of index combinations.
The total number of index combinations $N$ is equal to $314891567104$ in our example.
%This stochastic evaluation of the contraction value was applied on a tensor network from the direct term of the MP2 perturbation theory, as seen in Figure~\ref{fig:tennet} (a).


\subsubsection{uniform and compound drawing (\emph{uccucc})}
For the first drawing sequence a simpler drawing sequence was chosen. 
Also with the intention to see if we can improve the results with a more sophisticated drawing sequence afterwards.

The specific drawing sequence for \emph{uccucc} is\\
\vspace*{3mm}
\begin{tabular}{ccl}
	1. & 	$g$	 	& uniform sampling, $\gamma_{G,A,I}(g) \propto 1$ \\
	2. & $a$ and $i$	& compound drawing, $\gamma_{g}(a, i) \propto |\Gamma^{a}_{ig}|$ \\
	3. & 	$f$		& uniform sampling, $\gamma_{F,B,J}(f) \propto 1$ \\
	4. & $b$ and $j$	& compound drawing, $\gamma_{f}(b, j) \propto |\Gamma^{b}_{jf}|$ \\
\end{tabular}
\vspace*{2mm}

The estimated contraction value and the sample variance can be seen in Figure~\ref{fig:mean-uccucc} and \ref{fig:meanIntervallEst-uccucc}.
For these values a T-test was carried out where the result suggests that the algorithm works as intended.
This can be seen in Figure~\ref{fig:t-Test-uccucc}.

\begin{figure}
	\makebox[\linewidth][c]{	
		\subfigure[]{
			\includegraphics[width=0.6\textwidth]{figures/uccucc/mean-uccucc.pdf}
		}  
		\subfigure[Here smaller sample sizes have been cut off due to their large variance.]{
			\includegraphics[width=0.6\textwidth]{figures/uccucc/mean_lim-uccucc.pdf} 
		}
	}  
	\caption{The estimated contraction value as a function of the number of sampled used for $\braket{E^\mathrm{MP2,1}}$ over the number of total entries in the tensor.
		The dashed line indicates the true mean $ \mu  = 1.96197 \cdot 10^{-13} $.
	}
	\label{fig:mean-uccucc}
\end{figure}



\begin{figure}
	\makebox[\linewidth][c]{
		\subfigure[
			The confidence intervals were estimated as described in Eq.~\eqref{eq:intervalEstimation}.]{
	    		\includegraphics[width=0.6\textwidth]{figures/uccucc/meanIntervallUnderest-uccucc-3.pdf}
		}
		\subfigure[
			In the estimation of the confidence interval the variance of the sample variance was taken into account.
			The gray line is the envelope of the variance which is proportional to $1 / \sqrt{M}$.
		 ]{
			\includegraphics[width=0.6\textwidth]{figures/uccucc/meanIntervallEstZoomLim-uccucc-3.pdf} 
		}
	}  
	\caption{The estimated contraction value as a function of the number of sampled used for $\braket{E^\mathrm{MP2,1}}$ over the number of total entries in the tensor including the interval estimation.}
	\label{fig:meanIntervallEst-uccucc}
\end{figure}



\begin{figure}
	\centering
	\includegraphics[width=0.6\textwidth]{figures/uccucc/t-Test-uccucc-3.pdf}
	\caption{The fact that $T$ does not exceed the upper $t _ { M - 1 ; \frac{1-\gamma}{2} }$ or lower limit $-t _ { M - 1 ; \frac{1-\gamma}{2} }$, as described in \ref{subsec:t-test}, suggests that the contraction value is correctly estimated.
	The conditional probability $\alpha$ used is equal to $0.01$.}
	\label{fig:t-Test-uccucc}
\end{figure}




\subsubsection{tracing and compound drawing (\emph{tttccc})}

\emph{tttccc} does not relay on any predetermined uniform sampled indices.
This means every indices is importance sampled.


The drawing sequence used in this example is,\\
\vspace*{3mm}	
\begin{tabular}{ccl}
1. &		$i$		& tracing over $g$ and $a$, $\gamma_{G,A}(i) \propto \sum_{g,a} |\Gamma^{ag}_{i}|$\\
2. & 	$a$ 		& tracing over $f$, $\gamma_{F,i}(a) \propto \sum_{f} | \left. \Gamma \right.^{af}_{i}|$\\
3. & 	$g$ 		& tracing over $i$, $\gamma_{a,I}(g) \propto \sum_{i} | \left. \Gamma \right.^{a}_{ig}|$\\
4. & $b$ and $j$	& compound drawing, $\gamma_{g}(b,j) \propto |\left. \Gamma \right.^{bg}_{j}|$\\
5. & 	$f$ 		& compound drawing, $\gamma_{b,j}(f) \propto |\left. \Gamma \right.^{bf}_{j}|$
\end{tabular}

\vspace*{4mm}
For this sampling sequence the results for the mean can be seen in Fig.~\ref{fig:mean-ttTccc} and for the confidence interval in Fig.~\ref{fig:meanIntervallEst-ttTccc}.
The T-test performed on these results, as in Fig.~\ref{fig:t-Test-ttTccc}, lets us assume that the true contraction value corresponds to our estimated contraction value. 

\begin{figure}
	\makebox[\linewidth][c]{
		\subfigure[]{
			\includegraphics[width=0.6\textwidth]{figures/ttTccc/mean-ttTccc.pdf}
		}
		\subfigure[Smaller sample sizes were truncated due to a large sample variance.]{
			\includegraphics[width=0.6\textwidth]{figures/ttTccc/mean_lim-ttTccc.pdf} 
		}
	}  
  	\caption{The estimated contraction value as a function of the number of sampled used for $\braket{E^\mathrm{MP2,1}}$ over the number of total entries in the tensor.
		The dashed line indicates the true mean $ \mu  = 1.96197 \cdot 10^{-13} $.
	}
 	\label{fig:mean-ttTccc}
\end{figure}


\begin{figure}
	\makebox[\linewidth][c]{
		\subfigure[]{
	    		\includegraphics[width=0.6\textwidth]{figures/ttTccc/meanIntervallUnderest-ttTccc-3.pdf}
	    }  
		\subfigure[For the interval estimation the variance of the sample variance has been considered.
		The gray lines enveloping the confidence interval are proportional to $1/\sqrt{M}$.]{
	    		\includegraphics[width=0.6\textwidth]{figures/ttTccc/meanIntervallEstLim-ttTccc-3.pdf} 
	    }
	}  
  	\caption{The estimated contraction value as a function of the number of sampled used for $\braket{E^\mathrm{MP2,1}}$ over the number of total entries in the tensor.
  		The error bars represent the confidential interval as described in Eq.~\eqref{eq:intervalEstimation}.}
  	\label{fig:meanIntervallEst-ttTccc}
\end{figure}


\begin{figure}
	\centering
	\includegraphics[width=0.65\textwidth]{figures/ttTccc/t-Test-ttTccc-3.pdf}
	\caption{The fact that $T$ does not exceed the upper $t _ { M - 1 ; \frac{1-\gamma}{2} }$ or lower limit $-t _ { M - 1 ; \frac{1-\gamma}{2} }$, as described in \ref{subsec:t-test}, suggests that the contraction value is correctly estimated.
	The conditional probability $\alpha$ is equal to $0.01$.}
	\label{fig:t-Test-ttTccc}
\end{figure}












\subsubsection{comparison}


Comparing the two drawing sequences we can see that estimated contraction value converges faster to the exact contraction value, which can be seen in Fig.~\ref{fig:mean-uccucc-ttTccc}.
The reason for this is the smaller variance, as seen in Fig.~\ref{fig:variance-uccucc-ttTccc}.
The smaller variance also results in a smaller confidence interval, as in Fig.~\ref{fig:meanIntervallEst_uccucc-ttTccc}, which improves the overall quality of our estimation.

For the calculated values in the table below a tensor network with the exact contraction value $ \mu = 1.96197 \cdot 10^{-13}$ and a sample size of $M = 1 167 998 976$ was used.
This is about $0.00387$ of the total number of index combinations.

\vspace*{3mm}
\scalebox{0.9}{
\makebox[\linewidth][c]{
\begin{tabular}{c|c|c|c|c}
	 & $ \braket{E^\mathrm{EMP2,1}} \ \left[ 10^{-13} \right] $ & $\braket{E^\mathrm{EMP2,1}} - \mu \ \left[ 10^{-16} \right]$ & $S^{2}_{E^\mathrm{EMP2,1}} \ \left[ 10^{-21} \right]$ & $t_{M-1; \frac{1+\gamma}{2}} \sqrt{\frac{S^{2}_{E^\mathrm{EMP2,1}}}{M}} \ \left[ 10^{-16} \right]$ \\ 
	\hline \hline
	
	\multirow{3}{*}{\rotatebox{90}{uccucc}} 
	 & 1.96547 & 3.5 	& 3.15 	& 42.304\\ 
	\cline{2-5} 
	 & 1.97727 & 15.3 	& 3.183 	& 42.527\\ 
	\cline{2-5}  
	 & 1.97326 & 11.29 	& 3.189	& 42.564\\ 

	\hline \hline
	\multirow{3}{*}{\rotatebox{90}{tttccc}} 
	 & 1.96186 & -0.11 & 0.0968273 & 7.417\\ 
	\cline{2-5} 
	 & 1.96107 & -0.9 & 0.0965759 & 7.407\\ 
	\cline{2-5} 
	 & 1.96233 & 0.36 & 0.0965554 & 7.406\\ 
\end{tabular}
}
}

\begin{figure}
	\makebox[\linewidth][c]{
		\subfigure[]{
	    		\includegraphics[width=0.6\textwidth]{figures/uccucc-ttTccc/mean-uccucc-ttTccc.pdf}
	    }  
		\subfigure[more visible on one run]{
	    		\includegraphics[width=0.6\textwidth]{figures/uccucc-ttTccc/mean_lim-uccucc-ttTccc.pdf} 
	    }
	}  
	\caption{The estimated contraction value as a function of the number of sampled used for $\braket{E^\mathrm{MP2,1}}$ over the number of total entries in the tensor.
  		The error bars represent the confidential interval as described in Eq.~\eqref{eq:intervalEstimation}.}
	\label{fig:mean-uccucc-ttTccc}
\end{figure}



\begin{figure}
	\centering
	\includegraphics[width=0.45\textwidth]{figures/uccucc-ttTccc/variance-uccucc-ttTccc-6.pdf} 
	\caption{The estimated variance, according to Eq.~\eqref{eq:sampleVariance}, as a function of the number of sampled used for $\braket{E^\mathrm{MP2,1}}$ over the number of total entries in the tensor.
  		}
	\label{fig:variance-uccucc-ttTccc}
\end{figure}

\begin{figure}
	\makebox[\linewidth][c]{
		\subfigure[]{
	    		\includegraphics[width=0.6\textwidth]{figures/uccucc-ttTccc/meanIntervallUnderest-uccucc-ttTccc-2.pdf}
	    }  
		\subfigure[For the interval estimation the variance of the sample variance has been considered.
		The gray lines enveloping the confidence interval are proportional to $1/\sqrt{M}$.]{
	    		\includegraphics[width=0.7\textwidth]{figures/uccucc-ttTccc/meanIntervallEstZoomLim-uccucc-ttTccc-2.pdf} 
	    }
	}  
  	\caption{The estimated contraction value, from a \emph{uccucc} and \emph{tttccc} run, as a function of the number of sampled used for $\braket{E^\mathrm{MP2,1}}$ over the number of total entries in the tensor.
  		The error bars represent the confidential interval as described in Eq.~\eqref{eq:intervalEstimation}.}
  	\label{fig:meanIntervallEst_uccucc-ttTccc}
\end{figure}



\subsubsection{Conclusion}


In this work, we outlined a way to estimate the contraction value of a tensor network by using stochastic evaluation.
The used tensor network was obtained from the second-order  M{\o}ller--Plesset perturbation theory by translating correlation energy into a direct and exchange term with focus on the direct term
\begin{align}
	E^{\mathrm{MP2,1}} = 2 \sum_{ijab} \sum_{g,f} \left.\Gamma\right.^{ag}_{i} \left.\Gamma\right.^{b}_{jg} \left.\Gamma\right.^{i}_{af} \left.\Gamma\right.^{jf}_{b} \text{.}
\end{align}
This translation was mainly done by linearization of the Goldstone diagram.
The contraction value, in our case the direct term, is calculated by summing over all indices combinations $\{ \splitatcommas{g,a,i,f,b,j} \}$.

At this point the stochastic evaluation based on a Monte-Carlo method called importance sampling is applied by picking indices combinations which have a higher contribution to the contraction value.
Indices from an indices combination can be drawn from different distributions where some distributions depend on one or more known indices resulting in a drawing sequence. 
The distributions for drawing the indices are uniform, marginal and conditional probability distributions of the tensor~$\Gamma^{a}_{ig}$.

When choosing a drawing sequence the influence of a drawing upon the following drawings should be considered.
For ideal drawing sequences this will result in a minimization of the sample variance and therefore an more accurate estimation of the contraction value.


With this stochastic evaluation we were able to reduce the estimated 25 hour to calculated the contraction value to roughly 15 minutes with an accuracy of three significant digits while using about $0.00387$ times the total data available. 
This method of estimating the contraction value is not only applicable to this specific tensor network from the MP2 perturbation theory but also to any other linearized network of tensors.
