\chapter{Stochastic evaluation of tensor networks}
\label{ch:stocheval}

% {
% \begin{LARGE}
% What is the test system: carbon diamond with $N_\mathrm{o} = N_\mathrm{v} = 32$
% and $N_\mathrm{F} = 534$, with $\tau = \dots$
%
% Das System war carbon diamond in DFT-PBE relaxierter 2-atomiger fcc-Einheitszelle.
% Der He-core ist eingefroren, bleiben 4 Valenzelektronen pro Atom,
% daher 4 besetzte closed-shell Hartree--Fock Baender.
% Der Einfachheit halber wurden auch nur 4 unbesetzten Hartree-Fock B\"andern gew\"ahlt.
% Die Brillouin Zone wurde mit einem 2x2x2 k-Punkten integriert.
% (=32 besetzte/unbesetzte Orbitale)
% Die Tau Integration war auf einem minimax-Gitter \cite{doi:10.1021/ct5001268},
% der untersuchte Tau Punkt war der kleinste, da die Tensoren bei
% diesem Tau Punkt am dichtesten zu erwarten sind.
% \end{LARGE}
% }

For the comparison whether a stochastic evaluation improves over the
deterministic evaluation a system with an Intel Core i7-7700 processor with
hyper-threading enabled and the optimization level of \texttt{-Ofast} for the
\texttt{g++} compiler was used.

The tensors used to calculate the contraction value come from a
carbon diamond system in DFT-PBE relaxed 2-atom fcc unit cell.
The He-core is frozen, which means there remain $4$ valence electrons per atom,
therefore 4 occupied closed-shell Hartree--Fock bands.
For the sake of simplicity only $4$ unoccupied Hartree--Fock bands were chosen.
The Brillouin zone was integrated with a $ 2\!\times\!2\!\times\!2$
k-point, resulting in $N_\mathrm{o}=32$ occupied and $N_\mathrm{v}=32$
unoccupied orbitals.
The Tau integration was on a minimax grid. \cite{doi:10.1021/ct5001268}
The examined Tau point was the smallest, since the tensors are expected
to be the densest to this point.
The deterministic evaluation of $E^{\mathrm{MP2,2}}$ on such a system was
estimated to take 25 hours.

For $E^{\mathrm{MP2,1}}$ the computing time would have been smaller,
since one can exploit the symmetry of this problem.
If this is not done, the calculation time will be the same

\section{Monte-Carlo evaluation}
The \emph{stochastic variable} $X$, also known as random variable, assumes a numerical value upon drawing.
Such a variable $X$ can be either discrete or continuous.
Over the course of an arbitrary amount of drawings the probabilities of an outcome must not change and each drawing must be independent of each other.
The probability of an outcome is given by the \emph{cumulative distribution function} (CDF) which is defined as
\begin{equation}
	\mathbb{P} (X \leq x) = P_X(x)  \text{.}
\end{equation}


The function $p_{X}$ is the derivative of the CDF and is called \emph{probability density function} (PDF) which is used to specify the probability density of a certain outcome.
\begin{align}
p_{X}(x) = \dfrac{d}{dx} P_X(x)
\end{align}
For discrete random variables the PDF is a difference rather than a differential
\begin{align}
p_{X}(x) = \dfrac{1}{1}\left( P_{X}(x) - P_{X}(x-1) \right) = P_{X}(x) - P_{X}(x-1) \text{.}
\end{align}
The CDF is a non-decreasing function with following properties
\begin{align*}
\lim _{x \rightarrow-\infty} P_{X}(x)=0, \quad \lim _{x \rightarrow+\infty} P_{X}(x)=1
\end{align*}
which means for the PDF that $\lim _{x \rightarrow \pm \infty} p_{X}(x)=0$ and that it has to be \emph{normalized}
\begin{equation}
\int_{-\infty}^{\infty}\kern-2.6em\sum dx \  p(x) = 1 \text{.\footnotemark}
\label{eq:PDF_normalized}
\end{equation}
\footnotetext{The symbol $\int\kern-1.1em\sum$ denotes that the same equation can be used for discrete as well as continuous distributions. If this is not the case only one of the symbols is used.}

The \emph{$n$-th moment} of a stochastic function $O(X)$ for a stochastic variable $X$ distributed according to the PDF $p_{X}$ is defined as
\begin{equation}
	\mathbb{E}\left[ O(X)^{n}; p_{X} \right] = \int_{-\infty}^{\infty} dP_{X}(x) \ O(x)^{n}
\end{equation}

Of the PDF $p_{X}$ of the CDF $P_{X}$ exists, then the $n$-th moment can be written as
\begin{equation}
	\mathbb{E}\left[ O(X)^{n}; p_{X} \right] = \ \int_{-\infty}^x\kern-2.6em\sum dx \ O(x)^{n}\  p_{X}(x) \text{.}
\end{equation}

For $n=1$ we will get the first moment which is called the \emph{expectation value}.
\begin{equation}
	\mathbb{E}\left[ O(X); p_{X} \right] = \ \int_{-\infty}^x\kern-2.6em\sum dx \ O(x) \ p_{X}(x) = o \text{ \footnotemark}
\label{eq:1stMoment}
\end{equation}
\footnotetext{Deterministic variables are denoted by lower case letters.}
We denote that the random variable $X$ is distributed according to this PDF, by $X \sim p_{X}$. The reason for the above notation will be clarified in Section \ref{sec:importanceSampling}.\\


Another key aspect of a stochastic function is its \emph{variance}, defined as
\begin{align}
	\mathbb{V}[O(X); p_X] &= \mathbb{E}\left[\left(O(X) - \mathbb{E}[O(X);p_{X}]\right)^{2};p_{X}\right] \nonumber \\
	 &= \ \int_{-\infty}^{\infty}\kern-2.6em\sum dx \ (O(x) - \mathbb{E}[O(X);p_{X}])^{2} \  p(x)
\end{align}

The variance can be further simplified and expressed only by the first and second moment. By expanding the square and using the normalization of the PDF and the definition of the first moment, we get

\begin{align}
	\mathbb{V}[O(X); p_X]
	 	&= \ \int_{-\infty}^{\infty}\kern-2.6em\sum dx \ \left(O(x) - \mathbb{E}[O(X);p_{X}]\right)^{2} \  p(x) \nonumber\\
	 	&= \ \int_{-\infty}^{\infty}\kern-2.6em\sum dx \ \left(O(x)^{2} - 2 \ O(x) \ \mathbb{E}[O(X);p_{X}] +  \mathbb{E}[O(X);p_{X}]^{2} \right) \  p_{X}(x) \nonumber\\
		&= \ \int_{-\infty}^{\infty}\kern-2.6em\sum dx \ O(x)^{2} \ p_{X}(x) - \mathbb{E}[O(x);p_{X}]^{2} \nonumber\\
	 	&= \mathbb{E}[O(x)^{2};p_{X}] - \mathbb{E}[O(X);p_{X}]^{2} \text{.}
\label{eq:var}
\end{align}

By drawing $M$ samples $X_{i} \sim p_{X}$ both the expectation value and the variance can be estimated in an unbiased way by
\begin{align}
\braket{O}_{p_{X}} = \dfrac{1}{M} \sum_{i=1}^{M} O(x_i) \qquad \text{and} \qquad
S^{2}_{O,p_{X}} = \dfrac{1}{M-1} \sum_{i=1}^{M} (O(x_{i}) - \braket{O}_{p_{X}})^{2} \text{,}
\label{eq:unbiasedEsti_expVar}
\end{align}

respectively.
Note that estimations are stochastic functions.
Since $\braket{O}_{p_{X}}$ is an unbiased estimator its expectation value
is depending on $M$ realizations of the stochastic variable $X$.

\begin{equation}
	\mathbb{E}\left[\braket{O}\right] = o
\end{equation}

We can now write the contraction of a tensor network as an expectation value
of a stochastic function depending on multiple stochastic variables
$\{ \splitatcommas{G,A,I,F,B,F} \}$ which are uniformly distributed in
the following way:
\begin{equation}
\braket{E^{\mathrm{MP2,1}}} = \dfrac{N}{M} \sum_{m=1}^{M} \left.\Gamma^{*}\right.^{a_{m}g_{m}}_{i_{m}} \left.\Gamma\right.^{b_{m}}_{j_{m}g_{m}} \left.\Gamma\right.^{i_{m}}_{a_{m}f_{m}} \left.\Gamma^{*}\right.^{j_{m}f_{m}}_{b_{m}}
\label{eq:expVal_var_est}
\end{equation}
$\{ \splitatcommas{g_m,a_m,i_m,f_m,b_m,j_m} \}$ are the $m$-th realization of
the stochastic variable $\{ \splitatcommas{G,A,I,F,B,F} \}$ respectively.
The total number of index combinations $N$ is
$N_\mathrm{o}^2 \cdot N_\mathrm{v}^2 \cdot N_\mathrm{F}^2$.
This approximation can be optimized by choosing special,
non-uniform distributions of the indices of a tensor network.
This method is called importance sampling and can reduce the variance
significantly if done right.




\section{Importance sampling}
\label{sec:importanceSampling}

The main idea behind importance sampling is to prioritize regions which contribute more to the expectation value.
Let $f_X$ be the prioritized PDF of the stochastic Variable $X$ in contrast to the uniform, constant PDF $p_X$.
Weighting $X$ according to $W(x) = p_X(x)/f_X(x)$ leaves expectation values of observables $O(x)$ invariant:

\begin{multline}
	\mathbb{E}[\underbrace{O(X)W(X)}_{\tilde{O}(X)}; f_{X}]
		= \ \int_{-\infty}^{\infty}\kern-2.6em\sum dx \ O(x) \ W(x) \ f_{X}(x)
			= \ \int_{-\infty}^{\infty}\kern-2.6em\sum dx \ O(x) \  \dfrac{p_{X}(x)}{f_{X}(x)} f_{X}(x) \\
		= \ \int_{-\infty}^{\infty}\kern-2.6em\sum dx \ O(x) \  p_{X}(x) = \mathbb{E}[O(X); p_{X}]
\label{eq:exVal_is}
\end{multline}

Inspecting the variance helps to understand how a reduction of the variance is achieved and which properties we have to demand from $f_{X}$. We will start with the transformed observable $\tilde{O}(X) = O(X)W(X)$ and the corresponding PDF $f_{X}$.

\begin{align*}
	\mathbb{V}\left[ \tilde{O}(X); f_{X} \right]
		&= \mathbb{E}\left[ \tilde{O}(X)^{2}; f_{X}\right] - \mathbb{E}[\tilde{O};f_{X}]^{2} \\
		&= \ \int_{-\infty}^{\infty}\kern-2.6em\sum dx \ \tilde{O}(x)^{2} \ f_{X}(x) - \left( \ \int_{-\infty}^{\infty}\kern-2.6em\sum dx \ \tilde{O}(x) \ f_{X}(x) \right)^{2}
\end{align*}

For simplicity's sake we assume that $O(x) \geq 0$, for all $x$ lets us, in theory, choose $f_{X}(x) = \frac{O(x) p_{x}(x)}{o}$. By integrating over $x$ and using the definition of the expectation value \eqref{eq:1stMoment} we see that this PDF $f_{X}$ is indeed normalized,

\begin{equation*}
	\int_{-\infty}^{\infty}\kern-2.6em\sum dx \ f_{X}(x) = \dfrac{1}{o} \ \int_{-\infty}^{\infty}\kern-2.6em\sum dx \ O(x) \ p_{x}(x) = 1 \text{.}
\end{equation*}

The transformed observable becomes $\tilde{O}(x) = O(x) \frac{p_{X}(x)}{f_{X}(x)} = o$, i.g.~constant.
From which one can immediatly follow that the varianve vanisches for this particular choice of $f_X(x)$:
\begin{multline}
	\mathbb{V}\left[ \tilde{O}(X); f_{X} \right]
		= o^{2} \ \int_{-\infty}^{\infty}\kern-2.6em\sum dx \ f_{X}(x) - \left( o \ \int_{-\infty}^{\infty}\kern-2.6em\sum dx \ f_{X}(x) \right)^{2} \\
		= o^{2}-o^{2} = 0 \text{.}
\end{multline}


In reality this is not possible since $o$ is what we want to approximate.
Choosing $f_{X}(x)$ roughly proportional to $O(x)p_{X}(x)$, satisfying all
other characteristics of a PDF, will be sufficient enough since
$\tilde{O}(x)$ will have a smaller variance.


The same unbiased estimation of the expectation value and the variance as in Equation \eqref{eq:unbiasedEsti_expVar} can be applied for the transformed observable $\tilde{O}$:
\begin{equation}
	\braket{\tilde{O}}_{f_{X}} = \dfrac{1}{M} \sum_{i}^{M} \tilde{O}(x_{i})
	\qquad \text{and} \qquad
	S^{2}_{\tilde{O}, f_{X}} = \dfrac{1}{M-1} \sum^{M}_{i} \left( \tilde{O}(x_{i}) - \braket{\tilde{O}}_{f_{X}}\right)^{2}
\end{equation}
\cite{AC02552795, AC02445899, AC00103169, rubinstein, AC08207698, HandbookMCMC}

\section{Inversion method}

The \emph{inversion method}, also called inverse transform method, is used to generate pseudo-random numbers from any probability distribution given a CDF $F_X$.
Since $F_{X}: \mathbb{R} \rightarrow [0,1)$ its inverse is a function $F_{X}^{-1}: [0,1) \rightarrow \mathbb{R}$.
By using random variables, uniformly distributed on $[0,1)$, $U \sim \mathcal{U}(0,1)$ we are able to generate a random variable $X = F_{X}^{-1}(U)$ which are distributed according to the PDF $f_{X} = dF_X(x)/dx$.


\subsubsection{Simple Example}

As a simple example we can look at the function
$O(x) = \frac{1}{21}\left(\Theta(x + 10) - \Theta(x-10)\right)$,
with $\Theta(x)$ beeing the Heaviside step function,
on the discrete domain $[-50,50]$.
$O(x)$ is non-zero only between $[-10,10]$.
We compare calculating the expectation value $\mathbb{E}[O(X);p_{X}] = 40/2121$
once by using uniformly distributed variables and once using a truncated
normal distribution focusing on the non-zero values of $O(x)$.
% We choose $f_{X}(x) =  N\  \mathrm{exp}\left(\frac{(x-\mu)^{2}}{2 \sigma^{2}}\right)$
We choose $f_{X}(x) =  N\  \mathrm{exp}\left((x-\mu)^{2}/\left(2 \sigma^{2}\right)\right)$
where $\mu = 0$, $\sigma=10$ and the normalization constant $N$ such that
$1=\sum^{50}_{x=-50} f_{X}(x)$ is satisfied.

\begin{figure}
	\centering
	\begin{subfigure}{0.49\textwidth}
		\includegraphics[width=\textwidth]{figures/ex_inv/ex_dist}
		\caption{}
		\label{subfig:ex_dist}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		\includegraphics[width=\textwidth]{figures/ex_inv/ex_cdf}
		\caption{CDF $P_{X}$ and CDF $F_{X}$}
		\label{subfig:ex_cdf}
	\end{subfigure}
	\caption{Figure~\ref{subfig:ex_dist} shows the PDFs $p_x(x)$ and $f_x(x)$
	which will be used to stochasticly evaluate the integral over the function
	$O(x)$.
	For this estimation, when using the inversion method, the CDF of both PDFs
	are needed, which are shown in Fig.~\ref{subfig:ex_cdf}
	\vspace*{5mm}
	}
 	\label{fig:ex_dist-cdf}
\end{figure}

Since we use a non-uniformly random variable $X \sim f_{X}$
(as seen in Figure \ref{subfig:ex_amount}) we have to apply the transformation
of the observable $O \rightarrow \tilde{O} = O(x) p_{X}(x)/f_{X}(x) $ in order
to arrive at the same desired expectation value.
Figure \ref{subfig:ex_mody} shows the necessary transformation.


\begin{figure}
	\centering
	\begin{subfigure}{0.49\textwidth}
		\includegraphics[width=\textwidth]{figures/ex_inv/ex_amount.pdf}
		\caption{histogram of random number $X$ with the dashed line showing
		the expected distribution of samples}
		\label{subfig:ex_amount}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		\includegraphics[width=\textwidth]{figures/ex_inv/ex_mody.pdf}
		\caption{transformed observable $\tilde{O}(x)$}
		\label{subfig:ex_mody}
	\end{subfigure}
	\caption{}
 	\label{fig:ex_amount-mody}
\end{figure}

\begin{figure}
	\centering
	\begin{subfigure}{0.49\textwidth}
		\includegraphics[width=\textwidth]{figures/ex_inv/ex_mean.pdf}
		\caption{importance sampling}
		\label{subfig:ex_mean}
    \end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		\includegraphics[width=\textwidth]{figures/ex_inv/ex_meanNV.pdf}
		\caption{uniformly distributed sampling}
		\label{subfig:ex_meanNV}
	\end{subfigure}
	\caption{Deviation of the estimated expectation value $\braket{O}$ from
	the exact value $o=40/2121$ as a function of the number of sampled used
	for $\braket{O}$.
	Different graphs show different drawing sequences (runs).
	Note that the $\Delta O$-axis is scaled differently in both plots.
	}
 	\label{fig:ex_mean}
\end{figure}

By comparing the scale of the $\Delta O$ axis of Figure~\ref{subfig:ex_mean}
and \ref{subfig:ex_meanNV} one can see that importance sampling improves
over uniform sampling by orders of magnitude in this example.



\section{Applied distributions for importance sampling}

For a tensor network different distributions can be employed.
The only restriction on which distributions can be used depends on which indices in the configuration $\{g,a,i,f,b,j\}$ are already known.
The following examples can be applied on all the permutations of the indices as well.
Let $\{G,A,I,F,B,J\}$ denote the stochastic variables whose realizations $\{g_m, a_m, i_m, f_m, b_m, j_m\}$ will be used to estimate the contraction of the tensor network.
We will break down the six dimensional PDF into a product of simpler one or two dimensional PDFs, as in
\begin{multline}
\mathbb{P}\left( (G,A,I,F,B,J) = (g,a,i,f,b,j) \right) \\
	\propto \gamma_{G,A,I}(a) \cdot \gamma_{G, a}(i) \cdot \gamma_{a,i}(g) \cdot \gamma_{F,B,J}(a) \cdot \gamma_{F, B}(j) \cdot \gamma_{b,j}(f)
\end{multline}


\subsubsection{Uniform drawing}
In some cases runs uniform sampling was used to determine the first index on which the other PDFs may depend.

\begin{align}
	\gamma_{G,A,I}(a) = \mathbb{P}(A=a) \propto 1
\end{align}


\subsubsection{Tracing}
The tracing distributions are marginal distributions of the joint probability distribution proportional to the magnitude of the tensor $\Gamma^{a}_{i g}$, occurring in the contractions to be estimated.
The traced indices are denoted in uppercase, while the conditional indices are written in lower case.
Any summation over an arbitrary number of indices is possible with this method as e.g.
\begin{align}
	\gamma_{G,a}(i) = \mathbb{P}_a(I=i) = \mathbb{P}(I=i|A=a) \propto \sum_{g} \left| \Gamma^{a}_{ig} \right|
\end{align}
denotes the probability of finding $I=i$ under the condition of having found $A=a$ for any possible $G$ where $\mathbb{P}\left((G,A,I) = (g,a,i)\right) \propto \left| \Gamma^{a}_{i g}\right|$.

\subsubsection{Compound drawing}
Compound drawing uses conditional probability distribution and is able to determine any number of indices of a single tensor at once, where the indices which are not sampled have to be drawn before.
With the three dimensional $\Gamma$ tensor these distributions as in
\begin{align}
	\gamma_g (a,i) = \mathbb{P}_{g}(A=a,I=i) = \mathbb{P}(A=a,I=i|G=g) \propto \left| \Gamma^{a}_{ig} \right|
\end{align}
Other combinations of indices may also be used.
