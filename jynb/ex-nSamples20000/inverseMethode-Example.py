#!/usr/bin/env python
# coding: utf-8

# ${\displaystyle f(x\mid \mu ,\sigma ^{2})={\frac {1}{\sqrt {2\pi \sigma ^{2}}}}\operatorname {exp} \left(-{\frac {(x-\mu )^{2}}{2\sigma ^{2}}}\right)={\frac {1}{\sqrt {2\pi \sigma ^{2}}}}e^{-{\frac {(x-\mu )^{2}}{2\sigma ^{2}}}}\quad -\infty <x<\infty }$
# 
# 
# 
# 

# In[1]:


#import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt

saveas = 'pdf'
figuresize = (6,4)
if saveas == 'pgf':
    mpl.use("pgf")
    mpl.rcParams.update({
        "pgf.texsystem": "pdflatex",
        'font.family': 'serif',
        'text.usetex': True,
        'pgf.rcfonts': False,
    })
else:
    plt.rc('text', usetex=True)
    #plt.rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
    ## for Palatino and other serif fonts use:
    #rc('font',**{'family':'serif','serif':['Palatino']})
    #plt.rc('font',**{'family':'serif','serif':['Computer Modern Roman']})
    plt.rc('font',**{'family':'serif','serif':['DejaVu Sans']})
    # hfont = {'fontname':'Computer Modern Roman'}

size = 100 #number of x 
x_min = math.ceil(-size/2)
x_max = math.floor(size/2)

n = x_max - x_min + 1
index = np.arange(n)
x = np.arange(x_min, x_max+1)


# In[2]:


f = np.zeros(shape=(n)) # data
f[(np.arange(-10, 11)+math.ceil(size/2))] = 1
f = f/np.sum(f)

mean = 0
sig = 10
p = np.zeros(shape=(n)) # optimized sampling distribution
for i in index:
    p[i] = math.exp(-math.pow((x[i]-mean)/sig,2)/2)
p = p/np.sum(p)
    


# In[3]:


norm = 0
for i in index:
    norm += f[i]
f_norm = np.true_divide(f , norm)


plt.figure(figsize=figuresize)
plt.rcParams.update({'font.size': 16})

plt.plot(x, f, 'bo', alpha=0.35, label='$O(x)$')
plt.plot(x, np.full(x.shape,1/n), 'go', alpha=0.35, label='$p_X(x)$')
plt.plot(x, p, 'ro', alpha=0.35, label='$f_X(x)$')
plt.legend()
plt.xlabel('x')
plt.savefig('ex_dist.{0}'.format(saveas))
#plt.show()


# In[4]:


cdf = np.cumsum(p)

plt.figure(figsize=figuresize)
plt.rcParams.update({'font.size': 16})

plt.plot(x, p, 'ro', alpha=0.2, label='$f_X(x)$')
plt.plot(x, cdf, 'ro', label='$F_X(x)$')
plt.legend()
plt.xlabel('x')
plt.savefig('ex_cdf.{0}'.format(saveas))
#plt.show()


# In[9]:


nSample = 20000
nruns = 4
smp_x = [[] for i in range(nruns)]
smp = [[] for i in range(nruns)]

means = [[] for i in range(nruns)]

amount = [np.zeros(n) for i in range(nruns)]



for i in range(nruns):
    for j in range(nSample):
        idx = np.nonzero((cdf-np.random.random_sample()).clip(min=0))[0][0]
        smp_x[i].append(idx)
        amount[i][idx] += 1
        smp[i].append(f[idx]*(1/(x_max-x_min+1)/p[idx]))
        means[i].append(np.mean(smp[i]))

meanx_nv1 = np.divide(np.cumsum(np.floor(n*np.random.random_sample((nSample,)))+x_min),np.arange(nSample)+1)
meanx_nv2 = np.divide(np.cumsum(np.floor(n*np.random.random_sample((nSample,)))+x_min),np.arange(nSample)+1)
        
        

print(np.array(smp).shape)
cutoff = 4
meanx_nv1 = meanx_nv1[cutoff:]
meanx_nv2 = meanx_nv2[cutoff:]
smp_x = np.array(smp_x)[:,cutoff:]
smp = np.array(smp)[:,cutoff:]
means = np.array(means)[:,cutoff:]
nrange = np.arange(cutoff, nSample) + 1

print( "average x: {:f}".format(x_min + np.average(smp_x)))
print('mean:', np.mean(f),'vs', np.mean(smp, axis=1))
print('delta mean:', np.mean(smp, axis=1)-np.mean(f))

plt.figure(figsize=figuresize)
plt.rcParams.update({'font.size': 16})

plt.plot(nrange, meanx_nv1-np.mean(f))
plt.plot(nrange, meanx_nv2-np.mean(f))
for i in range(nruns):
    plt.plot(nrange, means[i]-np.mean(f))
plt.hlines(0,cutoff, nSample, linestyle = 'dashed')
plt.savefig('ex_mean.{0}'.format(saveas))
#plt.show()

plt.figure(figsize=figuresize)
plt.rcParams.update({'font.size': 16})

plt.bar(x, amount[0], align='edge',width=0.5)
plt.savefig('ex_amount.{0}'.format(saveas))
#plt.show()


# In[6]:


# mean plot
plt.figure(figsize=figuresize)
plt.rcParams.update({'font.size': 16})
plt.tight_layout()
plt.gcf().subplots_adjust(left=0.2)

for i in range(nruns):
    plt.plot(nrange, means[i]-np.mean(f))
plt.hlines(0,cutoff, nSample, linestyle = 'dashed')
plt.ylabel(r'$\Delta O = \langle O \rangle - o$')
plt.xlabel('$\#$ of samples')
plt.savefig('ex_mean.{0}'.format(saveas))
#plt.show()


plt.figure(figsize=figuresize)
plt.rcParams.update({'font.size': 16})
plt.tight_layout()
plt.gcf().subplots_adjust(left=0.2)

plt.plot(nrange, meanx_nv1-np.mean(f))
plt.plot(nrange, meanx_nv2-np.mean(f))
for i in range(nruns):
    plt.plot(nrange, means[i]-np.mean(f))
plt.hlines(0,cutoff, nSample, linestyle = 'dashed')
plt.ylabel(r'$\Delta O = \langle O \rangle - o$')
plt.xlabel('$\#$ of samples')
plt.savefig('ex_meanNV.{0}'.format(saveas))
#plt.show()


# amount plot

p_soll = np.zeros(shape=(n)) # optimized sampling distribution
for i in index:
    p_soll[i] = math.exp(-math.pow((x[i]-mean)/sig,2)/2)
p_soll = p_soll/np.sum(p_soll)*nSample
    

plt.figure(figsize=figuresize)
plt.rcParams.update({'font.size': 16})

plt.plot(x, p_soll, '--r', alpha=0.5)
plt.bar(x, amount[0], align='edge',width=0.5)
plt.ylabel('amount')
plt.xlabel('$x$')
#plt.legend()
plt.savefig('ex_amount.{0}'.format(saveas))
#plt.show()

# 
plt.figure(figsize=figuresize)
plt.rcParams.update({'font.size': 16})
plt.gcf().subplots_adjust(left=0.2)

#plt.semilogy(x, (1/(x_max-x_min+1)/p))
#plt.plot(x, (1/(x_max-x_min+1)/p))
plt.plot(x, (f/(x_max-x_min+1)/p), 'o', alpha=0.35)
plt.ylabel(r'$O(x) \frac{p_X(x)}{f_X(x)}$')
plt.xlabel('$\#$ of samples')
plt.savefig('ex_mody.{0}'.format(saveas))
#plt.show()


# In[7]:


np.mean(f)


# In[8]:


1/101


# In[ ]:




