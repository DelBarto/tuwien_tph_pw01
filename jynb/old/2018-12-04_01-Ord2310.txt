(-1.98298e-07,4.92733e-06)
(-1.98298e-07,-4.92733e-06)
maxi: 299007737856
maxID: [534][32][32][534][32][32] 
nbrOfSamples = 9343991808
0%: mean: 7.05605e-14 ,    variance: 1.0056e-26
 var_var: -1.86975e-53
2%: mean: 1.9716e-13 ,    variance: 6.44849e-22
 var_var: 2.91074e-47
4%: mean: 1.97629e-13 ,    variance: 6.4729e-22
 var_var: 1.52566e-47
6%: mean: 1.97659e-13 ,    variance: 6.46917e-22
 var_var: 9.8545e-48
8%: mean: 1.96648e-13 ,    variance: 6.45997e-22
 var_var: 7.4572e-48
10%: mean: 1.96028e-13 ,    variance: 6.45464e-22
 var_var: 5.90423e-48
12%: mean: 1.95865e-13 ,    variance: 6.45547e-22
 var_var: 4.86573e-48
14%: mean: 1.96384e-13 ,    variance: 6.46455e-22
 var_var: 4.25164e-48
16%: mean: 1.96666e-13 ,    variance: 6.46782e-22
 var_var: 3.71821e-48
18%: mean: 1.96376e-13 ,    variance: 6.45871e-22
 var_var: 3.28643e-48
20%: mean: 1.96375e-13 ,    variance: 6.46219e-22
 var_var: 2.94955e-48
22%: mean: 1.96389e-13 ,    variance: 6.46312e-22
 var_var: 2.68555e-48
24%: mean: 1.9626e-13 ,    variance: 6.46365e-22
 var_var: 2.48499e-48
26%: mean: 1.96299e-13 ,    variance: 6.46393e-22
 var_var: 2.27218e-48
28%: mean: 1.96307e-13 ,    variance: 6.45741e-22
 var_var: 2.08924e-48
30%: mean: 1.96171e-13 ,    variance: 6.45307e-22
 var_var: 1.94253e-48
32%: mean: 1.96088e-13 ,    variance: 6.45191e-22
 var_var: 1.81375e-48
34%: mean: 1.96068e-13 ,    variance: 6.4529e-22
 var_var: 1.69597e-48
36%: mean: 1.95884e-13 ,    variance: 6.45039e-22
 var_var: 1.59265e-48
38%: mean: 1.95984e-13 ,    variance: 6.44919e-22
 var_var: 1.49908e-48
40%: mean: 1.95889e-13 ,    variance: 6.44606e-22
 var_var: 1.41245e-48
42%: mean: 1.95935e-13 ,    variance: 6.44203e-22
 var_var: 1.33781e-48
44%: mean: 1.96003e-13 ,    variance: 6.44485e-22
 var_var: 1.28146e-48
46%: mean: 1.96019e-13 ,    variance: 6.44489e-22
 var_var: 1.22263e-48
48%: mean: 1.96027e-13 ,    variance: 6.44562e-22
 var_var: 1.17091e-48
50%: mean: 1.96019e-13 ,    variance: 6.44603e-22
 var_var: 1.12158e-48
52%: mean: 1.95918e-13 ,    variance: 6.44677e-22
 var_var: 1.07986e-48
54%: mean: 1.95908e-13 ,    variance: 6.44784e-22
 var_var: 1.03693e-48
56%: mean: 1.95966e-13 ,    variance: 6.4508e-22
 var_var: 1.00261e-48
58%: mean: 1.95921e-13 ,    variance: 6.4482e-22
 var_var: 9.66056e-49
60%: mean: 1.95893e-13 ,    variance: 6.45043e-22
 var_var: 9.3595e-49
62%: mean: 1.95895e-13 ,    variance: 6.45129e-22
 var_var: 9.06922e-49
64%: mean: 1.95928e-13 ,    variance: 6.45277e-22
 var_var: 8.77787e-49
66%: mean: 1.95945e-13 ,    variance: 6.4561e-22
 var_var: 8.53275e-49
68%: mean: 1.96118e-13 ,    variance: 6.45871e-22
 var_var: 8.26813e-49
70%: mean: 1.96114e-13 ,    variance: 6.45942e-22
 var_var: 8.01147e-49
72%: mean: 1.9611e-13 ,    variance: 6.45789e-22
 var_var: 7.77268e-49
74%: mean: 1.96113e-13 ,    variance: 6.4565e-22
 var_var: 7.5235e-49
76%: mean: 1.96096e-13 ,    variance: 6.45381e-22
 var_var: 7.2908e-49
78%: mean: 1.96021e-13 ,    variance: 6.4533e-22
 var_var: 7.07613e-49
80%: mean: 1.96072e-13 ,    variance: 6.45439e-22
 var_var: 6.89203e-49
82%: mean: 1.96024e-13 ,    variance: 6.45529e-22
 var_var: 6.75128e-49
84%: mean: 1.96027e-13 ,    variance: 6.45536e-22
 var_var: 6.57411e-49
86%: mean: 1.96037e-13 ,    variance: 6.45329e-22
 var_var: 6.3951e-49
88%: mean: 1.96076e-13 ,    variance: 6.45493e-22
 var_var: 6.23702e-49
90%: mean: 1.96083e-13 ,    variance: 6.45333e-22
 var_var: 6.06491e-49
92%: mean: 1.96081e-13 ,    variance: 6.45254e-22
 var_var: 5.92287e-49
94%: mean: 1.96068e-13 ,    variance: 6.45301e-22
 var_var: 5.77111e-49
96%: mean: 1.96076e-13 ,    variance: 6.4523e-22
 var_var: 5.64497e-49
98%: mean: 1.9613e-13 ,    variance: 6.45386e-22
 var_var: 5.53395e-49
100%: mean: 1.96115e-13 ,    variance: 6.45429e-22
 var_var: 5.41979e-49
