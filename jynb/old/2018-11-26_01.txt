(-1.98298e-07,4.92733e-06)
(-1.98298e-07,-4.92733e-06)
maxi: 299007737856
maxID: [534][32][32][534][32][32] 
nbrOfSamples = 9343991808
0%: mean: 2.10205e-14 ,    variance: 8.63147e-28
 var_var: -1.32953e-55
2%: mean: 1.94961e-13 ,    variance: 6.51631e-22
 var_var: 2.91674e-47
4%: mean: 1.95864e-13 ,    variance: 6.48113e-22
 var_var: 1.38914e-47
6%: mean: 1.95761e-13 ,    variance: 6.47504e-22
 var_var: 1.00324e-47
8%: mean: 1.96134e-13 ,    variance: 6.47754e-22
 var_var: 7.45106e-48
10%: mean: 1.96168e-13 ,    variance: 6.47774e-22
 var_var: 5.82286e-48
12%: mean: 1.96227e-13 ,    variance: 6.48218e-22
 var_var: 4.87176e-48
14%: mean: 1.96169e-13 ,    variance: 6.47368e-22
 var_var: 4.20809e-48
16%: mean: 1.96326e-13 ,    variance: 6.45952e-22
 var_var: 3.67838e-48
18%: mean: 1.96068e-13 ,    variance: 6.46198e-22
 var_var: 3.33817e-48
20%: mean: 1.96086e-13 ,    variance: 6.46413e-22
 var_var: 3.04522e-48
22%: mean: 1.96003e-13 ,    variance: 6.45968e-22
 var_var: 2.74058e-48
24%: mean: 1.95948e-13 ,    variance: 6.45941e-22
 var_var: 2.49277e-48
26%: mean: 1.9575e-13 ,    variance: 6.45485e-22
 var_var: 2.28277e-48
28%: mean: 1.95705e-13 ,    variance: 6.45583e-22
 var_var: 2.11643e-48
30%: mean: 1.95836e-13 ,    variance: 6.45777e-22
 var_var: 1.97671e-48
32%: mean: 1.95908e-13 ,    variance: 6.45437e-22
 var_var: 1.84871e-48
34%: mean: 1.95787e-13 ,    variance: 6.45132e-22
 var_var: 1.73948e-48
36%: mean: 1.95758e-13 ,    variance: 6.45449e-22
 var_var: 1.64094e-48
38%: mean: 1.95719e-13 ,    variance: 6.45488e-22
 var_var: 1.55802e-48
40%: mean: 1.95681e-13 ,    variance: 6.45746e-22
 var_var: 1.48978e-48
42%: mean: 1.95672e-13 ,    variance: 6.46011e-22
 var_var: 1.4269e-48
44%: mean: 1.95664e-13 ,    variance: 6.45941e-22
 var_var: 1.36144e-48
46%: mean: 1.95847e-13 ,    variance: 6.46502e-22
 var_var: 1.31414e-48
48%: mean: 1.95841e-13 ,    variance: 6.46521e-22
 var_var: 1.25691e-48
50%: mean: 1.95794e-13 ,    variance: 6.46273e-22
 var_var: 1.20821e-48
52%: mean: 1.95769e-13 ,    variance: 6.46158e-22
 var_var: 1.15938e-48
54%: mean: 1.95853e-13 ,    variance: 6.46274e-22
 var_var: 1.11387e-48
56%: mean: 1.95884e-13 ,    variance: 6.46238e-22
 var_var: 1.07586e-48
58%: mean: 1.95968e-13 ,    variance: 6.46362e-22
 var_var: 1.03764e-48
60%: mean: 1.95956e-13 ,    variance: 6.46268e-22
 var_var: 9.99662e-49
62%: mean: 1.95991e-13 ,    variance: 6.46391e-22
 var_var: 9.66156e-49
64%: mean: 1.95966e-13 ,    variance: 6.46521e-22
 var_var: 9.43799e-49
66%: mean: 1.95995e-13 ,    variance: 6.46447e-22
 var_var: 9.12229e-49
68%: mean: 1.96022e-13 ,    variance: 6.46236e-22
 var_var: 8.8172e-49
70%: mean: 1.96005e-13 ,    variance: 6.4598e-22
 var_var: 8.5532e-49
72%: mean: 1.9595e-13 ,    variance: 6.45979e-22
 var_var: 8.30489e-49
74%: mean: 1.96046e-13 ,    variance: 6.45997e-22
 var_var: 8.09214e-49
76%: mean: 1.96069e-13 ,    variance: 6.46168e-22
 var_var: 7.93952e-49
78%: mean: 1.96023e-13 ,    variance: 6.46176e-22
 var_var: 7.72088e-49
80%: mean: 1.96039e-13 ,    variance: 6.46176e-22
 var_var: 7.56244e-49
82%: mean: 1.96087e-13 ,    variance: 6.46447e-22
 var_var: 7.38716e-49
84%: mean: 1.96121e-13 ,    variance: 6.46441e-22
 var_var: 7.1953e-49
86%: mean: 1.96158e-13 ,    variance: 6.46632e-22
 var_var: 7.07265e-49
88%: mean: 1.96143e-13 ,    variance: 6.468e-22
 var_var: 6.9172e-49
90%: mean: 1.96127e-13 ,    variance: 6.46673e-22
 var_var: 6.76772e-49
92%: mean: 1.96156e-13 ,    variance: 6.46659e-22
 var_var: 6.61856e-49
94%: mean: 1.96187e-13 ,    variance: 6.46818e-22
 var_var: 6.48998e-49
96%: mean: 1.96246e-13 ,    variance: 6.46789e-22
 var_var: 6.34514e-49
98%: mean: 1.96234e-13 ,    variance: 6.46806e-22
 var_var: 6.20809e-49
100%: mean: 1.96227e-13 ,    variance: 6.46776e-22
 var_var: 6.07597e-49
Calculated Tsum = (0.0586645,-2.46921e-17)
Calculated Tsum = (1.96197e-13,-8.25801e-29)
