network: 
{ 1 2 3 0 0 0 }
{ 1 0 0 0 2 3 }
{ 0 0 0 1 2 3 }
{ 0 2 3 1 0 0 }

maxi: 299007737856
maxID: [534][32][32][534][32][32]
drawingOrder   :  { 2 0 1 4 3 3 }
fromWhichTensor:  { 0 0 3 2 1 1 }
drawingType    :  { c u t c c c }
tracingID      :  { 0 0 3 0 0 0 }
nbrOfSamples = 291999744
0%: mean: 3.20089e-14 ,    variance: 3.73741e-27
 var_var: 1.46389e-53
2%: mean: 2.00115e-13 ,    variance: 6.11983e-23
 var_var: 2.52088e-49
4%: mean: 1.99445e-13 ,    variance: 6.09607e-23
 var_var: 1.20657e-49
6%: mean: 1.97514e-13 ,    variance: 6.07139e-23
 var_var: 7.83285e-50
8%: mean: 1.97834e-13 ,    variance: 6.05707e-23
 var_var: 5.74402e-50
10%: mean: 1.97522e-13 ,    variance: 6.05867e-23
 var_var: 4.56241e-50
12%: mean: 1.97615e-13 ,    variance: 6.06673e-23
 var_var: 3.81131e-50
14%: mean: 1.97884e-13 ,    variance: 6.06265e-23
 var_var: 3.24012e-50
16%: mean: 1.97895e-13 ,    variance: 6.06873e-23
 var_var: 2.82733e-50
18%: mean: 1.97783e-13 ,    variance: 6.06822e-23
 var_var: 2.48864e-50
20%: mean: 1.98099e-13 ,    variance: 6.06765e-23
 var_var: 2.21994e-50
22%: mean: 1.9754e-13 ,    variance: 6.07306e-23
 var_var: 2.01721e-50
24%: mean: 1.97465e-13 ,    variance: 6.07167e-23
 var_var: 1.83507e-50
26%: mean: 1.97605e-13 ,    variance: 6.07829e-23
 var_var: 1.68471e-50
28%: mean: 1.97627e-13 ,    variance: 6.07646e-23
 var_var: 1.55586e-50
30%: mean: 1.97483e-13 ,    variance: 6.07883e-23
 var_var: 1.44926e-50
32%: mean: 1.97848e-13 ,    variance: 6.08342e-23
 var_var: 1.35076e-50
34%: mean: 1.97769e-13 ,    variance: 6.08007e-23
 var_var: 1.25853e-50
36%: mean: 1.97793e-13 ,    variance: 6.07901e-23
 var_var: 1.17592e-50
38%: mean: 1.97436e-13 ,    variance: 6.07696e-23
 var_var: 1.10532e-50
40%: mean: 1.97175e-13 ,    variance: 6.07457e-23
 var_var: 1.04155e-50
42%: mean: 1.97195e-13 ,    variance: 6.07453e-23
 var_var: 9.84835e-51
44%: mean: 1.97134e-13 ,    variance: 6.07758e-23
 var_var: 9.34527e-51
46%: mean: 1.9722e-13 ,    variance: 6.07903e-23
 var_var: 8.85808e-51
48%: mean: 1.97149e-13 ,    variance: 6.07796e-23
 var_var: 8.42537e-51
50%: mean: 1.97386e-13 ,    variance: 6.0817e-23
 var_var: 8.05261e-51
52%: mean: 1.9736e-13 ,    variance: 6.08295e-23
 var_var: 7.68508e-51
54%: mean: 1.97541e-13 ,    variance: 6.08432e-23
 var_var: 7.35174e-51
56%: mean: 1.97523e-13 ,    variance: 6.08379e-23
 var_var: 7.0198e-51
58%: mean: 1.97607e-13 ,    variance: 6.08518e-23
 var_var: 6.72457e-51
60%: mean: 1.97635e-13 ,    variance: 6.08793e-23
 var_var: 6.46449e-51
62%: mean: 1.97693e-13 ,    variance: 6.08861e-23
 var_var: 6.2087e-51
64%: mean: 1.97531e-13 ,    variance: 6.086e-23
 var_var: 5.95171e-51
66%: mean: 1.9753e-13 ,    variance: 6.08696e-23
 var_var: 5.71592e-51
68%: mean: 1.97541e-13 ,    variance: 6.08858e-23
 var_var: 5.50966e-51
70%: mean: 1.97647e-13 ,    variance: 6.09006e-23
 var_var: 5.31315e-51
72%: mean: 1.97527e-13 ,    variance: 6.08893e-23
 var_var: 5.12464e-51
74%: mean: 1.97571e-13 ,    variance: 6.08855e-23
 var_var: 4.9375e-51
76%: mean: 1.97514e-13 ,    variance: 6.08976e-23
 var_var: 4.76519e-51
78%: mean: 1.97597e-13 ,    variance: 6.08993e-23
 var_var: 4.59807e-51
80%: mean: 1.97718e-13 ,    variance: 6.08983e-23
 var_var: 4.44439e-51
82%: mean: 1.97695e-13 ,    variance: 6.08836e-23
 var_var: 4.288e-51
84%: mean: 1.97621e-13 ,    variance: 6.09004e-23
 var_var: 4.14932e-51
86%: mean: 1.97654e-13 ,    variance: 6.09069e-23
 var_var: 4.01342e-51
88%: mean: 1.97607e-13 ,    variance: 6.08983e-23
 var_var: 3.88123e-51
90%: mean: 1.97656e-13 ,    variance: 6.09196e-23
 var_var: 3.7688e-51
92%: mean: 1.9762e-13 ,    variance: 6.09274e-23
 var_var: 3.65074e-51
94%: mean: 1.97597e-13 ,    variance: 6.09238e-23
 var_var: 3.53682e-51
96%: mean: 1.97551e-13 ,    variance: 6.09159e-23
 var_var: 3.42359e-51
98%: mean: 1.97471e-13 ,    variance: 6.09209e-23
 var_var: 3.31948e-51
100%: mean: 1.9739e-13 ,    variance: 6.09019e-23
 var_var: 3.2159e-51
