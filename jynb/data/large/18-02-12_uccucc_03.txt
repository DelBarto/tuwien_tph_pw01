network: 
{ 1 2 3 0 0 0 }
{ 1 0 0 0 2 3 }
{ 0 0 0 1 2 3 }
{ 0 2 3 1 0 0 }

maxi: 1196030951424
maxID: [534][64][32][534][64][32]
drawingOrder   :  { 0 1 1 2 3 3 }
fromWhichTensor:  { 0 0 0 2 2 2 }
drawingType    :  { u c c u c c }
tracingID      :  { 0 0 0 0 0 0 }
nbrOfSamples = 1167998976
0%: mean: 6.00407e-17 ,    variance: 5.44254e-33
 var_var: -1.16562e-65
1%: mean: 8.50608e-13 ,    variance: 4.46805e-20
 var_var: 1.97822e-41
2%: mean: 8.04244e-13 ,    variance: 4.60757e-20
 var_var: 1.04459e-41
3%: mean: 8.22891e-13 ,    variance: 4.55049e-20
 var_var: 8.56444e-42
4%: mean: 8.62047e-13 ,    variance: 5.39492e-20
 var_var: 2.90318e-41
5%: mean: 8.6328e-13 ,    variance: 5.40229e-20
 var_var: 2.26148e-41
6%: mean: 8.76375e-13 ,    variance: 5.39558e-20
 var_var: 1.69545e-41
7%: mean: 8.79237e-13 ,    variance: 5.64064e-20
 var_var: 1.50256e-41
8%: mean: 8.80778e-13 ,    variance: 5.70494e-20
 var_var: 1.39826e-41
9%: mean: 8.82931e-13 ,    variance: 5.6782e-20
 var_var: 1.18524e-41
10%: mean: 8.77546e-13 ,    variance: 5.5841e-20
 var_var: 9.91921e-42
11%: mean: 8.71715e-13 ,    variance: 5.53595e-20
 var_var: 8.68354e-42
12%: mean: 8.83552e-13 ,    variance: 5.6289e-20
 var_var: 7.88143e-42
13%: mean: 8.80352e-13 ,    variance: 5.6178e-20
 var_var: 7.29738e-42
14%: mean: 8.83462e-13 ,    variance: 5.61703e-20
 var_var: 6.4882e-42
15%: mean: 8.87491e-13 ,    variance: 5.66861e-20
 var_var: 6.14339e-42
16%: mean: 8.86507e-13 ,    variance: 5.63445e-20
 var_var: 5.78116e-42
17%: mean: 8.85643e-13 ,    variance: 5.60243e-20
 var_var: 5.21928e-42
18%: mean: 8.82191e-13 ,    variance: 5.57079e-20
 var_var: 4.8708e-42
19%: mean: 8.83202e-13 ,    variance: 5.57409e-20
 var_var: 4.62937e-42
20%: mean: 8.85482e-13 ,    variance: 5.54372e-20
 var_var: 4.2599e-42
21%: mean: 8.84457e-13 ,    variance: 5.52192e-20
 var_var: 4.11701e-42
22%: mean: 8.85778e-13 ,    variance: 5.51167e-20
 var_var: 3.88637e-42
23%: mean: 8.80398e-13 ,    variance: 5.50564e-20
 var_var: 3.6972e-42
24%: mean: 8.78912e-13 ,    variance: 5.50271e-20
 var_var: 3.47849e-42
25%: mean: 8.77475e-13 ,    variance: 5.51743e-20
 var_var: 3.5543e-42
26%: mean: 8.82264e-13 ,    variance: 5.61026e-20
 var_var: 3.6808e-42
27%: mean: 8.8228e-13 ,    variance: 5.58858e-20
 var_var: 3.48836e-42
28%: mean: 8.80304e-13 ,    variance: 5.56817e-20
 var_var: 3.30254e-42
29%: mean: 8.83666e-13 ,    variance: 5.60388e-20
 var_var: 3.37209e-42
30%: mean: 8.87749e-13 ,    variance: 5.64271e-20
 var_var: 3.37068e-42
31%: mean: 8.8859e-13 ,    variance: 5.64878e-20
 var_var: 3.26433e-42
32%: mean: 8.88451e-13 ,    variance: 5.69762e-20
 var_var: 3.82725e-42
33%: mean: 8.88753e-13 ,    variance: 5.65622e-20
 var_var: 3.62343e-42
34%: mean: 8.88912e-13 ,    variance: 5.64961e-20
 var_var: 3.48932e-42
35%: mean: 8.88848e-13 ,    variance: 5.61856e-20
 var_var: 3.31604e-42
36%: mean: 8.87677e-13 ,    variance: 5.59538e-20
 var_var: 3.17635e-42
37%: mean: 8.90318e-13 ,    variance: 5.60024e-20
 var_var: 3.04062e-42
38%: mean: 8.89823e-13 ,    variance: 5.56967e-20
 var_var: 2.89909e-42
39%: mean: 8.88854e-13 ,    variance: 5.57596e-20
 var_var: 2.83218e-42
40%: mean: 8.88598e-13 ,    variance: 5.6114e-20
 var_var: 2.8111e-42
41%: mean: 8.88333e-13 ,    variance: 5.59239e-20
 var_var: 2.69544e-42
42%: mean: 8.88406e-13 ,    variance: 5.63291e-20
 var_var: 2.75952e-42
43%: mean: 8.88907e-13 ,    variance: 5.64155e-20
 var_var: 2.69369e-42
44%: mean: 8.86604e-13 ,    variance: 5.6225e-20
 var_var: 2.60161e-42
45%: mean: 8.88574e-13 ,    variance: 5.64905e-20
 var_var: 2.55626e-42
46%: mean: 8.87743e-13 ,    variance: 5.62759e-20
 var_var: 2.4634e-42
47%: mean: 8.86559e-13 ,    variance: 5.60151e-20
 var_var: 2.37513e-42
48%: mean: 8.87277e-13 ,    variance: 5.61631e-20
 var_var: 2.33131e-42
49%: mean: 8.87879e-13 ,    variance: 5.62949e-20
 var_var: 2.30084e-42
50%: mean: 8.87123e-13 ,    variance: 5.60749e-20
 var_var: 2.22217e-42
51%: mean: 8.88176e-13 ,    variance: 5.6148e-20
 var_var: 2.17179e-42
52%: mean: 8.86485e-13 ,    variance: 5.58186e-20
 var_var: 2.0965e-42
53%: mean: 8.86514e-13 ,    variance: 5.59539e-20
 var_var: 2.07273e-42
54%: mean: 8.84935e-13 ,    variance: 5.56624e-20
 var_var: 2.00271e-42
55%: mean: 8.85098e-13 ,    variance: 5.56039e-20
 var_var: 1.94563e-42
56%: mean: 8.86854e-13 ,    variance: 5.59811e-20
 var_var: 2.02567e-42
57%: mean: 8.85605e-13 ,    variance: 5.57206e-20
 var_var: 1.96414e-42
58%: mean: 8.86026e-13 ,    variance: 5.5758e-20
 var_var: 1.92337e-42
59%: mean: 8.86241e-13 ,    variance: 5.57807e-20
 var_var: 1.88055e-42
60%: mean: 8.87364e-13 ,    variance: 5.60026e-20
 var_var: 1.93528e-42
61%: mean: 8.87001e-13 ,    variance: 5.58682e-20
 var_var: 1.88283e-42
62%: mean: 8.86515e-13 ,    variance: 5.57549e-20
 var_var: 1.83513e-42
63%: mean: 8.86906e-13 ,    variance: 5.60171e-20
 var_var: 1.85387e-42
64%: mean: 8.85202e-13 ,    variance: 5.56879e-20
 var_var: 1.79823e-42
65%: mean: 8.85385e-13 ,    variance: 5.57145e-20
 var_var: 1.75806e-42
66%: mean: 8.8629e-13 ,    variance: 5.59249e-20
 var_var: 1.74484e-42
67%: mean: 8.87067e-13 ,    variance: 5.61873e-20
 var_var: 1.73166e-42
68%: mean: 8.88145e-13 ,    variance: 5.65454e-20
 var_var: 1.85032e-42
69%: mean: 8.87149e-13 ,    variance: 5.63825e-20
 var_var: 1.8064e-42
70%: mean: 8.88004e-13 ,    variance: 5.66001e-20
 var_var: 1.79231e-42
71%: mean: 8.87047e-13 ,    variance: 5.64332e-20
 var_var: 1.7478e-42
72%: mean: 8.86034e-13 ,    variance: 5.64166e-20
 var_var: 1.71801e-42
73%: mean: 8.8583e-13 ,    variance: 5.63951e-20
 var_var: 1.68968e-42
74%: mean: 8.84598e-13 ,    variance: 5.62899e-20
 var_var: 1.65143e-42
75%: mean: 8.84489e-13 ,    variance: 5.61586e-20
 var_var: 1.61451e-42
76%: mean: 8.83941e-13 ,    variance: 5.61154e-20
 var_var: 1.58443e-42
77%: mean: 8.83511e-13 ,    variance: 5.58984e-20
 var_var: 1.54696e-42
78%: mean: 8.82681e-13 ,    variance: 5.57192e-20
 var_var: 1.5101e-42
79%: mean: 8.82727e-13 ,    variance: 5.57262e-20
 var_var: 1.4901e-42
80%: mean: 8.81454e-13 ,    variance: 5.56804e-20
 var_var: 1.46565e-42
81%: mean: 8.81501e-13 ,    variance: 5.56464e-20
 var_var: 1.4393e-42
82%: mean: 8.82304e-13 ,    variance: 5.59163e-20
 var_var: 1.44712e-42
83%: mean: 8.82146e-13 ,    variance: 5.57658e-20
 var_var: 1.41483e-42
84%: mean: 8.82147e-13 ,    variance: 5.58393e-20
 var_var: 1.39751e-42
85%: mean: 8.82693e-13 ,    variance: 5.5898e-20
 var_var: 1.37903e-42
86%: mean: 8.81479e-13 ,    variance: 5.5746e-20
 var_var: 1.35006e-42
87%: mean: 8.81875e-13 ,    variance: 5.57413e-20
 var_var: 1.32575e-42
88%: mean: 8.80388e-13 ,    variance: 5.55936e-20
 var_var: 1.29968e-42
89%: mean: 8.80683e-13 ,    variance: 5.56527e-20
 var_var: 1.28432e-42
90%: mean: 8.79688e-13 ,    variance: 5.55717e-20
 var_var: 1.26272e-42
91%: mean: 8.80109e-13 ,    variance: 5.55289e-20
 var_var: 1.24342e-42
92%: mean: 8.81612e-13 ,    variance: 5.57803e-20
 var_var: 1.3003e-42
93%: mean: 8.81394e-13 ,    variance: 5.56049e-20
 var_var: 1.2739e-42
94%: mean: 8.81123e-13 ,    variance: 5.54923e-20
 var_var: 1.2507e-42
95%: mean: 8.81715e-13 ,    variance: 5.55123e-20
 var_var: 1.23376e-42
96%: mean: 8.80596e-13 ,    variance: 5.53647e-20
 var_var: 1.21238e-42
97%: mean: 8.81479e-13 ,    variance: 5.56104e-20
 var_var: 1.23718e-42
98%: mean: 8.81607e-13 ,    variance: 5.5612e-20
 var_var: 1.22147e-42
99%: mean: 8.80483e-13 ,    variance: 5.54587e-20
 var_var: 1.19917e-42
100%: mean: 8.79383e-13 ,    variance: 5.52562e-20
 var_var: 1.17671e-42
