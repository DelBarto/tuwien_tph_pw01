network: 
{ 1 2 3 0 0 0 }
{ 1 0 0 0 2 3 }
{ 0 0 0 1 2 3 }
{ 0 2 3 1 0 0 }

maxi: 1196030951424
maxID: [534][64][32][534][64][32]
drawingOrder   :  { 2 0 1 4 3 3 }
fromWhichTensor:  { 0 0 3 2 1 1 }
drawingType    :  { c u t c c c }
tracingID      :  { 0 0 3 0 0 0 }
nbrOfSamples = 46719959040
0%: mean: 1.06524e-12 ,    variance: 2.34852e-24
 var_var: -9.33238e-49
1%: mean: 8.76324e-13 ,    variance: 4.05853e-22
 var_var: -3.09222e-49
2%: mean: 8.76803e-13 ,    variance: 4.05856e-22
 var_var: -3.89299e-49
3%: mean: 8.76559e-13 ,    variance: 4.05798e-22
 var_var: -4.15876e-49
4%: mean: 8.76408e-13 ,    variance: 4.05762e-22
 var_var: -4.29195e-49
5%: mean: 8.76201e-13 ,    variance: 4.05797e-22
 var_var: -4.37124e-49
6%: mean: 8.76187e-13 ,    variance: 4.05637e-22
 var_var: -4.42428e-49
7%: mean: 8.76164e-13 ,    variance: 4.0561e-22
 var_var: -4.46134e-49
8%: mean: 8.76011e-13 ,    variance: 4.05624e-22
 var_var: -4.48893e-49
9%: mean: 8.7608e-13 ,    variance: 4.05601e-22
 var_var: -4.51029e-49
10%: mean: 8.76227e-13 ,    variance: 4.05617e-22
 var_var: -4.53849e-49
11%: mean: 8.76228e-13 ,    variance: 4.05622e-22
 var_var: -4.5524e-49
12%: mean: 8.76094e-13 ,    variance: 4.05545e-22
 var_var: -4.56399e-49
13%: mean: 8.76114e-13 ,    variance: 4.05535e-22
 var_var: -4.57373e-49
14%: mean: 8.76052e-13 ,    variance: 4.05542e-22
 var_var: -4.58212e-49
15%: mean: 8.76181e-13 ,    variance: 4.05608e-22
 var_var: -4.5895e-49
16%: mean: 8.76197e-13 ,    variance: 4.05603e-22
 var_var: -4.5962e-49
17%: mean: 8.76202e-13 ,    variance: 4.05605e-22
 var_var: -4.60203e-49
18%: mean: 8.76102e-13 ,    variance: 4.05608e-22
 var_var: -4.60725e-49
19%: mean: 8.76095e-13 ,    variance: 4.05615e-22
 var_var: -4.61189e-49
20%: mean: 8.76084e-13 ,    variance: 4.05618e-22
 var_var: -4.60547e-49
21%: mean: 8.76091e-13 ,    variance: 4.05628e-22
 var_var: -4.60929e-49
22%: mean: 8.76091e-13 ,    variance: 4.05599e-22
 var_var: -4.61276e-49
23%: mean: 8.76085e-13 ,    variance: 4.05605e-22
 var_var: -4.61589e-49
24%: mean: 8.76014e-13 ,    variance: 4.05577e-22
 var_var: -4.61874e-49
25%: mean: 8.76044e-13 ,    variance: 4.05585e-22
 var_var: -4.62138e-49
26%: mean: 8.76072e-13 ,    variance: 4.05593e-22
 var_var: -4.62384e-49
27%: mean: 8.76098e-13 ,    variance: 4.05602e-22
 var_var: -4.62616e-49
28%: mean: 8.76104e-13 ,    variance: 4.05617e-22
 var_var: -4.6283e-49
29%: mean: 8.76106e-13 ,    variance: 4.05614e-22
 var_var: -4.6303e-49
30%: mean: 8.76131e-13 ,    variance: 4.05631e-22
 var_var: -4.64255e-49
31%: mean: 8.76149e-13 ,    variance: 4.05636e-22
 var_var: -4.6443e-49
32%: mean: 8.76141e-13 ,    variance: 4.05619e-22
 var_var: -4.64596e-49
33%: mean: 8.76175e-13 ,    variance: 4.0562e-22
 var_var: -4.64749e-49
34%: mean: 8.76196e-13 ,    variance: 4.05616e-22
 var_var: -4.64892e-49
35%: mean: 8.76163e-13 ,    variance: 4.05592e-22
 var_var: -4.65028e-49
36%: mean: 8.76174e-13 ,    variance: 4.05608e-22
 var_var: -4.65154e-49
37%: mean: 8.76166e-13 ,    variance: 4.05613e-22
 var_var: -4.65276e-49
38%: mean: 8.76177e-13 ,    variance: 4.05599e-22
 var_var: -4.65391e-49
39%: mean: 8.76163e-13 ,    variance: 4.056e-22
 var_var: -4.655e-49
40%: mean: 8.76149e-13 ,    variance: 4.05595e-22
 var_var: -4.64575e-49
41%: mean: 8.76131e-13 ,    variance: 4.05589e-22
 var_var: -4.64674e-49
42%: mean: 8.76118e-13 ,    variance: 4.05567e-22
 var_var: -4.6477e-49
43%: mean: 8.76087e-13 ,    variance: 4.05561e-22
 var_var: -4.64859e-49
44%: mean: 8.76072e-13 ,    variance: 4.05565e-22
 var_var: -4.64944e-49
45%: mean: 8.76104e-13 ,    variance: 4.05568e-22
 var_var: -4.65025e-49
46%: mean: 8.7611e-13 ,    variance: 4.0557e-22
 var_var: -4.65102e-49
47%: mean: 8.76111e-13 ,    variance: 4.05572e-22
 var_var: -4.65176e-49
48%: mean: 8.76114e-13 ,    variance: 4.05576e-22
 var_var: -4.65246e-49
49%: mean: 8.76109e-13 ,    variance: 4.05567e-22
 var_var: -4.66339e-49
50%: mean: 8.76141e-13 ,    variance: 4.0558e-22
 var_var: -4.66404e-49
51%: mean: 8.76133e-13 ,    variance: 4.05565e-22
 var_var: -4.66466e-49
52%: mean: 8.76129e-13 ,    variance: 4.0557e-22
 var_var: -4.66525e-49
53%: mean: 8.76118e-13 ,    variance: 4.05573e-22
 var_var: -4.66582e-49
54%: mean: 8.76101e-13 ,    variance: 4.05563e-22
 var_var: -4.66638e-49
55%: mean: 8.7609e-13 ,    variance: 4.05566e-22
 var_var: -4.66691e-49
56%: mean: 8.76106e-13 ,    variance: 4.05583e-22
 var_var: -4.66741e-49
57%: mean: 8.76118e-13 ,    variance: 4.05595e-22
 var_var: -4.66791e-49
58%: mean: 8.7611e-13 ,    variance: 4.05587e-22
 var_var: -4.6684e-49
59%: mean: 8.76142e-13 ,    variance: 4.05603e-22
 var_var: -4.65866e-49
60%: mean: 8.7615e-13 ,    variance: 4.05613e-22
 var_var: -4.65913e-49
61%: mean: 8.76137e-13 ,    variance: 4.05599e-22
 var_var: -4.65958e-49
62%: mean: 8.76114e-13 ,    variance: 4.05587e-22
 var_var: -4.66002e-49
63%: mean: 8.76107e-13 ,    variance: 4.0559e-22
 var_var: -4.66043e-49
64%: mean: 8.761e-13 ,    variance: 4.05588e-22
 var_var: -4.66084e-49
65%: mean: 8.76093e-13 ,    variance: 4.05576e-22
 var_var: -4.66124e-49
66%: mean: 8.76103e-13 ,    variance: 4.05577e-22
 var_var: -4.66162e-49
67%: mean: 8.76125e-13 ,    variance: 4.05585e-22
 var_var: -4.66199e-49
68%: mean: 8.76135e-13 ,    variance: 4.05588e-22
 var_var: -4.66236e-49
69%: mean: 8.76148e-13 ,    variance: 4.05596e-22
 var_var: -4.67289e-49
70%: mean: 8.76167e-13 ,    variance: 4.05591e-22
 var_var: -4.67324e-49
71%: mean: 8.76184e-13 ,    variance: 4.05594e-22
 var_var: -4.67357e-49
72%: mean: 8.76183e-13 ,    variance: 4.05594e-22
 var_var: -4.67389e-49
73%: mean: 8.76193e-13 ,    variance: 4.05599e-22
 var_var: -4.6742e-49
74%: mean: 8.76197e-13 ,    variance: 4.05589e-22
 var_var: -4.67451e-49
75%: mean: 8.76204e-13 ,    variance: 4.0559e-22
 var_var: -4.6748e-49
76%: mean: 8.76189e-13 ,    variance: 4.05584e-22
 var_var: -4.67509e-49
77%: mean: 8.76206e-13 ,    variance: 4.05591e-22
 var_var: -4.67537e-49
78%: mean: 8.762e-13 ,    variance: 4.05589e-22
 var_var: -4.67565e-49
79%: mean: 8.76187e-13 ,    variance: 4.05588e-22
 var_var: -4.66575e-49
80%: mean: 8.76182e-13 ,    variance: 4.05594e-22
 var_var: -4.66601e-49
81%: mean: 8.7618e-13 ,    variance: 4.05596e-22
 var_var: -4.66626e-49
82%: mean: 8.76178e-13 ,    variance: 4.05599e-22
 var_var: -4.66651e-49
83%: mean: 8.76186e-13 ,    variance: 4.05609e-22
 var_var: -4.66676e-49
84%: mean: 8.76207e-13 ,    variance: 4.05617e-22
 var_var: -4.667e-49
85%: mean: 8.762e-13 ,    variance: 4.0561e-22
 var_var: -4.66724e-49
86%: mean: 8.76197e-13 ,    variance: 4.05614e-22
 var_var: -4.66747e-49
87%: mean: 8.76204e-13 ,    variance: 4.05618e-22
 var_var: -4.6677e-49
88%: mean: 8.76199e-13 ,    variance: 4.05616e-22
 var_var: -4.67807e-49
89%: mean: 8.76198e-13 ,    variance: 4.05616e-22
 var_var: -4.67829e-49
90%: mean: 8.76208e-13 ,    variance: 4.05614e-22
 var_var: -4.67851e-49
91%: mean: 8.76184e-13 ,    variance: 4.05605e-22
 var_var: -4.67871e-49
92%: mean: 8.76192e-13 ,    variance: 4.05609e-22
 var_var: -4.67891e-49
93%: mean: 8.76189e-13 ,    variance: 4.05605e-22
 var_var: -4.67911e-49
94%: mean: 8.76193e-13 ,    variance: 4.05609e-22
 var_var: -4.6793e-49
95%: mean: 8.7621e-13 ,    variance: 4.05614e-22
 var_var: -4.67949e-49
96%: mean: 8.762e-13 ,    variance: 4.05609e-22
 var_var: -4.67968e-49
97%: mean: 8.76199e-13 ,    variance: 4.05609e-22
 var_var: -4.67986e-49
98%: mean: 8.76188e-13 ,    variance: 4.05597e-22
 var_var: -4.66991e-49
99%: mean: 8.762e-13 ,    variance: 4.056e-22
 var_var: -4.67008e-49
100%: mean: 8.76196e-13 ,    variance: 4.05603e-22
 var_var: -4.67026e-49
