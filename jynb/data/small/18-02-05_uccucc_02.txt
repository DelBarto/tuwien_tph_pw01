network: 
{ 1 2 3 0 0 0 }
{ 1 0 0 0 2 3 }
{ 0 0 0 1 2 3 }
{ 0 2 3 1 0 0 }

maxi: 299007737856
maxID: [534][32][32][534][32][32]
drawingOrder   :  { 0 1 1 2 3 3 }
fromWhichTensor:  { 0 0 0 2 2 2 }
drawingType    :  { u c c u c c }
tracingID      :  { 0 0 0 0 0 0 }
nbrOfSamples = 291999744
0%: mean: 6.57504e-14 ,    variance: 8.6295e-27
 var_var: -1.39568e-53
2%: mean: 2.1116e-13 ,    variance: 2.54321e-21
 var_var: 5.27639e-44
4%: mean: 2.05379e-13 ,    variance: 2.72467e-21
 var_var: 4.55661e-44
6%: mean: 2.09886e-13 ,    variance: 2.75461e-21
 var_var: 3.2458e-44
8%: mean: 1.99685e-13 ,    variance: 2.86517e-21
 var_var: 2.91048e-44
10%: mean: 2.01152e-13 ,    variance: 3.03676e-21
 var_var: 3.76252e-44
12%: mean: 2.03072e-13 ,    variance: 3.08948e-21
 var_var: 3.1735e-44
14%: mean: 2.04779e-13 ,    variance: 3.1676e-21
 var_var: 3.25742e-44
16%: mean: 2.04372e-13 ,    variance: 3.14726e-21
 var_var: 2.63337e-44
18%: mean: 2.05964e-13 ,    variance: 3.24166e-21
 var_var: 2.46309e-44
20%: mean: 2.06097e-13 ,    variance: 3.23043e-21
 var_var: 2.1966e-44
22%: mean: 2.04552e-13 ,    variance: 3.23654e-21
 var_var: 1.97125e-44
24%: mean: 2.01051e-13 ,    variance: 3.21126e-21
 var_var: 1.71541e-44
26%: mean: 2.04044e-13 ,    variance: 3.19283e-21
 var_var: 1.51593e-44
28%: mean: 2.03869e-13 ,    variance: 3.22098e-21
 var_var: 1.72214e-44
30%: mean: 2.03399e-13 ,    variance: 3.24611e-21
 var_var: 1.59648e-44
32%: mean: 2.03257e-13 ,    variance: 3.23169e-21
 var_var: 1.43263e-44
34%: mean: 2.02723e-13 ,    variance: 3.19437e-21
 var_var: 1.29039e-44
36%: mean: 2.00531e-13 ,    variance: 3.19991e-21
 var_var: 1.22249e-44
38%: mean: 2.0081e-13 ,    variance: 3.20812e-21
 var_var: 1.23756e-44
40%: mean: 1.99743e-13 ,    variance: 3.18497e-21
 var_var: 1.20394e-44
42%: mean: 1.98559e-13 ,    variance: 3.17819e-21
 var_var: 1.12222e-44
44%: mean: 1.98102e-13 ,    variance: 3.18477e-21
 var_var: 1.19092e-44
46%: mean: 1.98698e-13 ,    variance: 3.23469e-21
 var_var: 1.18709e-44
48%: mean: 1.99088e-13 ,    variance: 3.25718e-21
 var_var: 1.16414e-44
50%: mean: 1.98952e-13 ,    variance: 3.24299e-21
 var_var: 1.08533e-44
52%: mean: 1.98498e-13 ,    variance: 3.22865e-21
 var_var: 1.01509e-44
54%: mean: 1.98944e-13 ,    variance: 3.23178e-21
 var_var: 9.61259e-45
56%: mean: 1.99266e-13 ,    variance: 3.26583e-21
 var_var: 9.97382e-45
58%: mean: 1.98079e-13 ,    variance: 3.24312e-21
 var_var: 9.44265e-45
60%: mean: 1.97698e-13 ,    variance: 3.23353e-21
 var_var: 8.97558e-45
62%: mean: 1.97368e-13 ,    variance: 3.23415e-21
 var_var: 8.5749e-45
64%: mean: 1.9682e-13 ,    variance: 3.20292e-21
 var_var: 8.0981e-45
66%: mean: 1.97801e-13 ,    variance: 3.23363e-21
 var_var: 8.48447e-45
68%: mean: 1.96938e-13 ,    variance: 3.22457e-21
 var_var: 8.114e-45
70%: mean: 1.96891e-13 ,    variance: 3.22381e-21
 var_var: 7.76748e-45
72%: mean: 1.9696e-13 ,    variance: 3.21758e-21
 var_var: 7.42274e-45
74%: mean: 1.96764e-13 ,    variance: 3.22134e-21
 var_var: 7.21345e-45
76%: mean: 1.96019e-13 ,    variance: 3.22356e-21
 var_var: 7.06877e-45
78%: mean: 1.96204e-13 ,    variance: 3.22084e-21
 var_var: 6.86811e-45
80%: mean: 1.95734e-13 ,    variance: 3.21176e-21
 var_var: 6.6331e-45
82%: mean: 1.95883e-13 ,    variance: 3.20988e-21
 var_var: 6.40911e-45
84%: mean: 1.95559e-13 ,    variance: 3.19638e-21
 var_var: 6.14295e-45
86%: mean: 1.95322e-13 ,    variance: 3.19128e-21
 var_var: 5.96823e-45
88%: mean: 1.94859e-13 ,    variance: 3.18056e-21
 var_var: 5.73343e-45
90%: mean: 1.9453e-13 ,    variance: 3.17048e-21
 var_var: 5.5101e-45
92%: mean: 1.94051e-13 ,    variance: 3.17017e-21
 var_var: 5.39829e-45
94%: mean: 1.93817e-13 ,    variance: 3.17159e-21
 var_var: 5.25814e-45
96%: mean: 1.9394e-13 ,    variance: 3.17024e-21
 var_var: 5.17047e-45
98%: mean: 1.94616e-13 ,    variance: 3.1836e-21
 var_var: 5.1029e-45
100%: mean: 1.94405e-13 ,    variance: 3.17465e-21
 var_var: 4.92965e-45
