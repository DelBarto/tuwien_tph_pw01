network: 
{ 1 2 3 0 0 0 }
{ 1 0 0 0 2 3 }
{ 0 0 0 1 2 3 }
{ 0 2 3 1 0 0 }

maxi: 299007737856
maxID: [534][32][32][534][32][32]
drawingOrder   :  { 2 1 0 4 3 3 }
fromWhichTensor:  { 0 3 0 2 1 1 }
drawingType    :  { t t T c c c }
tracingID      :  { 2 3 0 0 0 0 }
nbrOfSamples = 1167998976
0%: mean: 6.83969e-13 ,    variance: 7.74188e-25
 var_var: 6.40796e-49
1%: mean: 1.93633e-13 ,    variance: 9.90405e-23
 var_var: 4.0013e-48
2%: mean: 1.93013e-13 ,    variance: 9.82518e-23
 var_var: 1.89172e-48
3%: mean: 1.93558e-13 ,    variance: 9.84586e-23
 var_var: 1.23451e-48
4%: mean: 1.93062e-13 ,    variance: 9.71721e-23
 var_var: 7.40249e-49
5%: mean: 1.93758e-13 ,    variance: 9.69922e-23
 var_var: 4.37885e-49
6%: mean: 1.94965e-13 ,    variance: 9.71925e-23
 var_var: 2.90685e-49
7%: mean: 1.95367e-13 ,    variance: 9.70458e-23
 var_var: 1.75066e-49
8%: mean: 1.9553e-13 ,    variance: 9.69437e-23
 var_var: 9.92498e-50
9%: mean: 1.95611e-13 ,    variance: 9.66357e-23
 var_var: 3.19121e-50
10%: mean: 1.95644e-13 ,    variance: 9.67867e-23
 var_var: -4.26842e-51
11%: mean: 1.95393e-13 ,    variance: 9.67597e-23
 var_var: -3.38416e-50
12%: mean: 1.95153e-13 ,    variance: 9.65589e-23
 var_var: -6.3228e-50
13%: mean: 1.95648e-13 ,    variance: 9.66868e-23
 var_var: -7.99994e-50
14%: mean: 1.95667e-13 ,    variance: 9.65564e-23
 var_var: -1.02396e-49
15%: mean: 1.95664e-13 ,    variance: 9.67119e-23
 var_var: -1.13451e-49
16%: mean: 1.95745e-13 ,    variance: 9.66761e-23
 var_var: -1.31575e-49
17%: mean: 1.96108e-13 ,    variance: 9.68397e-23
 var_var: -1.46234e-49
18%: mean: 1.96116e-13 ,    variance: 9.68555e-23
 var_var: -1.57569e-49
19%: mean: 1.96146e-13 ,    variance: 9.67919e-23
 var_var: -1.71096e-49
20%: mean: 1.96134e-13 ,    variance: 9.67664e-23
 var_var: -1.83343e-49
21%: mean: 1.96243e-13 ,    variance: 9.68107e-23
 var_var: -1.92177e-49
22%: mean: 1.96221e-13 ,    variance: 9.67553e-23
 var_var: -2.00322e-49
23%: mean: 1.96219e-13 ,    variance: 9.67033e-23
 var_var: -2.10251e-49
24%: mean: 1.96202e-13 ,    variance: 9.66341e-23
 var_var: -2.18537e-49
25%: mean: 1.96166e-13 ,    variance: 9.67342e-23
 var_var: -2.22361e-49
26%: mean: 1.96269e-13 ,    variance: 9.67289e-23
 var_var: -2.29349e-49
27%: mean: 1.96136e-13 ,    variance: 9.67083e-23
 var_var: -2.35454e-49
28%: mean: 1.96287e-13 ,    variance: 9.67049e-23
 var_var: -2.40661e-49
29%: mean: 1.96151e-13 ,    variance: 9.66794e-23
 var_var: -2.44173e-49
30%: mean: 1.96112e-13 ,    variance: 9.66837e-23
 var_var: -2.4716e-49
31%: mean: 1.96045e-13 ,    variance: 9.67351e-23
 var_var: -2.51117e-49
32%: mean: 1.96185e-13 ,    variance: 9.67541e-23
 var_var: -2.5543e-49
33%: mean: 1.96044e-13 ,    variance: 9.67433e-23
 var_var: -2.58834e-49
34%: mean: 1.96026e-13 ,    variance: 9.67678e-23
 var_var: -2.62239e-49
35%: mean: 1.95966e-13 ,    variance: 9.67716e-23
 var_var: -2.64336e-49
36%: mean: 1.95897e-13 ,    variance: 9.67146e-23
 var_var: -2.67712e-49
37%: mean: 1.95925e-13 ,    variance: 9.66908e-23
 var_var: -2.70366e-49
38%: mean: 1.95905e-13 ,    variance: 9.66827e-23
 var_var: -2.73303e-49
39%: mean: 1.95879e-13 ,    variance: 9.67333e-23
 var_var: -2.74039e-49
40%: mean: 1.959e-13 ,    variance: 9.67369e-23
 var_var: -2.76309e-49
41%: mean: 1.95904e-13 ,    variance: 9.67149e-23
 var_var: -2.79093e-49
42%: mean: 1.95885e-13 ,    variance: 9.66364e-23
 var_var: -2.81947e-49
43%: mean: 1.95832e-13 ,    variance: 9.65416e-23
 var_var: -2.8499e-49
44%: mean: 1.95817e-13 ,    variance: 9.65321e-23
 var_var: -2.87194e-49
45%: mean: 1.95782e-13 ,    variance: 9.65601e-23
 var_var: -2.88839e-49
46%: mean: 1.95813e-13 ,    variance: 9.65843e-23
 var_var: -2.90296e-49
47%: mean: 1.95935e-13 ,    variance: 9.65869e-23
 var_var: -2.92055e-49
48%: mean: 1.95925e-13 ,    variance: 9.66208e-23
 var_var: -2.93707e-49
49%: mean: 1.95946e-13 ,    variance: 9.66186e-23
 var_var: -2.95424e-49
50%: mean: 1.95979e-13 ,    variance: 9.66326e-23
 var_var: -2.96854e-49
51%: mean: 1.95946e-13 ,    variance: 9.66497e-23
 var_var: -2.98136e-49
52%: mean: 1.95918e-13 ,    variance: 9.66167e-23
 var_var: -2.99856e-49
53%: mean: 1.95934e-13 ,    variance: 9.66373e-23
 var_var: -3.00735e-49
54%: mean: 1.95984e-13 ,    variance: 9.66815e-23
 var_var: -3.01984e-49
55%: mean: 1.95967e-13 ,    variance: 9.66708e-23
 var_var: -3.03415e-49
56%: mean: 1.95992e-13 ,    variance: 9.66965e-23
 var_var: -3.04114e-49
57%: mean: 1.9597e-13 ,    variance: 9.67171e-23
 var_var: -3.05034e-49
58%: mean: 1.95962e-13 ,    variance: 9.6703e-23
 var_var: -3.06301e-49
59%: mean: 1.95959e-13 ,    variance: 9.67106e-23
 var_var: -3.07273e-49
60%: mean: 1.96002e-13 ,    variance: 9.67785e-23
 var_var: -3.07773e-49
61%: mean: 1.95939e-13 ,    variance: 9.67731e-23
 var_var: -3.08797e-49
62%: mean: 1.95905e-13 ,    variance: 9.67557e-23
 var_var: -3.09815e-49
63%: mean: 1.95908e-13 ,    variance: 9.67288e-23
 var_var: -3.11015e-49
64%: mean: 1.95906e-13 ,    variance: 9.67163e-23
 var_var: -3.12095e-49
65%: mean: 1.95915e-13 ,    variance: 9.67359e-23
 var_var: -3.12952e-49
66%: mean: 1.95913e-13 ,    variance: 9.67559e-23
 var_var: -3.1363e-49
67%: mean: 1.959e-13 ,    variance: 9.67442e-23
 var_var: -3.144e-49
68%: mean: 1.95935e-13 ,    variance: 9.67772e-23
 var_var: -3.15121e-49
69%: mean: 1.95934e-13 ,    variance: 9.67426e-23
 var_var: -3.16219e-49
70%: mean: 1.95931e-13 ,    variance: 9.67451e-23
 var_var: -3.16971e-49
71%: mean: 1.95915e-13 ,    variance: 9.67297e-23
 var_var: -3.17411e-49
72%: mean: 1.95919e-13 ,    variance: 9.67347e-23
 var_var: -3.18231e-49
73%: mean: 1.9584e-13 ,    variance: 9.67024e-23
 var_var: -3.19143e-49
74%: mean: 1.95826e-13 ,    variance: 9.66917e-23
 var_var: -3.19981e-49
75%: mean: 1.95827e-13 ,    variance: 9.67163e-23
 var_var: -3.20554e-49
76%: mean: 1.95774e-13 ,    variance: 9.66796e-23
 var_var: -3.21231e-49
77%: mean: 1.95742e-13 ,    variance: 9.66458e-23
 var_var: -3.22125e-49
78%: mean: 1.95716e-13 ,    variance: 9.66209e-23
 var_var: -3.22875e-49
79%: mean: 1.95713e-13 ,    variance: 9.66091e-23
 var_var: -3.23558e-49
80%: mean: 1.95736e-13 ,    variance: 9.664e-23
 var_var: -3.24138e-49
81%: mean: 1.95763e-13 ,    variance: 9.66461e-23
 var_var: -3.24848e-49
82%: mean: 1.95748e-13 ,    variance: 9.66605e-23
 var_var: -3.25335e-49
83%: mean: 1.95701e-13 ,    variance: 9.66524e-23
 var_var: -3.25856e-49
84%: mean: 1.95698e-13 ,    variance: 9.66466e-23
 var_var: -3.26482e-49
85%: mean: 1.9569e-13 ,    variance: 9.66586e-23
 var_var: -3.2695e-49
86%: mean: 1.95753e-13 ,    variance: 9.6666e-23
 var_var: -3.27554e-49
87%: mean: 1.95739e-13 ,    variance: 9.66792e-23
 var_var: -3.28039e-49
88%: mean: 1.95784e-13 ,    variance: 9.66943e-23
 var_var: -3.28538e-49
89%: mean: 1.95734e-13 ,    variance: 9.66708e-23
 var_var: -3.2906e-49
90%: mean: 1.95738e-13 ,    variance: 9.66906e-23
 var_var: -3.29503e-49
91%: mean: 1.95769e-13 ,    variance: 9.67396e-23
 var_var: -3.29699e-49
92%: mean: 1.95786e-13 ,    variance: 9.67383e-23
 var_var: -3.30229e-49
93%: mean: 1.95775e-13 ,    variance: 9.67298e-23
 var_var: -3.30645e-49
94%: mean: 1.95795e-13 ,    variance: 9.67148e-23
 var_var: -3.31098e-49
95%: mean: 1.95773e-13 ,    variance: 9.67116e-23
 var_var: -3.31542e-49
96%: mean: 1.95781e-13 ,    variance: 9.67286e-23
 var_var: -3.31904e-49
97%: mean: 1.9576e-13 ,    variance: 9.67489e-23
 var_var: -3.32264e-49
98%: mean: 1.95779e-13 ,    variance: 9.67753e-23
 var_var: -3.32514e-49
99%: mean: 1.95789e-13 ,    variance: 9.67614e-23
 var_var: -3.32907e-49
100%: mean: 1.95791e-13 ,    variance: 9.6789e-23
 var_var: -3.3313e-49
