network: 
{ 1 2 3 0 0 0 }
{ 1 0 0 0 2 3 }
{ 0 0 0 1 2 3 }
{ 0 2 3 1 0 0 }

maxi: 299007737856
maxID: [534][32][32][534][32][32]
drawingOrder   :  { 2 1 0 4 3 3 }
fromWhichTensor:  { 0 3 0 2 1 1 }
drawingType    :  { t t u c c c }
tracingID      :  { 2 3 0 0 0 0 }
nbrOfSamples = 1167998976
0%: mean: 1.36873e-13 ,    variance: 1.64401e-25
 var_var: 1.14925e-50
1%: mean: 1.94639e-13 ,    variance: 1.10783e-22
 var_var: 2.55235e-48
2%: mean: 1.949e-13 ,    variance: 1.11433e-22
 var_var: 1.33926e-48
3%: mean: 1.95912e-13 ,    variance: 1.11288e-22
 var_var: 8.40534e-49
4%: mean: 1.94798e-13 ,    variance: 1.11318e-22
 var_var: 6.14929e-49
5%: mean: 1.94537e-13 ,    variance: 1.11942e-22
 var_var: 5.01868e-49
6%: mean: 1.94355e-13 ,    variance: 1.11626e-22
 var_var: 4.04594e-49
7%: mean: 1.93646e-13 ,    variance: 1.11333e-22
 var_var: 3.47949e-49
8%: mean: 1.94286e-13 ,    variance: 1.1165e-22
 var_var: 3.08765e-49
9%: mean: 1.9478e-13 ,    variance: 1.11814e-22
 var_var: 2.76354e-49
10%: mean: 1.94714e-13 ,    variance: 1.11821e-22
 var_var: 2.4991e-49
11%: mean: 1.94728e-13 ,    variance: 1.11766e-22
 var_var: 2.22787e-49
12%: mean: 1.94875e-13 ,    variance: 1.12045e-22
 var_var: 2.08414e-49
13%: mean: 1.95063e-13 ,    variance: 1.11965e-22
 var_var: 1.92724e-49
14%: mean: 1.95348e-13 ,    variance: 1.12272e-22
 var_var: 1.81935e-49
15%: mean: 1.95343e-13 ,    variance: 1.12067e-22
 var_var: 1.66998e-49
16%: mean: 1.95323e-13 ,    variance: 1.12054e-22
 var_var: 1.56163e-49
17%: mean: 1.95329e-13 ,    variance: 1.12191e-22
 var_var: 1.48458e-49
18%: mean: 1.95512e-13 ,    variance: 1.12142e-22
 var_var: 1.39903e-49
19%: mean: 1.9554e-13 ,    variance: 1.12192e-22
 var_var: 1.33164e-49
20%: mean: 1.9566e-13 ,    variance: 1.12217e-22
 var_var: 1.26393e-49
21%: mean: 1.95608e-13 ,    variance: 1.12177e-22
 var_var: 1.19814e-49
22%: mean: 1.95714e-13 ,    variance: 1.12192e-22
 var_var: 1.1391e-49
23%: mean: 1.95711e-13 ,    variance: 1.12201e-22
 var_var: 1.08657e-49
24%: mean: 1.95892e-13 ,    variance: 1.12331e-22
 var_var: 1.04764e-49
25%: mean: 1.95775e-13 ,    variance: 1.1235e-22
 var_var: 1.01023e-49
26%: mean: 1.95873e-13 ,    variance: 1.12435e-22
 var_var: 9.7699e-50
27%: mean: 1.95919e-13 ,    variance: 1.12483e-22
 var_var: 9.41211e-50
28%: mean: 1.95939e-13 ,    variance: 1.12416e-22
 var_var: 9.04937e-50
29%: mean: 1.95898e-13 ,    variance: 1.12413e-22
 var_var: 8.71028e-50
30%: mean: 1.95877e-13 ,    variance: 1.12446e-22
 var_var: 8.46688e-50
31%: mean: 1.95903e-13 ,    variance: 1.12411e-22
 var_var: 8.17872e-50
32%: mean: 1.95914e-13 ,    variance: 1.12415e-22
 var_var: 7.92875e-50
33%: mean: 1.95888e-13 ,    variance: 1.12401e-22
 var_var: 7.67745e-50
34%: mean: 1.95953e-13 ,    variance: 1.12373e-22
 var_var: 7.41296e-50
35%: mean: 1.95943e-13 ,    variance: 1.12418e-22
 var_var: 7.17586e-50
36%: mean: 1.9582e-13 ,    variance: 1.12348e-22
 var_var: 6.928e-50
37%: mean: 1.95738e-13 ,    variance: 1.12287e-22
 var_var: 6.69974e-50
38%: mean: 1.95763e-13 ,    variance: 1.12325e-22
 var_var: 6.51506e-50
39%: mean: 1.95869e-13 ,    variance: 1.12363e-22
 var_var: 6.32256e-50
40%: mean: 1.95877e-13 ,    variance: 1.12346e-22
 var_var: 6.1509e-50
41%: mean: 1.95791e-13 ,    variance: 1.12396e-22
 var_var: 6.01828e-50
42%: mean: 1.95844e-13 ,    variance: 1.12423e-22
 var_var: 5.8615e-50
43%: mean: 1.95869e-13 ,    variance: 1.12449e-22
 var_var: 5.72325e-50
44%: mean: 1.95938e-13 ,    variance: 1.12407e-22
 var_var: 5.55971e-50
45%: mean: 1.95988e-13 ,    variance: 1.12432e-22
 var_var: 5.43819e-50
46%: mean: 1.95915e-13 ,    variance: 1.1238e-22
 var_var: 5.29658e-50
47%: mean: 1.95956e-13 ,    variance: 1.12415e-22
 var_var: 5.18321e-50
48%: mean: 1.95939e-13 ,    variance: 1.12438e-22
 var_var: 5.08176e-50
49%: mean: 1.95972e-13 ,    variance: 1.12424e-22
 var_var: 4.95846e-50
50%: mean: 1.9589e-13 ,    variance: 1.12385e-22
 var_var: 4.8417e-50
51%: mean: 1.9593e-13 ,    variance: 1.12394e-22
 var_var: 4.74567e-50
52%: mean: 1.95964e-13 ,    variance: 1.12379e-22
 var_var: 4.65091e-50
53%: mean: 1.95982e-13 ,    variance: 1.12392e-22
 var_var: 4.553e-50
54%: mean: 1.96044e-13 ,    variance: 1.12435e-22
 var_var: 4.46814e-50
55%: mean: 1.96022e-13 ,    variance: 1.12455e-22
 var_var: 4.39478e-50
56%: mean: 1.96055e-13 ,    variance: 1.12481e-22
 var_var: 4.31245e-50
57%: mean: 1.96053e-13 ,    variance: 1.12487e-22
 var_var: 4.23023e-50
58%: mean: 1.96053e-13 ,    variance: 1.12449e-22
 var_var: 4.14331e-50
59%: mean: 1.96093e-13 ,    variance: 1.12474e-22
 var_var: 4.07608e-50
60%: mean: 1.96114e-13 ,    variance: 1.12464e-22
 var_var: 4.01022e-50
61%: mean: 1.961e-13 ,    variance: 1.12464e-22
 var_var: 3.93055e-50
62%: mean: 1.96181e-13 ,    variance: 1.12491e-22
 var_var: 3.85926e-50
63%: mean: 1.96173e-13 ,    variance: 1.12483e-22
 var_var: 3.79523e-50
64%: mean: 1.96142e-13 ,    variance: 1.12502e-22
 var_var: 3.74447e-50
65%: mean: 1.96253e-13 ,    variance: 1.12518e-22
 var_var: 3.67624e-50
66%: mean: 1.96296e-13 ,    variance: 1.12514e-22
 var_var: 3.62056e-50
67%: mean: 1.96317e-13 ,    variance: 1.12524e-22
 var_var: 3.55895e-50
68%: mean: 1.96309e-13 ,    variance: 1.12535e-22
 var_var: 3.50159e-50
69%: mean: 1.96316e-13 ,    variance: 1.1255e-22
 var_var: 3.44598e-50
70%: mean: 1.96332e-13 ,    variance: 1.12548e-22
 var_var: 3.39361e-50
71%: mean: 1.96321e-13 ,    variance: 1.12531e-22
 var_var: 3.33887e-50
72%: mean: 1.96362e-13 ,    variance: 1.12521e-22
 var_var: 3.28677e-50
73%: mean: 1.9632e-13 ,    variance: 1.12528e-22
 var_var: 3.23487e-50
74%: mean: 1.96318e-13 ,    variance: 1.12545e-22
 var_var: 3.19351e-50
75%: mean: 1.96263e-13 ,    variance: 1.12539e-22
 var_var: 3.14697e-50
76%: mean: 1.96293e-13 ,    variance: 1.12538e-22
 var_var: 3.10252e-50
77%: mean: 1.96241e-13 ,    variance: 1.12539e-22
 var_var: 3.05749e-50
78%: mean: 1.96176e-13 ,    variance: 1.1251e-22
 var_var: 3.00664e-50
79%: mean: 1.96174e-13 ,    variance: 1.12518e-22
 var_var: 2.96442e-50
80%: mean: 1.96149e-13 ,    variance: 1.12513e-22
 var_var: 2.91634e-50
81%: mean: 1.96142e-13 ,    variance: 1.12511e-22
 var_var: 2.8772e-50
82%: mean: 1.96122e-13 ,    variance: 1.12498e-22
 var_var: 2.83683e-50
83%: mean: 1.96134e-13 ,    variance: 1.12504e-22
 var_var: 2.80262e-50
84%: mean: 1.96142e-13 ,    variance: 1.12493e-22
 var_var: 2.76135e-50
85%: mean: 1.96153e-13 ,    variance: 1.12484e-22
 var_var: 2.72275e-50
86%: mean: 1.96099e-13 ,    variance: 1.1244e-22
 var_var: 2.68617e-50
87%: mean: 1.96131e-13 ,    variance: 1.12416e-22
 var_var: 2.64631e-50
88%: mean: 1.96082e-13 ,    variance: 1.1239e-22
 var_var: 2.60727e-50
89%: mean: 1.96109e-13 ,    variance: 1.12391e-22
 var_var: 2.57666e-50
90%: mean: 1.96052e-13 ,    variance: 1.12377e-22
 var_var: 2.54342e-50
91%: mean: 1.9611e-13 ,    variance: 1.12393e-22
 var_var: 2.51171e-50
92%: mean: 1.96111e-13 ,    variance: 1.12379e-22
 var_var: 2.47955e-50
93%: mean: 1.96129e-13 ,    variance: 1.12396e-22
 var_var: 2.44882e-50
94%: mean: 1.96128e-13 ,    variance: 1.12388e-22
 var_var: 2.41606e-50
95%: mean: 1.96128e-13 ,    variance: 1.12388e-22
 var_var: 2.39149e-50
96%: mean: 1.96146e-13 ,    variance: 1.12388e-22
 var_var: 2.36601e-50
97%: mean: 1.96142e-13 ,    variance: 1.12373e-22
 var_var: 2.33601e-50
98%: mean: 1.96168e-13 ,    variance: 1.12359e-22
 var_var: 2.30778e-50
99%: mean: 1.96123e-13 ,    variance: 1.12352e-22
 var_var: 2.27875e-50
100%: mean: 1.96118e-13 ,    variance: 1.1234e-22
 var_var: 2.24959e-50
