network: 
{ 1 2 3 0 0 0 }
{ 1 0 0 0 2 3 }
{ 0 0 0 1 2 3 }
{ 0 2 3 1 0 0 }

maxi: 299007737856
maxID: [534][32][32][534][32][32]
drawingOrder   :  { 2 1 0 4 3 3 }
fromWhichTensor:  { 0 3 0 2 1 1 }
drawingType    :  { t t u c c c }
tracingID      :  { 2 3 0 0 0 0 }
nbrOfSamples = 6131994624
0%: mean: 3.60962e-14 ,    variance: 2.51294e-27
 var_var: -1.2192e-54
1%: mean: 1.96854e-13 ,    variance: 1.12256e-22
 var_var: 5.07971e-49
2%: mean: 1.97057e-13 ,    variance: 1.13085e-22
 var_var: 2.4888e-49
3%: mean: 1.96755e-13 ,    variance: 1.12892e-22
 var_var: 1.58844e-49
4%: mean: 1.96447e-13 ,    variance: 1.12659e-22
 var_var: 1.14343e-49
5%: mean: 1.96441e-13 ,    variance: 1.12727e-22
 var_var: 9.01136e-50
6%: mean: 1.95945e-13 ,    variance: 1.12563e-22
 var_var: 7.27167e-50
7%: mean: 1.95996e-13 ,    variance: 1.12535e-22
 var_var: 6.07746e-50
8%: mean: 1.96286e-13 ,    variance: 1.12599e-22
 var_var: 5.20742e-50
9%: mean: 1.96199e-13 ,    variance: 1.12555e-22
 var_var: 4.50918e-50
10%: mean: 1.96351e-13 ,    variance: 1.12582e-22
 var_var: 3.94516e-50
11%: mean: 1.96438e-13 ,    variance: 1.12557e-22
 var_var: 3.45367e-50
12%: mean: 1.96402e-13 ,    variance: 1.12544e-22
 var_var: 3.07255e-50
13%: mean: 1.96285e-13 ,    variance: 1.12468e-22
 var_var: 2.72242e-50
14%: mean: 1.96298e-13 ,    variance: 1.12467e-22
 var_var: 2.45019e-50
15%: mean: 1.96266e-13 ,    variance: 1.12512e-22
 var_var: 2.21278e-50
16%: mean: 1.96288e-13 ,    variance: 1.12567e-22
 var_var: 2.0218e-50
17%: mean: 1.9632e-13 ,    variance: 1.12551e-22
 var_var: 1.84022e-50
18%: mean: 1.96266e-13 ,    variance: 1.1251e-22
 var_var: 1.6808e-50
19%: mean: 1.96196e-13 ,    variance: 1.12535e-22
 var_var: 1.54071e-50
20%: mean: 1.96232e-13 ,    variance: 1.125e-22
 var_var: 1.40082e-50
21%: mean: 1.96259e-13 ,    variance: 1.1247e-22
 var_var: 1.27868e-50
22%: mean: 1.96287e-13 ,    variance: 1.12481e-22
 var_var: 1.17145e-50
23%: mean: 1.96306e-13 ,    variance: 1.1252e-22
 var_var: 1.08149e-50
24%: mean: 1.9625e-13 ,    variance: 1.12504e-22
 var_var: 9.89123e-51
25%: mean: 1.9628e-13 ,    variance: 1.12506e-22
 var_var: 9.03573e-51
26%: mean: 1.96255e-13 ,    variance: 1.12534e-22
 var_var: 8.27402e-51
27%: mean: 1.96265e-13 ,    variance: 1.12548e-22
 var_var: 7.54587e-51
28%: mean: 1.96272e-13 ,    variance: 1.12538e-22
 var_var: 6.88164e-51
29%: mean: 1.96287e-13 ,    variance: 1.12508e-22
 var_var: 6.2545e-51
30%: mean: 1.96339e-13 ,    variance: 1.12505e-22
 var_var: 5.66884e-51
31%: mean: 1.96312e-13 ,    variance: 1.12522e-22
 var_var: 5.1465e-51
32%: mean: 1.96321e-13 ,    variance: 1.12534e-22
 var_var: 4.64652e-51
33%: mean: 1.96269e-13 ,    variance: 1.12513e-22
 var_var: 4.13596e-51
34%: mean: 1.96186e-13 ,    variance: 1.12479e-22
 var_var: 3.66409e-51
35%: mean: 1.96185e-13 ,    variance: 1.1248e-22
 var_var: 3.2482e-51
36%: mean: 1.96193e-13 ,    variance: 1.12484e-22
 var_var: 2.86855e-51
37%: mean: 1.96169e-13 ,    variance: 1.12466e-22
 var_var: 2.48017e-51
38%: mean: 1.962e-13 ,    variance: 1.12475e-22
 var_var: 2.11896e-51
39%: mean: 1.96177e-13 ,    variance: 1.1244e-22
 var_var: 1.77375e-51
40%: mean: 1.96138e-13 ,    variance: 1.12465e-22
 var_var: 1.48119e-51
41%: mean: 1.96133e-13 ,    variance: 1.12483e-22
 var_var: 1.18521e-51
42%: mean: 1.96135e-13 ,    variance: 1.12488e-22
 var_var: 8.84911e-52
43%: mean: 1.96125e-13 ,    variance: 1.12516e-22
 var_var: 6.35255e-52
44%: mean: 1.96142e-13 ,    variance: 1.12534e-22
 var_var: 3.81344e-52
45%: mean: 1.96209e-13 ,    variance: 1.12542e-22
 var_var: 1.19586e-52
46%: mean: 1.9618e-13 ,    variance: 1.12537e-22
 var_var: -1.17222e-52
47%: mean: 1.96193e-13 ,    variance: 1.12533e-22
 var_var: -3.5144e-52
48%: mean: 1.96226e-13 ,    variance: 1.1257e-22
 var_var: -5.43467e-52
49%: mean: 1.96177e-13 ,    variance: 1.12573e-22
 var_var: -7.61074e-52
50%: mean: 1.96176e-13 ,    variance: 1.12591e-22
 var_var: -9.45991e-52
51%: mean: 1.96169e-13 ,    variance: 1.12592e-22
 var_var: -1.13241e-51
52%: mean: 1.9617e-13 ,    variance: 1.126e-22
 var_var: -1.32412e-51
53%: mean: 1.96203e-13 ,    variance: 1.12611e-22
 var_var: -1.50142e-51
54%: mean: 1.96158e-13 ,    variance: 1.12597e-22
 var_var: -1.68007e-51
55%: mean: 1.9617e-13 ,    variance: 1.12605e-22
 var_var: -1.84879e-51
56%: mean: 1.96202e-13 ,    variance: 1.12601e-22
 var_var: -2.01027e-51
57%: mean: 1.96207e-13 ,    variance: 1.12602e-22
 var_var: -2.17439e-51
58%: mean: 1.96204e-13 ,    variance: 1.12607e-22
 var_var: -2.33581e-51
59%: mean: 1.96204e-13 ,    variance: 1.12617e-22
 var_var: -2.4796e-51
60%: mean: 1.96203e-13 ,    variance: 1.1263e-22
 var_var: -2.6167e-51
61%: mean: 1.96201e-13 ,    variance: 1.12624e-22
 var_var: -2.76134e-51
62%: mean: 1.96195e-13 ,    variance: 1.12616e-22
 var_var: -2.89709e-51
63%: mean: 1.96229e-13 ,    variance: 1.12625e-22
 var_var: -3.01973e-51
64%: mean: 1.96216e-13 ,    variance: 1.12624e-22
 var_var: -3.14765e-51
65%: mean: 1.96188e-13 ,    variance: 1.12607e-22
 var_var: -3.27834e-51
66%: mean: 1.96208e-13 ,    variance: 1.12594e-22
 var_var: -3.40243e-51
67%: mean: 1.9624e-13 ,    variance: 1.12608e-22
 var_var: -3.51826e-51
68%: mean: 1.9626e-13 ,    variance: 1.1261e-22
 var_var: -3.62813e-51
69%: mean: 1.9626e-13 ,    variance: 1.12591e-22
 var_var: -3.74746e-51
70%: mean: 1.96254e-13 ,    variance: 1.12589e-22
 var_var: -3.85349e-51
71%: mean: 1.96272e-13 ,    variance: 1.12587e-22
 var_var: -3.95698e-51
72%: mean: 1.9627e-13 ,    variance: 1.12596e-22
 var_var: -4.05158e-51
73%: mean: 1.96286e-13 ,    variance: 1.12601e-22
 var_var: -4.1471e-51
74%: mean: 1.96273e-13 ,    variance: 1.12601e-22
 var_var: -4.24128e-51
75%: mean: 1.96262e-13 ,    variance: 1.12599e-22
 var_var: -4.3494e-51
76%: mean: 1.96258e-13 ,    variance: 1.12606e-22
 var_var: -4.42569e-51
77%: mean: 1.96241e-13 ,    variance: 1.12611e-22
 var_var: -4.50963e-51
78%: mean: 1.96255e-13 ,    variance: 1.12625e-22
 var_var: -4.58901e-51
79%: mean: 1.9626e-13 ,    variance: 1.12622e-22
 var_var: -4.67081e-51
80%: mean: 1.96279e-13 ,    variance: 1.12633e-22
 var_var: -4.74762e-51
81%: mean: 1.96262e-13 ,    variance: 1.12622e-22
 var_var: -4.83184e-51
82%: mean: 1.96291e-13 ,    variance: 1.12627e-22
 var_var: -4.9062e-51
83%: mean: 1.96292e-13 ,    variance: 1.12635e-22
 var_var: -4.97779e-51
84%: mean: 1.96318e-13 ,    variance: 1.12637e-22
 var_var: -5.05082e-51
85%: mean: 1.96333e-13 ,    variance: 1.12635e-22
 var_var: -5.12277e-51
86%: mean: 1.96332e-13 ,    variance: 1.12628e-22
 var_var: -5.19845e-51
87%: mean: 1.96342e-13 ,    variance: 1.12638e-22
 var_var: -5.26611e-51
88%: mean: 1.96347e-13 ,    variance: 1.12632e-22
 var_var: -5.33465e-51
89%: mean: 1.96346e-13 ,    variance: 1.12636e-22
 var_var: -5.39726e-51
90%: mean: 1.96353e-13 ,    variance: 1.12638e-22
 var_var: -5.46332e-51
91%: mean: 1.96341e-13 ,    variance: 1.12632e-22
 var_var: -5.52616e-51
92%: mean: 1.96348e-13 ,    variance: 1.12645e-22
 var_var: -5.58477e-51
93%: mean: 1.96357e-13 ,    variance: 1.12645e-22
 var_var: -5.64536e-51
94%: mean: 1.96374e-13 ,    variance: 1.12651e-22
 var_var: -5.70217e-51
95%: mean: 1.96358e-13 ,    variance: 1.12653e-22
 var_var: -5.75835e-51
96%: mean: 1.96363e-13 ,    variance: 1.12669e-22
 var_var: -5.80905e-51
97%: mean: 1.96361e-13 ,    variance: 1.12677e-22
 var_var: -5.8603e-51
98%: mean: 1.96377e-13 ,    variance: 1.12685e-22
 var_var: -5.91048e-51
99%: mean: 1.96379e-13 ,    variance: 1.12688e-22
 var_var: -5.96125e-51
100%: mean: 1.96373e-13 ,    variance: 1.12685e-22
 var_var: -6.01041e-51
