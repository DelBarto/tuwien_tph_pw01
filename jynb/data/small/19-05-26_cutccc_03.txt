network: 
{ 1 2 3 0 0 0 }
{ 1 0 0 0 2 3 }
{ 0 0 0 1 2 3 }
{ 0 2 3 1 0 0 }

maxi: 299007737856
maxID: [534][32][32][534][32][32]
drawingOrder   :  { 2 0 1 4 3 3 }
fromWhichTensor:  { 0 0 3 2 1 1 }
drawingType    :  { c u t c c c }
tracingID      :  { 0 0 3 0 0 0 }
nbrOfSamples = 1167998976
0%: mean: 1.13166e-12 ,    variance: 2.45859e-24
 var_var: -1.30412e-48
1%: mean: 1.9383e-13 ,    variance: 6.09837e-23
 var_var: -5.58263e-49
2%: mean: 1.94036e-13 ,    variance: 6.0635e-23
 var_var: -6.18596e-49
3%: mean: 1.94304e-13 ,    variance: 6.06013e-23
 var_var: -6.37163e-49
4%: mean: 1.94623e-13 ,    variance: 6.07073e-23
 var_var: -6.4636e-49
5%: mean: 1.94809e-13 ,    variance: 6.07211e-23
 var_var: -6.52518e-49
6%: mean: 1.94744e-13 ,    variance: 6.07235e-23
 var_var: -6.56663e-49
7%: mean: 1.94757e-13 ,    variance: 6.0706e-23
 var_var: -6.59449e-49
8%: mean: 1.94669e-13 ,    variance: 6.06088e-23
 var_var: -6.61539e-49
9%: mean: 1.94927e-13 ,    variance: 6.06239e-23
 var_var: -6.62988e-49
10%: mean: 1.94997e-13 ,    variance: 6.06249e-23
 var_var: -6.64188e-49
11%: mean: 1.95215e-13 ,    variance: 6.06196e-23
 var_var: -6.65213e-49
12%: mean: 1.95295e-13 ,    variance: 6.06604e-23
 var_var: -6.66041e-49
13%: mean: 1.95288e-13 ,    variance: 6.07089e-23
 var_var: -6.66826e-49
14%: mean: 1.9533e-13 ,    variance: 6.06799e-23
 var_var: -6.67527e-49
15%: mean: 1.95397e-13 ,    variance: 6.06652e-23
 var_var: -6.68094e-49
16%: mean: 1.95517e-13 ,    variance: 6.06594e-23
 var_var: -6.68574e-49
17%: mean: 1.9568e-13 ,    variance: 6.06924e-23
 var_var: -6.6902e-49
18%: mean: 1.95742e-13 ,    variance: 6.07122e-23
 var_var: -6.69436e-49
19%: mean: 1.95966e-13 ,    variance: 6.07285e-23
 var_var: -6.69823e-49
20%: mean: 1.96005e-13 ,    variance: 6.07185e-23
 var_var: -6.70175e-49
21%: mean: 1.9595e-13 ,    variance: 6.07141e-23
 var_var: -6.7049e-49
22%: mean: 1.96028e-13 ,    variance: 6.07382e-23
 var_var: -6.70772e-49
23%: mean: 1.95934e-13 ,    variance: 6.07581e-23
 var_var: -6.71033e-49
24%: mean: 1.95917e-13 ,    variance: 6.07465e-23
 var_var: -6.71293e-49
25%: mean: 1.95952e-13 ,    variance: 6.07386e-23
 var_var: -6.71525e-49
26%: mean: 1.95946e-13 ,    variance: 6.0719e-23
 var_var: -6.71726e-49
27%: mean: 1.95828e-13 ,    variance: 6.07033e-23
 var_var: -6.71907e-49
28%: mean: 1.95881e-13 ,    variance: 6.07238e-23
 var_var: -6.72068e-49
29%: mean: 1.95952e-13 ,    variance: 6.07336e-23
 var_var: -6.72227e-49
30%: mean: 1.961e-13 ,    variance: 6.07275e-23
 var_var: -6.72377e-49
31%: mean: 1.96048e-13 ,    variance: 6.07271e-23
 var_var: -6.72522e-49
32%: mean: 1.95965e-13 ,    variance: 6.0721e-23
 var_var: -6.72651e-49
33%: mean: 1.96107e-13 ,    variance: 6.07223e-23
 var_var: -6.72771e-49
34%: mean: 1.96173e-13 ,    variance: 6.07264e-23
 var_var: -6.72887e-49
35%: mean: 1.96167e-13 ,    variance: 6.07179e-23
 var_var: -6.72994e-49
36%: mean: 1.96183e-13 ,    variance: 6.07176e-23
 var_var: -6.73092e-49
37%: mean: 1.96146e-13 ,    variance: 6.07094e-23
 var_var: -6.73186e-49
38%: mean: 1.96199e-13 ,    variance: 6.07054e-23
 var_var: -6.73267e-49
39%: mean: 1.96173e-13 ,    variance: 6.06955e-23
 var_var: -6.73336e-49
40%: mean: 1.9613e-13 ,    variance: 6.06774e-23
 var_var: -6.73402e-49
41%: mean: 1.9612e-13 ,    variance: 6.06839e-23
 var_var: -6.73458e-49
42%: mean: 1.96176e-13 ,    variance: 6.06974e-23
 var_var: -6.73516e-49
43%: mean: 1.96162e-13 ,    variance: 6.0696e-23
 var_var: -6.73574e-49
44%: mean: 1.96258e-13 ,    variance: 6.0715e-23
 var_var: -6.73633e-49
45%: mean: 1.96245e-13 ,    variance: 6.07143e-23
 var_var: -6.73696e-49
46%: mean: 1.96252e-13 ,    variance: 6.07234e-23
 var_var: -6.73755e-49
47%: mean: 1.96229e-13 ,    variance: 6.07245e-23
 var_var: -6.73815e-49
48%: mean: 1.96189e-13 ,    variance: 6.07139e-23
 var_var: -6.73873e-49
49%: mean: 1.96168e-13 ,    variance: 6.07206e-23
 var_var: -6.73927e-49
50%: mean: 1.96184e-13 ,    variance: 6.07165e-23
 var_var: -6.73979e-49
51%: mean: 1.96145e-13 ,    variance: 6.07228e-23
 var_var: -6.74027e-49
52%: mean: 1.96125e-13 ,    variance: 6.0723e-23
 var_var: -6.74075e-49
53%: mean: 1.96159e-13 ,    variance: 6.07238e-23
 var_var: -6.74124e-49
54%: mean: 1.96117e-13 ,    variance: 6.07084e-23
 var_var: -6.74172e-49
55%: mean: 1.96121e-13 ,    variance: 6.07122e-23
 var_var: -6.74213e-49
56%: mean: 1.96182e-13 ,    variance: 6.0704e-23
 var_var: -6.74253e-49
57%: mean: 1.96188e-13 ,    variance: 6.06902e-23
 var_var: -6.74289e-49
58%: mean: 1.96161e-13 ,    variance: 6.06871e-23
 var_var: -6.74319e-49
59%: mean: 1.96182e-13 ,    variance: 6.06909e-23
 var_var: -6.74349e-49
60%: mean: 1.9625e-13 ,    variance: 6.06944e-23
 var_var: -6.74378e-49
61%: mean: 1.96211e-13 ,    variance: 6.0693e-23
 var_var: -6.74409e-49
62%: mean: 1.9619e-13 ,    variance: 6.06993e-23
 var_var: -6.74437e-49
63%: mean: 1.96192e-13 ,    variance: 6.07063e-23
 var_var: -6.74467e-49
64%: mean: 1.96153e-13 ,    variance: 6.07086e-23
 var_var: -6.74498e-49
65%: mean: 1.96137e-13 ,    variance: 6.07186e-23
 var_var: -6.74528e-49
66%: mean: 1.96162e-13 ,    variance: 6.07154e-23
 var_var: -6.74559e-49
67%: mean: 1.96157e-13 ,    variance: 6.07096e-23
 var_var: -6.74589e-49
68%: mean: 1.96142e-13 ,    variance: 6.07081e-23
 var_var: -6.74616e-49
69%: mean: 1.96148e-13 ,    variance: 6.07062e-23
 var_var: -6.74643e-49
70%: mean: 1.96164e-13 ,    variance: 6.07038e-23
 var_var: -6.74669e-49
71%: mean: 1.96191e-13 ,    variance: 6.07018e-23
 var_var: -6.74692e-49
72%: mean: 1.96171e-13 ,    variance: 6.07044e-23
 var_var: -6.74715e-49
73%: mean: 1.96167e-13 ,    variance: 6.07075e-23
 var_var: -6.74737e-49
74%: mean: 1.96157e-13 ,    variance: 6.07136e-23
 var_var: -6.74759e-49
75%: mean: 1.96104e-13 ,    variance: 6.07102e-23
 var_var: -6.74782e-49
76%: mean: 1.96078e-13 ,    variance: 6.07108e-23
 var_var: -6.74805e-49
77%: mean: 1.961e-13 ,    variance: 6.07055e-23
 var_var: -6.74826e-49
78%: mean: 1.96132e-13 ,    variance: 6.07101e-23
 var_var: -6.74846e-49
79%: mean: 1.96147e-13 ,    variance: 6.07131e-23
 var_var: -6.74866e-49
80%: mean: 1.96181e-13 ,    variance: 6.07135e-23
 var_var: -6.74886e-49
81%: mean: 1.96209e-13 ,    variance: 6.07132e-23
 var_var: -6.74906e-49
82%: mean: 1.96198e-13 ,    variance: 6.07161e-23
 var_var: -6.74925e-49
83%: mean: 1.96198e-13 ,    variance: 6.0722e-23
 var_var: -6.74945e-49
84%: mean: 1.96169e-13 ,    variance: 6.07236e-23
 var_var: -6.74966e-49
85%: mean: 1.96167e-13 ,    variance: 6.07192e-23
 var_var: -6.74986e-49
86%: mean: 1.96156e-13 ,    variance: 6.0719e-23
 var_var: -6.75005e-49
87%: mean: 1.96151e-13 ,    variance: 6.07162e-23
 var_var: -6.75024e-49
88%: mean: 1.96149e-13 ,    variance: 6.07118e-23
 var_var: -6.75041e-49
89%: mean: 1.96152e-13 ,    variance: 6.07133e-23
 var_var: -6.75057e-49
90%: mean: 1.96161e-13 ,    variance: 6.07094e-23
 var_var: -6.75073e-49
91%: mean: 1.96152e-13 ,    variance: 6.0709e-23
 var_var: -6.75087e-49
92%: mean: 1.96142e-13 ,    variance: 6.0711e-23
 var_var: -6.751e-49
93%: mean: 1.96155e-13 ,    variance: 6.07118e-23
 var_var: -6.75115e-49
94%: mean: 1.96142e-13 ,    variance: 6.07196e-23
 var_var: -6.7513e-49
95%: mean: 1.96144e-13 ,    variance: 6.07211e-23
 var_var: -6.75145e-49
96%: mean: 1.96138e-13 ,    variance: 6.07204e-23
 var_var: -6.7516e-49
97%: mean: 1.96133e-13 ,    variance: 6.07249e-23
 var_var: -6.75175e-49
98%: mean: 1.96132e-13 ,    variance: 6.07189e-23
 var_var: -6.75191e-49
99%: mean: 1.96116e-13 ,    variance: 6.07166e-23
 var_var: -6.75205e-49
100%: mean: 1.96118e-13 ,    variance: 6.07153e-23
 var_var: -6.75219e-49
