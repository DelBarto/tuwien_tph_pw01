#include <iostream>
#include <fstream>
#include <string>
#include <cstdio>
#include <cmath>
using namespace std;



string filename = "../Tensor.tar/FTODDUMP_MC";

float* C;
int dim[3]= {500,32,32};

int posArr(int x, int y, int z, int c, int Tdim[]){
        // indices of Tensor start at 1
        x-=1;
        y-=1;
        z-=1;
        return x+Tdim[0]*y+Tdim[0]*Tdim[1]*z+Tdim[0]*Tdim[1]*Tdim[2]*c;
}

int Ten2Arr(int *idx, int comp, int *Tdim, size_t dimension){ // dimension has to be passed too because arrays decay to pointers when passed as function parameters
        for (size_t i = 0; i < dimension; i++) idx[i] -= 1;
        int pos = 0;
        int shift = 1;
        // 3D: x+Tdim[0]*y+Tdim[0]*Tdim[1]*z+Tdim[0]*Tdim[1]*Tdim[2]*c;
        for (size_t i = 0; i < dimension; i++) {
                pos += shift*idx[i];
                shift *= Tdim[i];
        }
        pos += shift*comp; //complex shift
        return pos;
}

void ReadFile(ifstream &in)
{
        float** inData;
        short int** index;
        if (!in) {
                cout << "Cannot open file.\n";
                return;
        }

        string line;
        getline(in, line);
        cout << line << "\n";

        in >> dim[0] >> dim[1] >> dim[2];
        cout << dim[0] << "*" << dim[1] << "*" << dim[2] << " = "  << dim[0]*dim[1]*dim[2] << "\n";

        getline(in, line);
        // cout << line << "\n";
        getline(in, line);
        cout << line << "\n";

        size_t size_a = dim[0]*dim[1]*dim[2];
        cout << size_a << "\n";

        inData = new float*[size_a];
        index = new short int*[size_a];
        for (size_t i = 0; i < size_a; i++) {
                inData[i] = new float[2];
                index[i] = new short int[3];
                in >> inData[i][0] >> inData[i][1] >> index[i][0] >> index[i][1] >> index[i][2];
                // cout << i << ": " << index[i][0] << " - " << index[i][1] << " - " << index[i][2] << "\n";
        }
        in.close();

        C = new float[size_a*2];
        int idx;
        for (size_t i = 0; i < size_a; i++) {
                for (size_t j = 0; j < 2; j++) {
                        // idx = (index[i][0]-1)+dim[0]*(index[i][1]-1)+dim[0]*dim[1]*(index[i][2]-1)+dim[0]*dim[1]*dim[2]*j;
                        idx = posArr(index[i][0],index[i][1],index[i][2],j, dim);
                        C[idx] = inData[i][j];
                }
        }

        //Free each sub-array
        for(size_t i = 0; i < size_a; i++) {
                delete[] inData[i];
                delete[] index[i];
        }
        delete[] index[0];

        //Free the array of pointers
        delete [] inData;
        delete [] index;
}



void writeMag(){
        FILE* fout = fopen("cTen.dat", "w");
        for(int x= 1; x < dim[0]; x++) {
                for(int y = 1; y < dim[1]; y++) {
                        for(int z = 1; z < dim[2]; z++) {
                                int idx[] = {x,y,z};
                                int dimension = sizeof(idx)/sizeof(*idx);
                                // fprintf(fout, "%i\t%i\t%i\t%f\n",x,y,z, 400*(pow(C[posArr(x,y,z,0,dim)],2.0)+pow(C[posArr(x,y,z,1,dim)],2.0)));
                                double CC = 400*(pow(C[Ten2Arr(idx,0,dim, dimension)],2.0)+pow(C[Ten2Arr(idx,1,dim, dimension)],2.0));
                                fprintf(fout, "%i\t%i\t%i\t%f\n",x,y,z,CC);
                        }
                }
        }
        fclose(fout);


}




int main() {
        ifstream myfile;
        myfile.open(filename);

        if(myfile.is_open())
        {
                ReadFile(myfile);
        }else{
                cout << "READ went wrong!!!";
        }
        // writeMag();
        float sum = 0;
        for(int x= 1; x < dim[0]; x++) {
                for(int y = 1; y < dim[1]; y++) {
                        for(int z = 1; z < dim[2]; z++) {
                                sum += pow(C[posArr(x,y,z,0,dim)],2.0)+pow(C[posArr(x,y,z,1,dim)],2.0);
                        }
                }
        }



        std::cout << "sum: " << sum << '\n';
        delete[] C;
        return 0;
}
