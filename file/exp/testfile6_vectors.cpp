
#include "../tensor.h"
#include <iostream>
#include <vector>
#include <string>
#include <sstream>


void vec(std::vector<Tensor> v){
        for (size_t i = 0; i < v.size(); i++) {
                std::cout << i << ": " << v.at(i).id << '\n';
        }
        std::cout << v.size() << '\n';
}

int main(int argc, char const *argv[]) {
        Tensor t1("C", false);
        std::vector<Tensor> v(3);
        for (size_t i = 0; i < 3; i++) {
                std::stringstream s;
                s << "C" << i;
                v.at(i) = Tensor(s.str(), false);
        }
        vec(v);

        std::cout << (int[2] {
                10,20
        })[0] << '\n';
}
