#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cstdio>
#include <cmath>
#include <vector>
#include <random>
#include <complex>
#include <bitset>

int main(){
	int a = 90;
	int b = 80;
	int c = 5;
	std::cout << a << '\n';
	std::cout << b << '\n' << '\n';
	
	int *x = &a;
	*x = c;
	x = &b;

	std::cout << a << '\n';
	std::cout << b << '\n';
	std::cout << *x << '\n';
	
}
