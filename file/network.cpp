#include "./network.h"

void printArray(int *x, int count) {
        std::cout << "{ ";
        for (int i = 0; i < count; i++) {
                std::cout << x[i] << " ";
        }
        std::cout << "}";
}

TensorNet::TensorNet() {
}

TensorNet::TensorNet(const TensorNet &other) :
        tensors(other.tensors),
        network(other.network),
        nbrTensors(other.nbrTensors),
        nbrIndices(other.nbrIndices),
        maxID(other.maxID) {
}

TensorNet::TensorNet(size_t nbrT, size_t nbrI, unsigned int **Tnet, std::vector<Tensor*> tens) {
        setTnet(nbrT, nbrI, Tnet);
        setTlist(tens);
        genMaxID();
}

TensorNet::~TensorNet() {
        if (network != NULL) {
                for (size_t i = 0; i < nbrTensors; i++) {
                        delete[] network[i];
                }
                delete[] network;
        }
        for (size_t i = 0; i < tensors.size(); i++) {
                delete tensors.at(i);
        }
        delete[] maxID;
}


void TensorNet::setTnet(size_t nbrT, size_t nbrI, unsigned int **Tnet) {
        nbrTensors = nbrT;
        tensors.resize(nbrTensors);
        nbrIndices = nbrI;
        network = Tnet;
}


void TensorNet::setTlist(std::vector<Tensor*> tens) {
        tensors = tens;
}


unsigned int TensorNet::getNetwork(size_t tens, size_t idx) {
        return network[tens][idx];
}


void TensorNet::genMaxID() {
        maxi = 1; // one could ask why not unsigned long int -> reason long stores two chunks of 16 bit where as size_t does not do that therefore it is more efficient to read in a size_t variable from the storage
        maxID = new std::size_t[nbrIndices];
        size_t indicesCounter = 0;
        for (int i = 0; i < static_cast<int>(nbrIndices); i++) {
                for (size_t k = 0; k < nbrTensors; k++) {
                        if (network[k][i] != 0) {
                                maxID[indicesCounter] = tensors.at(k)->DimSize[getNetwork(k, i)-1];
                                maxi  *= maxID[indicesCounter];
                                indicesCounter++;
                                break;
                        }
                }
        }
        std::cout << "maxi: " << maxi<< '\n';
        std::cout << "maxID: " << "[" << maxID[0] << "]" << "[" << maxID[1] << "]";
        std::cout << "[" << maxID[2] << "]" << "[" << maxID[3] << "]" << "[" << maxID[4] << "]" << "[" << maxID[5] << "] \n";
        return;
}


std::complex<double> TensorNet::calcVal() {
        std::size_t indices[nbrIndices];
        for (size_t i = 0; i < nbrIndices; i++) {
                indices[i] = 0;
        }

        std::complex<double> Tsum = 0;

        // Sum over all indices [i][j][a][b][g][f] like SUM_{i,j,a,b,g,f} C(g,a,i) * CONJG( C(g,b,j) ) * C(f,b,j) * CONJG( C(f,a,i) )
        for (size_t i = 0; i < maxi; i++) {
                // multiply Tensors for one indices combination [i][j][a][b][g][f] like C(g,a,i) * CONJG( C(g,b,j) ) * C(f,b,j) * CONJG( C(f,a,i) )
                std::complex<double> sumpart = 1;
                for (size_t j = 0; j < nbrTensors; j++) {
                        int idx[tensors.at(j)->dimension];
                        // generate indices to grab the Tensor entry
                        for (size_t k = 0; k < nbrIndices; k++) {
                                if (network[j][k] != 0) {
                                        idx[network[j][k]-1] = indices[k];
                                }
                        }
                        // multyply tensors for one part of the sum
                        sumpart *= tensors.at(j)->get(idx);
                }
                // add part of the sum to the sum
                Tsum += sumpart;

                // increase indices, to loop through them
                for (size_t j = 0; j < nbrIndices; j++) {
                        if (indices[j]+1 < maxID[j]) {
                                indices[j]++;
                                break;
                        } else {
                                indices[j] = 0;
                        }
                }
        }

        std::cout << "Calculated Tsum = "<< Tsum << "\n";
        Tsum /= maxi;
        std::cout << "Calculated Tsum / N= "<< Tsum << "\n";

        return 0.0;
}


std::complex<double> TensorNet::sampleTNet(int* tensor_order, int* isample, std::size_t size_isample) {
        int choosen_id[nbrIndices];
        double weight[nbrTensors];
        for (size_t i = 0; i < nbrIndices; i++) choosen_id[i] = -1;
        for (size_t i = 0; i < nbrTensors; i++) weight[i] = 1;

        // choose uniform distibuted indices
        for (int *t = tensor_order; t < tensor_order+nbrTensors; t++) {  // search tensor to sample inidces from
                for (int i = 0; i < static_cast<int>(nbrIndices); i++) {  // loop over all indices with are going to be uniform sampled
                        bool is_i_in_issample = false;
                        for (int *is = isample; is < isample+size_isample; is++) {
                                if (i == *is) {
                                        is_i_in_issample = true;
                                }
                        }
                        if (!is_i_in_issample && network[*t][i] != 0) {
                                choosen_id[i] = tensors.at(*t)->getUniDistIndex(network[*t][i]-1);
                        }
                }
        }
        // std::cout << "--> {" << choosen_id[0] << "," << choosen_id[1] << "," << choosen_id[2] << "," << choosen_id[3] << "," << choosen_id[4] << "," << choosen_id[5] << "}" << '\n';


        // call function for importance sampling
        for (int *i = isample; i < isample+size_isample; i++) {
                // std::cout << *i << "  --> {" << choosen_id[0] << "," << choosen_id[1] << "," << choosen_id[2] << "," << choosen_id[3] << "," << choosen_id[4] << "," << choosen_id[5] << "}" << '\n';
                for (int *t = tensor_order; t < tensor_order+nbrTensors; t++) {
                        if (choosen_id[*i] == -1 && network[*t][*i] != 0) {
                                int *sampledTensor_id = new int(tensors.at(*t)->dimension);  // the to sampled id is ignored
                                for (size_t id = 0; id < nbrIndices; id++) {  // get indices for importance sampling
                                        if (*i != static_cast<int>(id) && network[*t][id] != 0) {
                                                sampledTensor_id[network[*t][id]-1] = choosen_id[id];
                                        }
                                }

                                choosen_id[*i] = tensors.at(*t)->getISampIndex(network[*t][*i]-1, sampledTensor_id);

                                sampledTensor_id[network[*t][*i]-1] = choosen_id[*i];
                                // std::cout << tensors.at(*t)->identification << " - " << weight[*t] << '\n';

                                weight[*t] *= tensors.at(*t)->getISampProb(network[*t][*i]-1, sampledTensor_id);
                                weight[*t] *= tensors.at(*t)->DimSize[network[*t][*i]-1];

                                // std::cout << "{" << sampledTensor_id[0] << "," << sampledTensor_id[1] << "," << sampledTensor_id[2] << "}    <<<<<<<<<<>>>>>>>>>  " << weight[*t] << '\n';
                                // std::cout << tensors.at(*t)->getISampProb(network[*t][*i]-1, sampledTensor_id) << '\n';
                                // std::cout << weight[*t] << '\n';
                                delete[] sampledTensor_id;
                                break;
                        }
                }
        }

        // std::cout << "--> {" << choosen_id[0] << "," << choosen_id[1] << "," << choosen_id[2] << "," << choosen_id[3] << "," << choosen_id[4] << "," << choosen_id[5] << "}" << "\n";
        // std::cout << "###### {" << weight[0] << "," << weight[1] << "," << weight[2] << "," << weight[3] << "," << weight[4] << "," << weight[5] << "}" << "\n";
/*
        std::cout << "{ ";
        for (size_t i = 0; i < nbrIndices; i++) {
                std::cout << choosen_id[i] << " ";
        }
        std::cout << "}" << '\n';
        std::cout << "Weight: { ";
        for (size_t i = 0; i < nbrTensors; i++) {
                 std::cout << weight[i] << " ";
        }
        std::cout << "}" << '\n';
*/

        std::complex<double> T_part = 1;
        std::complex<double> tempEntry = 1;
        for (int t = 0; t < static_cast<int>(nbrTensors); t++) {
                // std::cout << tensors.at(t)->identification << ": " << '\n';
                int *sampledTensor_id = new int(tensors.at(t)->dimension);
                for (size_t i = 0; i < nbrIndices; i++) {
                        if (network[t][i] != 0) {
                                sampledTensor_id[network[t][i]-1] = choosen_id[i];
                        }
                }
                tempEntry = tensors.at(t)->get(sampledTensor_id);
                // printArray(sampledTensor_id, tensors.at(t)->dimension);
                // std::cout << "   -   " << tempEntry << '\n';
                T_part *= tempEntry;

                delete[] sampledTensor_id;
                // std::cout << "weight: " << weight[t] << '\n';
                // std::cout << "bevor: " << tempEntry << '\n';
                T_part /= weight[t];
                // std::cout << "after: " << tempEntry/weight[t] << '\n';
                // std::cout << '\n';
        }
        // std::cout << "--> {" << choosen_id[0] << "," << choosen_id[1] << "," << choosen_id[2] << "," << choosen_id[3] << "," << choosen_id[4] << "," << choosen_id[5] << "}" << '\n';
        // std::cout << "T_part: "<< T_part << "\n\n";

        return T_part;

}
