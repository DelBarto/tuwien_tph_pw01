//==================================
// include guard
#ifndef NETWORK_H_  // if x.h hasn't been included yet...
#define NETWORK_H_  // #define this so the compiler knows it has been included
//==================================

// includes
#include <vector>
#include <iostream>
#include <random>
#include <complex>
#include <stdio.h>
#include <mpi.h>
#include "./tensor.h"
#include "./randomGen.h"

// variables

// functions
struct TensorNet {
        std::vector<Tensor*> tensors;
        unsigned int **network = NULL;
        std::size_t nbrTensors;
        std::size_t nbrIndices;

        std::size_t maxi;
        std::size_t *maxID;

        TensorNet();
        TensorNet(const TensorNet &);
        TensorNet(size_t nbrT, size_t nbrI, unsigned int **Tnet, std::vector<Tensor*> tens);
        ~TensorNet();

        void setTnet(size_t nbrT, size_t nbrI, unsigned int **Tnet);
        void setTlist(std::vector<Tensor*> tens);
        unsigned int getNetwork(size_t tens, size_t ind);

        void genMaxID();
        std::complex<double> calcVal();
        std::complex<double> sampleTNet(int* tensor_order, int* choose_uni, std::size_t size_choose_uni);
};


//=================================
#endif  // FILE_NETWORK_H_
//=================================
