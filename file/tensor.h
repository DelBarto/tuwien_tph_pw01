//==================================
// include guard
#ifndef TENSOR_H_  // if x.h hasn't been included yet...
#define TENSOR_H_  // #define this so the compiler knows it has been included
//==================================

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cstdio>
#include <cmath>
#include <vector>
#include <random>
#include <complex>
// #include "./pcg-cpp-0.98/include/pcg_random.hpp"
#include "./randomGen.h"


struct Tensor {
        std::string filename = "";
        std::string identification = "";
        std::size_t dimension = 0;
        int *DimSize = NULL;
        bool isComplexConj = false;

        std::size_t nbrEntries;
        std::vector < std::complex<double> >  Tens;

        // size_t size_TMag;  // size of TMag
        // double* TMag = NULL;

        std::vector<int*> idx_order;
        std::vector<double*> spDist;
        std::vector<std::uniform_int_distribution<int>*> uniInt;

        Tensor();
        Tensor(std::string identifier, bool isCC);
        Tensor(std::string identifier, std::string fn, int dim, bool isCC);
        ~Tensor();


        void setDimProbaties(int dim, int *DSize);
        void setDimension(int dim);
        void setDimSize(int *DSize);

        int Ten2Arr(int *idx);
        int* Arr2Ten(int pos);
        void file2tensor(std::string fn, size_t dim);

        void set(int *idx, std::complex<double> val);
        void set(int pos, std::complex<double> val);
        std::complex<double> get(int *idx);
        std::complex<double> get(int pos);
        double mag(int *idx);
        double mag(int pos);
        void print(int *idx);

        int *Arr2Dist(int pos, int* DimensionSize);
        int Dist2Arr(int* idx, int* DimensionSize);
        void genSpliceDistribution();
        void saveSliceDistribution(std::size_t id);

        int getISampIndex(int rand_id, int* idx);
        double getISampProb(int rand_id, int* upper_idx);
        int getUniDistIndex(std::size_t index);
};


//=================================
#endif  // TENSOR_H_
//=================================
