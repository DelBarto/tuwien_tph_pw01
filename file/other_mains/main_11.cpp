#include <iostream>
#include <vector>
#include <complex>
#include <string>
#include <sstream>
#include <ctime>

#include "./tensor.h"
#include "./network.h"



int main() {
        size_t nTensors = 4;
        size_t nIndices = 6;

        // E= \sum_{i,j,a,b,g,f} C(g,a,i) * CONJG( C(g,b,j) ) * C(f,b,j) * CONJG( C(f,a,i) )
        //                                 g, a, i, f, b, j
        int netw[nTensors][nIndices] =  { {1, 2, 3, 0, 0, 0},    // C(g,a,i)
                                          {1, 0, 0, 0, 2, 3},    // CONJG( C(g,b,j) )
                                          {0, 0, 0, 1, 2, 3},    // C(f,b,j)
                                          {0, 2, 3, 1, 0, 0} };  // CONJG( C(f,a,i))

        unsigned int **net;
        net = new unsigned int*[nTensors];
        for (size_t i = 0; i < nTensors; i++) {
                net[i] = new unsigned int[nIndices];
                for (size_t j = 0; j < nIndices; j++) {
                        net[i][j] = netw[i][j];
                }
        }


        std::string filename = "./TenNet_1/FTODDUMP_MC";
        // filename = "./TenNet_1/test_is";
        // filename = "./TenNet_1/test-4x2x2";
        Tensor C1("C1", filename, 3, false);  // C(g,a,i)
        Tensor C2("C2", filename, 3, true);  // CONJG( C(g,b,j) )
        Tensor C3("C3", filename, 3, false);  // C(f,b,j)
        Tensor C4("C4", filename, 3, true);  // CONJG( C(f,a,i) )
        std::complex<double> E = 0;

        // int test_idx[] = {7, 3, 3};
        // std::cout << "C1: " << C1.get(test_idx) << '\n';
        // C1.print(test_idx);
        // std::cout << "C2: " << C2.get(test_idx) << '\n';
        // C2.print(test_idx);
        // std::cout << "C3: " << C3.get(test_idx) << '\n';
        // C3.print(test_idx);
        // std::cout << "C4: " << C4.get(test_idx) << '\n';
        // C4.print(test_idx);

        std::vector<Tensor*> Tlist(nTensors);
        Tlist.at(0) = new Tensor("C1", filename, 3, false);
        Tlist.at(1) = new Tensor("C2", filename, 3, true);
        Tlist.at(2) = new Tensor("C3", filename, 3, false);
        Tlist.at(3) = new Tensor("C4", filename, 3, true);

        clock_t begin = clock();
        double elapsed_secs;
        clock_t end;

        TensorNet Tnet =  TensorNet(nTensors, nIndices, net, Tlist);
        // Tnet.calcVal();

        end = clock();
        elapsed_secs = static_cast<double>(end - begin) / CLOCKS_PER_SEC;
        std::cout << "dt = " << elapsed_secs << '\n';
        begin = end;

        // ============= C -> V -> E =============
        Tensor V("V", false);
        Tensor VC("V", false);

        V.setDimension(C1.dimension+C2.dimension-2);
        VC.setDimension(C3.dimension+C4.dimension-2);
        int setDimSizeV[C1.dimension+C2.dimension-2] = {C1.DimSize[1], C1.DimSize[2], C2.DimSize[1], C2.DimSize[2]};
        int setDimSizeVC[C3.dimension+C4.dimension-2] = {C4.DimSize[1], C4.DimSize[2], C3.DimSize[1], C3.DimSize[2]};

        V.setDimProbaties(C1.dimension+C2.dimension-2, setDimSizeV);
        VC.setDimProbaties(C3.dimension+C4.dimension-2, setDimSizeVC);


        std::cout << "{" << V.DimSize[0] << "," << V.DimSize[1] << ",";
        std::cout << V.DimSize[2] << "," << V.DimSize[2] << "}" << '\n';


        for (int a = 0; a < V.DimSize[0]; a++) {
                for (int i = 0; i < V.DimSize[1]; i++) {
                        for (int b = 0; b < V.DimSize[2]; b++) {
                                for (int j = 0; j < V.DimSize[3]; j++) {
                                        int idxV[V.dimension] = {a, b, i, j};
                                        // int idxVC[VC.dimension] = {i, j, a, b};
                                        std::complex<double> V_val = 0;
                                        std::complex<double> VC_val = 0;
                                        for (int f = 0; f < C1.DimSize[0]; f++) {
                                                int idxC[C1.dimension] = {f, a, i};
                                                int idxCC[C2.dimension] = {f, b, j};

                                                VC_val += C3.get(idxC)*C4.get(idxCC);

                                                V_val += C1.get(idxCC)*C2.get(idxC);
                                        }
                                        VC.set(idxV, VC_val);
                                        V.set(idxV, V_val);
                                }
                        }
                }
        }
        std::cout << "1" << '\n';
        for (int a = 0; a < V.DimSize[0]; a++) {
                for (int b = 0; b < V.DimSize[2]; b++) {
                        for (int i = 0; i < V.DimSize[1]; i++) {
                                for (int j = 0; j < V.DimSize[3]; j++) {
                                        int idxV[V.dimension] = {a, b, i, j};
                                        E += V.get(idxV)*VC.get(idxV);
                                }
                        }
                }
        }
        std::cout << "V: E = " << E << '\n';
        // =======================================


        end = clock();
        elapsed_secs = static_cast<double>(end - begin) / CLOCKS_PER_SEC;
        std::cout << "dt = " << elapsed_secs << '\n';
        begin = end;

        // ============= C -> K -> E =============
        Tensor K("V", false);
        Tensor KC("V", false);

        K.setDimension(C1.dimension+C4.dimension-4);
        KC.setDimension(C3.dimension+C2.dimension-4);
        int setDimSizeK[K.dimension] = {C1.DimSize[0], C4.DimSize[0]};
        int setDimSizeKC[KC.dimension] = {C3.DimSize[0], C2.DimSize[0]};

        K.setDimProbaties(K.dimension, setDimSizeK);
        KC.setDimProbaties(KC.dimension, setDimSizeKC);


        std::cout << "{" << K.DimSize[0] << "," << K.DimSize[1] << "}" << '\n';

        for (int f = 0; f < K.DimSize[0]; f++) {
                for (int g = 0; g < K.DimSize[1]; g++) {
                        std::complex<double> K_val = 0;
                        std::complex<double> KC_val = 0;
                        int idxK[K.dimension] = {f, g};
                        for (int a = 0; a < C1.DimSize[1]; a++) {
                                for (int i = 0; i < C1.DimSize[2]; i++) {
                                        int idxC[C1.dimension] = {g, a, i};
                                        int idxCC[C4.dimension] = {f, a, i};
                                        K_val += C1.get(idxC)*C4.get(idxCC);
                                        KC_val += C3.get(idxCC)*C2.get(idxC);
                                }
                        }
                        K.set(idxK, K_val);
                        KC.set(idxK, KC_val);
                }
        }
        E = 0;
        for (int f = 0; f < K.DimSize[0]; f++) {
                for (int g = 0; g < K.DimSize[1]; g++) {
                        int idxK[K.dimension] =  {f, g};
                        // int idxKC[K.dimension] =  {g, f};
                        E += K.get(idxK)*KC.get(idxK);
                }
        }
        std::cout << "K: E = " << E << '\n';
        // =======================================


        end = clock();
        elapsed_secs = static_cast<double>(end - begin) / CLOCKS_PER_SEC;
        std::cout << "dt = " << elapsed_secs << '\n';
        begin = end;


        E = 0;

        for (int j = 0; j < C2.DimSize[2]; j++) {
                for (int b = 0; b < C2.DimSize[1]; b++) {
                        for (int f = 0; f < C1.DimSize[0]; f++) {
                                for (int i = 0; i < C1.DimSize[2]; i++) {
                                        for (int a = 0; a < C1.DimSize[1]; a++) {
                                                for (int g = 0; g < C2.DimSize[0]; g++) {
                                                        int idxC_1[C1.dimension] = {g, a, i};
                                                        int idxC_2[C2.dimension] = {g, b, j};
                                                        int idxC_3[C3.dimension] = {f, b, j};
                                                        int idxC_4[C4.dimension] = {f, a, i};

                                                        E += C1.get(idxC_1)*C2.get(idxC_2)*C3.get(idxC_3)*C4.get(idxC_4);
                                                }
                                        }
                                }
                        }
                }
        }
        std::cout << '\n';
        std::cout << "C: E = " << E << '\n';

        end = clock();
        elapsed_secs = static_cast<double>(end - begin) / CLOCKS_PER_SEC;
        std::cout << "dt = " << elapsed_secs << '\n';
        return 0;
}
