#include <iostream>
#include <vector>
#include <complex>
#include <string>
#include <sstream>
#include <ctime>

#include "./tensor.h"
#include "./network.h"



int main() {
        std::complex<double> x = 1.0 + 1.0i;
        std::complex<double> y = 2.0 + 0.5i;

        std::cout << std::abs(x) << " = " << sqrt(2) << '\n';
        std::cout << x.real() << '\n';
        std::cout << x*y << '\n';
        std::cout << x*y+x*y << '\n';
        return 0;
}
