#include <iostream>
#include <vector>
#include <random>
#include "./randomGen.cpp"


void printOut(const int a) {
        std::cout << a << '\n';
}

int main() {
        // Gen g();
        std::uniform_int_distribution<int> uni_int(0, 3);
        std::cout << Gen::uni_real_Dist(Gen::generator) << '\n';

        std::cout << '\n';
        for (size_t i = 0; i < 20; i++) {
                std::cout << uni_int(Gen::generator) << '\n';
        }


        return 0;
}
