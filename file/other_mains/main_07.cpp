#include <iostream>
#include <iomanip>
#include <vector>
#include <complex>
#include <cmath>

int main() {
        int size = 10;
        std::vector < std::complex<double> >  tensor;
        tensor.resize(size);
        std::fill(tensor.begin(), tensor.end(), 0);

        for (int j = 0; j < size; j++) {
                tensor.at(j) += j*1.0;
                tensor.at(j) += (std::complex<double>)(1.0i*j); //sqrt(2));
                std::cout << tensor.at(j) << "  " << std::abs(tensor.at(j)) << '\n';
        }
}
