
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cstdio>
#include <iomanip>
#include <vector>
#include <complex>
#include <cmath>

// #include "./tensor.cpp"

int main() {
        int dimension = 3;
        std::string filename = "./test-read.txt";
        std::ifstream in;
        in.open(filename.c_str());

        if (!in.is_open()) {
                std::cout << "READ went wrong!!!" << "\n";
                return 0;
        }
        if (!in) {
                std::cout << "Cannot open file.\n";
                return 0;
        }

        int *DimensionSize = new int[dimension];
        for (size_t i = 0; i < dimension; i++) {
                in >> DimensionSize[i];
                std::cout << DimensionSize[i] << " - ";
        }
        std::cout << '\n';




        int dim = 3;
        int DS[] = {8, 4, 4};
        int idx[] = {7, 3, 3};


        Tensor t("C", false);
        t.setDimProbaties(dim, DS);

        int i = t.Ten2Arr(idx);
        int *id = t.Arr2Ten(i);
        std::cout << i << ":  [" << id[0] << "," << id[1] << "," << id[2] << "]" << '\n';

}
