#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <ctime>

#include "./tensor.h"
#include "./network.h"



int main() {
        clock_t begin = clock();
        double elapsed_secs;
        clock_t end;

        std::string readinFile = "./TenNet_1/FTODDUMP_MC";
        readinFile = "./TenNet_1/test1";
        Tensor C("C", readinFile, 3, false);
        Tensor CC("CC", readinFile, 3, true);
        double E_real = 0;
        double E_comp = 0;

        // ============= C -> V -> E =============
        Tensor V("V", false);
        Tensor VC("V", false);

        V.setDimension(C.dimension+CC.dimension-2);
        VC.setDimension(C.dimension+CC.dimension-2);
        int setDimSizeV[C.dimension+CC.dimension-2] = {C.DimSize[1], C.DimSize[2], CC.DimSize[1], CC.DimSize[2]};
        int setDimSizeVC[C.dimension+CC.dimension-2] = {CC.DimSize[1], CC.DimSize[2], C.DimSize[1], C.DimSize[2]};

        V.setDimProbaties(setDimSizeV, C.dimension+CC.dimension-2);
        VC.setDimProbaties(setDimSizeVC, C.dimension+CC.dimension-2);


        std::cout << "{" << V.DimSize[0] << "," << V.DimSize[1] << ",";
        std::cout << V.DimSize[2] << "," << V.DimSize[2] << "}" << '\n';


        for (int a = 1; a <= V.DimSize[0]; a++) {
                for (int i = 1; i <= V.DimSize[1]; i++) {
                        for (int b = 1; b <= V.DimSize[2]; b++) {
                                for (int j = 1; j <= V.DimSize[3]; j++) {
                                        int idxV[V.dimension] = {a, b, i, j};
                                        // int idxVC[VC.dimension] = {i, j, a, b};
                                        double V_r = 0;
                                        double V_c = 0;
                                        double VC_r = 0;
                                        double VC_c = 0;
                                        for (int f = 1; f <= C.DimSize[0]; f++) {
                                                int idxC2[CC.dimension] = {f, a, i};
                                                int idxCC2[C.dimension] = {f, b, j};
                                                VC_r += C.get(idxC2, 0)*CC.get(idxCC2, 0) + C.get(idxC2, 1)*CC.get(idxCC2, 1);
                                                VC_c += C.get(idxC2, 0)*CC.get(idxCC2, 1) + C.get(idxC2, 1)*CC.get(idxCC2, 0);

                                                int idxC1[C.dimension] = {f, b, j};
                                                int idxCC1[CC.dimension] = {f, a, i};
                                                V_r += C.get(idxC1, 0)*CC.get(idxCC1, 0) + C.get(idxC1, 1)*CC.get(idxCC1, 1);
                                                V_c += C.get(idxC1, 0)*CC.get(idxCC1, 1) + C.get(idxC1, 1)*CC.get(idxCC1, 0);
                                        }
                                        VC.setEntry(idxV, 0, VC_r);
                                        VC.setEntry(idxV, 1, VC_c);
                                        V.setEntry(idxV, 0, V_r);
                                        V.setEntry(idxV, 1, V_c);
                                }
                        }
                }
        }
        std::cout << "1" << '\n';
        for (int a = 1; a <= V.DimSize[0]; a++) {
                for (int b = 1; b <= V.DimSize[2]; b++) {
                        for (int i = 1; i <= V.DimSize[1]; i++) {
                                for (int j = 1; j <= V.DimSize[3]; j++) {
                                        int idxV[V.dimension] = {a, b, i, j};
                                        // int idxVC[VC.dimension] = {i, j, a, b};

                                        // std::cout << "{" << a << "," << b << "," << i << "," << j << "}  -->  ";
                                        // std::cout << V.get(idxV, 0) << "  <-->  " << VC.get(idxVC, 0);
                                        // if (V.get(idxV, 1) == VC.get(idxVC, 1)) std::cout << "!!!!!!!!!!!!";
                                        // std::cout << "\n";

                                        // std::cout << V.get(idxV, 0)*VC.get(idxV, 1) << " + " << V.get(idxV, 1)*VC.get(idxV, 0) << '\n';
                                        E_real += V.get(idxV, 0)*VC.get(idxV, 0) + V.get(idxV, 1)*VC.get(idxV, 1);
                                        E_comp += V.get(idxV, 0)*VC.get(idxV, 1) + V.get(idxV, 1)*VC.get(idxV, 0);
                                        // E_real += V.get(idxV, 0)*VC.get(idxVC, 0) + V.get(idxV, 1)*VC.get(idxVC, 1);
                                        // E_comp += V.get(idxV, 0)*VC.get(idxVC, 1) + V.get(idxV, 1)*VC.get(idxVC, 0);
                                }
                        }
                }
        }
        std::cout << "V: E_real = " << E_real << '\n';
        std::cout << "V: E_comp = " << E_comp << '\n';
        // =======================================


        end = clock();
        elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
        std::cout << "dt = " << elapsed_secs << '\n';
        begin = end;

        // ============= C -> K -> E =============
        Tensor K("V", false);
        Tensor KC("V", false);

        K.setDimension(C.dimension+CC.dimension-4);
        KC.setDimension(C.dimension+CC.dimension-4);
        int setDimSizeK[K.dimension] = {C.DimSize[0], CC.DimSize[0]};
        int setDimSizeKC[KC.dimension] = {CC.DimSize[0], C.DimSize[0]};

        K.setDimProbaties(setDimSizeK, K.dimension);
        KC.setDimProbaties(setDimSizeKC, KC.dimension);


        std::cout << "{" << K.DimSize[0] << "," << K.DimSize[1] << "}" << '\n';

        for (int f = 1; f <= K.DimSize[0]; f++) {
                for (int g = 1; g <= K.DimSize[1]; g++) {
                        double K_r = 0;
                        double K_c = 0;
                        double KC_r = 0;
                        double KC_c = 0;
                        int idxK[K.dimension] = {f, g};
                        for (int a = 1; a <= C.DimSize[1]; a++) {
                                for (int i = 1; i <= C.DimSize[2]; i++) {
                                        int idxC1[C.dimension] = {g, a, i};
                                        int idxCC1[CC.dimension] = {f, a, i};
                                        K_r += C.get(idxC1, 0)*CC.get(idxCC1, 0) + C.get(idxC1, 1)*CC.get(idxCC1, 1);
                                        K_c += C.get(idxC1, 0)*CC.get(idxCC1, 1) + C.get(idxC1, 1)*CC.get(idxCC1, 0);

                                        int idxC2[CC.dimension] = {f, a, i};
                                        int idxCC2[C.dimension] = {g, a, i};
                                        KC_r += C.get(idxC2, 0)*CC.get(idxCC2, 0) + C.get(idxC2, 1)*CC.get(idxCC2, 1);
                                        KC_c += C.get(idxC2, 0)*CC.get(idxCC2, 1) + C.get(idxC2, 1)*CC.get(idxCC2, 0);
                                }
                        }
                        K.setEntry(idxK, 0, K_r);
                        K.setEntry(idxK, 1, K_c);

                        KC.setEntry(idxK, 0, KC_r);
                        KC.setEntry(idxK, 1, KC_c);
                }
        }
        std::cout << "2" << '\n';
        E_real = 0;
        E_comp = 0;
        for (int f = 1; f <= K.DimSize[0]; f++) {
                for (int g = 1; g <= K.DimSize[1]; g++) {
                        int idxK[K.dimension] =  {f, g};
                        // int idxKC[K.dimension] =  {g, f};
                        E_real += K.get(idxK, 0)*KC.get(idxK, 0) + K.get(idxK, 1)*KC.get(idxK, 1);
                        E_comp += K.get(idxK, 0)*KC.get(idxK, 1) + K.get(idxK, 1)*KC.get(idxK, 0);
                }
        }
        std::cout << "K: E_real = " << E_real << '\n';
        std::cout << "K: E_comp = " << E_comp << '\n';
        // =======================================


        end = clock();
        elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
        std::cout << "dt = " << elapsed_secs << '\n';
        begin = end;


        E_real = 0;
        E_comp = 0;

        for (int f = 1; f <= C.DimSize[0]; f++) {
                for (int g = 1; g <= CC.DimSize[0]; g++) {
                        for (int a = 1; a <= C.DimSize[1]; a++) {
                                for (int b = 1; b <= CC.DimSize[1]; b++) {
                                        for (int i = 1; i <= C.DimSize[2]; i++) {
                                                for (int j = 1; j <= C.DimSize[2]; j++) {
                                                        int idxC_1[C.dimension] = {g, a, i};
                                                        int idxC_2[C.dimension] = {g, b, j};
                                                        int idxC_3[C.dimension] = {f, b, j};
                                                        int idxC_4[C.dimension] = {f, a, i};

                                                        E_real += C.get(idxC_1, 0)*CC.get(idxC_2, 0)*C.get(idxC_3, 0)*CC.get(idxC_4, 0);
                                                        E_real += C.get(idxC_1, 0)*CC.get(idxC_2, 1)*C.get(idxC_3, 0)*CC.get(idxC_4, 1);  // ?
                                                        E_real += C.get(idxC_1, 0)*CC.get(idxC_2, 0)*C.get(idxC_3, 1)*CC.get(idxC_4, 1);  // ?
                                                        E_real += C.get(idxC_1, 0)*CC.get(idxC_2, 1)*C.get(idxC_3, 1)*CC.get(idxC_4, 0);  // ?
                                                        E_real += C.get(idxC_1, 1)*CC.get(idxC_2, 0)*C.get(idxC_3, 0)*CC.get(idxC_4, 1);  // ?
                                                        E_real += C.get(idxC_1, 1)*CC.get(idxC_2, 0)*C.get(idxC_3, 1)*CC.get(idxC_4, 0);  // ?
                                                        E_real += C.get(idxC_1, 1)*CC.get(idxC_2, 1)*C.get(idxC_3, 0)*CC.get(idxC_4, 0);  // ?
                                                        E_real += C.get(idxC_1, 1)*CC.get(idxC_2, 1)*C.get(idxC_3, 1)*CC.get(idxC_4, 1);

                                                        E_comp += C.get(idxC_1, 1)*CC.get(idxC_2, 0)*C.get(idxC_3, 0)*CC.get(idxC_4, 0);
                                                        E_comp += C.get(idxC_1, 0)*CC.get(idxC_2, 1)*C.get(idxC_3, 0)*CC.get(idxC_4, 0);
                                                        E_comp += C.get(idxC_1, 0)*CC.get(idxC_2, 0)*C.get(idxC_3, 1)*CC.get(idxC_4, 0);
                                                        E_comp += C.get(idxC_1, 0)*CC.get(idxC_2, 0)*C.get(idxC_3, 0)*CC.get(idxC_4, 1);
                                                        E_comp += C.get(idxC_1, 0)*CC.get(idxC_2, 1)*C.get(idxC_3, 1)*CC.get(idxC_4, 1);  // ?
                                                        E_comp += C.get(idxC_1, 1)*CC.get(idxC_2, 0)*C.get(idxC_3, 1)*CC.get(idxC_4, 1);  // ?
                                                        E_comp += C.get(idxC_1, 1)*CC.get(idxC_2, 1)*C.get(idxC_3, 0)*CC.get(idxC_4, 1);  // ?
                                                        E_comp += C.get(idxC_1, 1)*CC.get(idxC_2, 1)*C.get(idxC_3, 1)*CC.get(idxC_4, 0);  // ?
                                                }
                                        }
                                }
                        }
                }
        }
        std::cout << '\n';
        std::cout << "C: E_real = " << E_real << '\n';
        std::cout << "C: E_comp = " << E_comp << '\n';

        end = clock();
        elapsed_secs = static_cast<double>(end - begin) / CLOCKS_PER_SEC;
        std::cout << "dt = " << elapsed_secs << '\n';
        return 0;
}
