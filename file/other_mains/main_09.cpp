
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cstdio>
#include <iomanip>
#include <vector>
#include <complex>
#include <cmath>

#include "./tensor.h"

int main() {

        std::string filename = "./TenNet_1/test-4x2x2";
        filename = "./TenNet_1/test1";
        // filename = "./TenNet_1/FTODDUMP_MC";
        Tensor C1("C1", filename, 3, false);


        std::size_t nbr = 10000000;
        std::size_t is_idx = 0;
        C1.saveSliceDistribution(is_idx);
        int idx[] = {0, 1, 1};
        int counter[C1.DimSize[is_idx]];
        for (size_t i = 0; i < C1.DimSize[is_idx]; i++) {
                counter[i] = 0;
        }
        for (size_t i = 0; i < nbr; i++) {
                idx[is_idx] = C1.getISampIndex(0, idx);
                // std::cout << idx[0] << " " << idx[1] << " " << idx[2] << "  ";
                // std::cout << C1.getISampProb(0, idx) << '\n';

                counter[idx[is_idx]]++;
                // if (idx[is_idx] == 0) break;
                // std::cout << '\n';
        }
        std::cout << '\n';
        for (size_t i = 0; i < C1.DimSize[is_idx]; i++) {
                std::cout << i << ": "  << counter[i] << "   " << 1.0*counter[i]/nbr << '\n';
                /* code */
        }
}
