#include <iostream>
#include <vector>
#include <string>
#include <sstream>




int main() {
        int tensor_ord[3] = {1, 2, 3};

        for(int i : tensor_ord) {
                std::cout << i << '\n';
        }

        int *test = new int(3);
        test[0] = 100;
        test[1] = 200;
        test[2] = 300;

        // for (int n=*test; n!=0; n=*(test+1)) {
        //         std::cout << n << '\n';
        // }
        std::cout << *test << '\n';
        std::cout << *(test+1) << '\n';
        int *k = test;
        for(int *i = test; i < test+3; i++) {
                std::cout << *i << '\n';
        }


        return 0;
}
