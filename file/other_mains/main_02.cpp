#include <iostream>
#include <vector>
#include <string>
#include <sstream>

#include "./tensor.h"
#include "./network.h"



int main() {
        size_t nTensors = 4;
        size_t nIndices = 6;

        // E= \sum_{i,j,a,b,g,f} C(g,a,i) * CONJG( C(g,b,j) ) * C(f,b,j) * CONJG( C(f,a,i) )
        //                                 g, a, i, f, b, j
        int netw[nTensors][nIndices] =  { {1, 2, 3, 0, 0, 0},    // C(g,a,i)
                                          {1, 0, 0, 0, 2, 3},    // CONJG( C(g,b,j) )
                                          {0, 0, 0, 1, 2, 3},    // C(f,b,j)
                                          {0, 2, 3, 1, 0, 0} };  // CONJG( C(f,a,i))

        unsigned int **net;
        net = new unsigned int*[nTensors];
        for (size_t i = 0; i < nTensors; i++) {
                net[i] = new unsigned int[nIndices];
                for (size_t j = 0; j < nIndices; j++) {
                        net[i][j] = netw[i][j];
                }
        }

        std::vector<Tensor*> Tlist(nTensors);
        std::string filename = "./TenNet_1/test4";
        Tlist.at(0) = new Tensor("C1", filename, 3, false);
        Tlist.at(1) = new Tensor("C2", filename, 3, true);
        Tlist.at(2) = new Tensor("C3", filename, 3, false);
        Tlist.at(3) = new Tensor("C4", filename, 3, true);

        TensorNet Tnet = TensorNet(nTensors, nIndices, net, Tlist);
        Tnet.calcVal();
        //
        // int idx1[] = {4, 1, 2};
        // int idx2[] = {4, 2, 1};
        // Tlist.at(0)->printTenIdx(idx1, 0);
        // Tlist.at(0)->printTenIdx(idx2, 0);
        // Tlist.at(0)->printTenIdx(idx1, 1);
        // Tlist.at(0)->printTenIdx(idx2, 1);
        // Tlist.at(1)->printTenIdx(idx1, 0);
        // Tlist.at(1)->printTenIdx(idx2, 0);
        // Tlist.at(1)->printTenIdx(idx1, 1);
        // Tlist.at(1)->printTenIdx(idx2, 1);



        return 0;
}
