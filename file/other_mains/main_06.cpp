#include <iostream>
#include <vector>


int main() {
        size_t count = 10;
        size_t dim = 3;
        std::vector<double*> v;
        v.resize(dim);
        for (size_t i = 0; i < dim; i++) {
                v.at(i) = new double(count);
        }
        for (size_t i = 0; i < count; i++) {
                v.at(1)[i] = i;
        }

        std::cout << v.at(1)[5] << '\n';

        return 0;
}
