#include <iostream>
#include <complex>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>

// #include "./tensor.h"
// #include "./network.h"
// #include "./statMod.h"



int main() {
        std::vector< std::vector<int> > v = { {1, 0, 2},
                                              {0, 1, 0},
                                              {0, 0, 1} };
        
        for (size_t k = 0; k < v.size(); k++) {
            for (size_t i = 0; i < v.at(k).size(); i++) {
                std::cout << v.at(k).at(i) << " ";
            }
            std::cout << "\n";  
        }
                    
        std::cout << "\n";
        std::cout << "\n";  
        
        std::vector< int > vec = { 1,2,3,4,5,2,3,1 };
         
        int max = *std::max_element(vec.begin(), vec.end());
        std::cout << "max element of vec: " << max << "    " << vec.size() << "\n";
        
        vec.clear();
        
        vec.push_back(2);
        vec.push_back(1);
        vec.push_back(3);
        
        max = *std::max_element(vec.begin(), vec.end());
        std::cout << "max element of vec: " << max << "    " << vec.size() << "\n";
        
        return 0;
}
