#include <iostream>
#include <complex>
#include <vector>
#include <string>
#include <sstream>

// #include "./tensor.h"
#include "./network.h"



int main() {
        size_t nTensors = 4;
        size_t nIndices = 6;

        // E= \sum_{i,j,a,b,g,f} C(g,a,i) * CONJG( C(g,b,j) ) * C(f,b,j) * CONJG( C(f,a,i) )
        //                                 g, a, i, f, b, j
        int netw[nTensors][nIndices] =  { {1, 2, 3, 0, 0, 0},    // C(g,a,i)
                                          {1, 0, 0, 0, 2, 3},    // CONJG( C(g,b,j) )
                                          {0, 0, 0, 1, 2, 3},    // C(f,b,j)
                                          {0, 2, 3, 1, 0, 0} };  // CONJG( C(f,a,i))

        unsigned int **net;
        net = new unsigned int*[nTensors];
        for (size_t i = 0; i < nTensors; i++) {
                net[i] = new unsigned int[nIndices];
                for (size_t j = 0; j < nIndices; j++) {
                        net[i][j] = netw[i][j];
                }
        }

        std::vector<Tensor*> Tlist(nTensors);
        std::string filename = "./TenNet_1/test1";
        filename = "./TenNet_1/test_is";
        // filename = "./TenNet_1/FTODDUMP_MC";
        Tlist.at(0) = new Tensor("C1", filename, 3, false);
        Tlist.at(1) = new Tensor("C2", filename, 3, true);
        Tlist.at(2) = new Tensor("C3", filename, 3, false);
        Tlist.at(3) = new Tensor("C4", filename, 3, true);


        // TensorNet *Tnet =  new TensorNet(nTensors, nIndices, net, Tlist);
        TensorNet Tnet =  TensorNet(nTensors, nIndices, net, Tlist);

        int tensor_ord[nTensors] = {0, 1, 2, 3};
        int nbr_is_idx = 1;
        int is_idx[nbr_is_idx] = {0, 3};

        int nbrOfSamples = 8*4*4*8*4*4;
        nbrOfSamples = 1;
        std::complex<double> sampledValues[nbrOfSamples];

        // double *val;
        for (int i = 0; i < nbrOfSamples; i++) {
                sampledValues[i] = Tnet.sampleTNet(tensor_ord, is_idx, nbr_is_idx);
        }


        // std::cout << "finisched sampling" << '\n';
/**/


        // for (int i = 0; i < nbrOfSamples; i++) {
        //         std::cout << i << " " << sampledValues[i] << '\n';
        // }

        std::complex<double> val = 0;
        double variance;
        double tempVar;
        for (int i = 0; i < nbrOfSamples; i++) {
                variance = 0;
                val += sampledValues[i];

                // std::cout << '\n';
                for (int j = 0; j < i; j++) {
                        tempVar = sampledValues[j].real() - val.real()/(i+1);
                        // std::cout << sampledValues[j][0] << " - " << val[0]/(i+1) << '\n';
                        variance += tempVar*tempVar;
                }

                if ( i % (nbrOfSamples/10) == 0 ) {
                        std::cout << (static_cast<int> (i / (nbrOfSamples/10)) *10) << "%: " << val.real()/(i+1) << " " <<  sqrt(variance)/i<< '\n';
                }
                // std::cout << i << " " << val.real() << '\n';
        }
        std::cout << val.real()/(nbrOfSamples) << " " <<  sqrt(variance)/(nbrOfSamples-1)<< '\n';
/**/

        Tnet.calcVal();
        return 0;
}
