//==================================
// include guard
#ifndef FILE_TENSOR_H_  // if x.h hasn't been included yet...
#define FILE_TENSOR_H_  // #define this so the compiler knows it has been included
//==================================

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cstdio>
#include <cmath>
#include <vector>
#include <random>
#include <complex>
// #include "./pcg-cpp-0.98/include/pcg_random.hpp"
#include "./randomGen.h"


struct Tensor {
        std::string filename = "";
        std::string identification = "";
        std::vector < std::complex<double> >  Tens;
        double* TMag = NULL;
        size_t size_TMag;  // size of TMag
        std::size_t dimension = 0;
        int *DimSize = NULL;
        // cache last Tens access
        int *cacheIndices;
        double cacheTens[2];  // real and complex Value
        bool isComplexConj = false;

        std::vector<double*> spDist;
        std::vector<std::uniform_int_distribution<int>*> uniInt;



        Tensor();
        Tensor(std::string identifier, bool isCC);
        Tensor(std::string identifier, std::string fn, int dim, bool isCC);
        ~Tensor();

        void setDimProbaties(int *DSize, int dim);
        void setDimension(int dim);
        void setDimSize(int *DSize);
        int Ten2Arr(int *idx, bool comp);
        int* Arr2Ten(int pos);
        void setEntry(int *idx, bool complex, double val);
        void file2tensor(std::string fn);
        double get(int *idx, bool complex);
        double mag(int *idx);
        void printTenIdx(int *idx, bool comp);
        void Arr2Dist(int pos, int *idx, int* DimensionSize);
        int Dist2Arr(int* idx, int* DimensionSize);
        void genSpliceDistribution();
        void saveSliceDistribution(int index);
        int getISampIndex(int rand_id, int* idx);
        double getISampProb(int rand_id, int* upper_idx);
        int getUniDistIndex(int index);
};


//=================================
#endif  // FILE_TENSOR_H_
//=================================
