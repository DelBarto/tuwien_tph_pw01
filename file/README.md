# Project
This is a project within my master's degree in technical physics at the Vienna University of Technology. Its main task is to estimates a function (energy of a ground state), which consists of several tensors that are linked over a tensor network, with importance sampling (IS). The currently used methode for IS is the inversion methode.
...

## future plans
- [x] reducing numerical noise by recentering with `statMod.*`
- [ ] statistical tests
- [ ] include other networks
- [ ] parallelize (maybe over MPI)

# how to set up a tensor network for this programm
--> in making

## how to choose
--> in making
