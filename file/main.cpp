#include <iostream>
#include <complex>
#include <vector>
#include <string>
#include <sstream>
#include <stdio.h>
#include <mpi.h>

// #include "./tensor.h"
#include "./network.h"
#include "./statMod.h"

#define smallSAMPLE_DEBUG  1

int main() {
        StatMod stat;

        size_t nTensors = 4;
        size_t nIndices = 6;

        // E= \sum_{i,j,a,b,g,f} C(g,a,i) * CONJG( C(g,b,j) ) * C(f,b,j) * CONJG( C(f,a,i) )
        //                                 g, a, i, f, b, j
        int netw[nTensors][nIndices] =  { {1, 2, 3, 0, 0, 0},    // C(g,a,i)
                                          {1, 0, 0, 0, 2, 3},    // CONJG( C(g,b,j) )
                                          {0, 0, 0, 1, 2, 3},    // C(f,b,j)
                                          {0, 2, 3, 1, 0, 0} };  // CONJG( C(f,a,i))

        unsigned int **net;
        net = new unsigned int*[nTensors];
        for (size_t i = 0; i < nTensors; i++) {
                net[i] = new unsigned int[nIndices];
                for (size_t j = 0; j < nIndices; j++) {
                        net[i][j] = netw[i][j];
                }
        }

        std::vector<Tensor*> Tlist(nTensors);
        std::string filename = "./TenNet/test1";
        // filename = "./TenNet/test_is2";
        // filename = "./TenNet/test-4x2x2";
        // filename = "./TenNet/test-16x6x8";
        // filename = "./TenNet/FTODDUMP_MC";
        Tlist.at(0) = new Tensor("C1", filename, 3, false);
        Tlist.at(1) = new Tensor("C2", filename, 3, true);
        Tlist.at(2) = new Tensor("C3", filename, 3, false);
        Tlist.at(3) = new Tensor("C4", filename, 3, true);

        int ikl[] = {7, 3, 2};
        std::cout << Tlist.at(0)->Tens[Tlist.at(0)->Ten2Arr(ikl)] << '\n';
        std::cout << Tlist.at(1)->Tens[Tlist.at(1)->Ten2Arr(ikl)] << '\n';


        std::cout << Tlist.at(0)->Dist2Arr(ikl, Tlist.at(0)->DimSize) << '\n';
        std::cout << Tlist.at(0)->getISampProb(1, ikl) << '\n';
        ikl[1] = 2;
        std::cout << Tlist.at(0)->Dist2Arr(ikl, Tlist.at(0)->DimSize) << '\n';
        std::cout << Tlist.at(0)->getISampProb(1, ikl) << '\n';


        TensorNet Tnet =  TensorNet(nTensors, nIndices, net, Tlist);

        int tensor_ord[nTensors] = {0, 2, 1, 3};
        int nbr_is_idx = 2;
        int is_idx[nbr_is_idx] = {0, 3};

        std::size_t nbrOfSamples = 0;
        // nbrOfSamples = 534l*534l*32l;
        // nbrOfSamples *= 32l*32l;
        nbrOfSamples = 1;
        nbrOfSamples = 10000000;

        std::cout << "nbrOfSamples = " << nbrOfSamples << '\n';
        int howManyOut = 50;
        int divider = nbrOfSamples/howManyOut;
        for (size_t i = 0; i < nbrOfSamples; i++) {
                std::complex<double> temp = Tnet.sampleTNet(tensor_ord, is_idx, nbr_is_idx);
                stat.addOpt(temp);

                if ( ((i+1) % (divider) == 0 && i != 0) || i == 1 ) {
                        std::cout << (static_cast<int> ((i+1) / (divider))*(100/howManyOut)) << "%: ";
                        stat.mean_varOpt();
                        stat.var_of_varOpt();
                }


                // debug small number
                //std::cout << i << ": \n";
                //stat.mean_varOpt();
                //stat.var_of_varOpt();
        }


        if (filename != "./TenNet_1/FTODDUMP_MC") {
                 Tnet.calcVal();
        }
        return 0;
}
