#include "./tensor.h"

Tensor::Tensor() {
}


Tensor::Tensor(std::string identifier, bool isCC) {
        identification = identifier;
        isComplexConj = isCC;
        dimension = 0;
}


Tensor::Tensor(std::string identifier, std::string fn, int dim, bool isCC) {
        isComplexConj = isCC;
        identification = identifier;
        file2tensor(fn, dim);
}


Tensor::~Tensor() {
        // pointer array must be deleted
        if (DimSize != NULL) delete[] DimSize;
        for (size_t i = 0; i < dimension; i++) {
                if (idx_order.at(i) != NULL) delete[] idx_order.at(i);
                if (spDist.at(i) != NULL) delete[] spDist.at(i);
        }
}


void Tensor::setDimProbaties(int dim, int *DSize) {
        setDimension(dim);
        setDimSize(DSize);

        nbrEntries = 1;
        for (size_t j = 0; j < dimension; j++) {
                nbrEntries *= DimSize[j];
        }

        Tens.resize(nbrEntries);
        std::fill(Tens.begin(), Tens.end(), 0);

        idx_order.resize(dimension);
        for (size_t i = 0; i < dimension; i++) {
                idx_order.at(i) = new int[dimension];
                idx_order.at(i)[0] = i;
                for (size_t j = 1; j < dimension; j++) {
                        idx_order.at(i)[j] = (j <= i ? j-1 : j);
                }
        }

        for (size_t i = 0; i < dimension; i++) {
                spDist.at(i) = new double[nbrEntries];
                for (size_t j = 0; j < nbrEntries; j++) {
                        spDist.at(i)[nbrEntries] = 0;
                }
        }
}


void Tensor::setDimension(int dim) {
        dimension = dim;
        spDist.resize(dimension);
}


void Tensor::setDimSize(int *DSize) {
        DimSize = new int[dimension];

        uniInt.resize(dimension);
        for (size_t i = 0; i < dimension; i++) {
                DimSize[i] = DSize[i];
                uniInt.at(i) = new std::uniform_int_distribution<int>(0, DimSize[i]-1);
        }
}


int Tensor::Ten2Arr(int *idx) {
        int pos = 0;
        int shift = 1;

        for (std::size_t i = 0; i < dimension; i++) {
                pos += shift*idx[i];
                shift *= DimSize[i];
        }

        return pos;
}


int* Tensor::Arr2Ten(int pos) {
        int *idx = new int[dimension];
        idx[0] = pos%DimSize[0];
        int mod = DimSize[0];

        for (size_t i = 1; i < dimension; i++) {
                idx[i] = (pos%(mod*DimSize[i])-pos%mod)/mod;
                mod *= DimSize[i];
        }

        return idx;
}


void Tensor::file2tensor(std::string fn, size_t dim) {
        filename = fn;
        std::ifstream in;
        in.open(filename.c_str());
        if (!in.is_open()) {
                std::cout << "READ went wrong!!!" << "\n";
                return;
        }

        if (!in) {
                std::cout << "Cannot open file.\n";
                return;
        }

        std::string line;
        getline(in, line);

        int *DimensionSize = new int[dim];
        for (size_t i = 0; i < dim; i++) {
                in >> DimensionSize[i];
        }
        setDimProbaties(dim, DimensionSize);
        delete [] DimensionSize;

        // std::cout << "{" << DimSize[0] << "," << DimSize[1] << "," << DimSize[2] <<"}" << '\n';

        getline(in, line);
        getline(in, line);

        //================ read in Tensor ================
        int *index = new int[dimension];
        double *value = new double[dimension];
        for (std::size_t i = 0; i < nbrEntries; i++) {
                in >> value[0] >> value[1];
                for (size_t j = 0; j < dimension; j++) {
                        in >> index[j];
                        index[j] -= 1;
                }
                if (isComplexConj) value[1] *= -1;
                set(index, std::complex<double> (value[0]*1.0 + value[1]*1.0i));
                // std::cout << "{" << index[0] << "," << index[1] << "," << index[2] <<"}" << '\n';
        }
        delete[] value;
        delete[] index;
        in.close();
        //================================================

        genSpliceDistribution();
}


void Tensor::set(int *idx, std::complex<double> val) {
        Tens[Ten2Arr(idx)] = val;
}
void Tensor::set(int pos, std::complex<double> val) {
        Tens[pos] = val;
}


std::complex<double> Tensor::get(int *idx) {
        return Tens[Ten2Arr(idx)];
}
std::complex<double> Tensor::get(int pos) {
        return Tens[pos];
}


double Tensor::mag(int *idx) {
        return std::abs(get(idx));
}
double Tensor::mag(int pos) {
        return std::abs(get(pos));
}


void Tensor::print(int *idx) {
        std::cout << identification << ": {" << idx[0] << "," << idx[1] << ",";
        std::cout << idx[2] << "} --> ";
        std::cout << Tens[Ten2Arr(idx)] << '\n';
        return;
}


int* Tensor::Arr2Dist(int pos, int* DimensionSize) {
        int *idx = new int[dimension];
        idx[0] = pos%DimensionSize[0];
        int mod = DimensionSize[0];
        std::cout << dimension << '\n';
        for (size_t i = 1; i < dimension; i++) {
                std::cout << idx[i] << '\n';
                idx[i] = (pos%(mod*DimensionSize[i])-pos%mod)/mod;
                mod *= DimensionSize[i];
        }
        return idx;
}

// ################################# here something wrong ---> guess DimensionSize wrong order
int Tensor::Dist2Arr(int* idx, int* DimensionSize) {
        int pos = 0;
        int multiplier = 1;
        for (size_t i = 0; i < dimension; i++) {
                pos += multiplier*idx[i];
                multiplier *= DimensionSize[i];
        }
        return pos;
}


void Tensor::genSpliceDistribution() {
        int *idx = new int[dimension];
        for (size_t id = 0; id < dimension; id++) {  // gen spliceDist for every index
                for (size_t i = 0; i < dimension; i++) {
                        idx[i] = 0;
                }

                double summation = 0;
                for (size_t i = 0; i < nbrEntries; i++) {
                        summation += mag(Dist2Arr(idx, DimSize));
                        spDist.at(id)[i] = summation;
                        if ((i+1)%DimSize[id] == 0) {  // normalize splice distribution
                                for (int k = 0; k < DimSize[id]; k++) {
                                        spDist.at(id)[i-k] /= summation;
                                }
                                summation = 0;
                        }

                        for (int *j = idx_order.at(id); j < idx_order.at(id)+dimension; j++) {
                                if (idx[*j]+1 < DimSize[*j]) {
                                        idx[*j]++;
                                        break;
                                } else {
                                        idx[*j] = 0;
                                }
                        }
                }
                // saveSliceDistribution(id);
        }

        delete[] idx;
}


void Tensor::saveSliceDistribution(std::size_t id) {
        std::ofstream myfile;
        std::ostringstream Dist_fname;
        Dist_fname << "./spDist/spdist_" << identification << "_" << id << "-";
        for (size_t i = 0; i < dimension; i++) {
                Dist_fname << DimSize[i] << "_";
        }
        Dist_fname << ".dat";
        myfile.open(Dist_fname.str());

        int *idx = new int[dimension];

        for (size_t i = 0; i < dimension; i++) {
                idx[i] = 0;
        }

        for (size_t i = 0; i < nbrEntries; i++) {
                for (size_t j = 0; j < dimension; j++) {
                        myfile << idx[j] << " ";
                }
                myfile << spDist.at(id)[i] << " " << spDist.at(id)[i] - (spDist.at(id)[i-1] != 1 ? spDist.at(id)[i-1] : 0)  << '\n';
                if ((i+1)%DimSize[id] == 0) {
                        myfile << '\n';
                }

                for (int *j = idx_order.at(id); j < idx_order.at(id)+dimension; j++) {
                        if (idx[*j]+1 < DimSize[*j]) {
                                idx[*j]++;
                                break;
                        } else {
                                idx[*j] = 0;
                        }
                }
        }

        myfile.close();
        std::cout << "spliceDist -> written to file: Coutput\"spdist_" << identification << "_" << id << "-";
        for (size_t i = 0; i < dimension; i++) {
                std::cout << DimSize[i] << "_";
        }
        std::cout << ".dat\"\n";
        delete[] idx;
}


int Tensor::getISampIndex(int rand_id, int* idx) {
        idx[rand_id] = 0;
        int spDistArrStart = Dist2Arr(idx, DimSize);
        double rand_uniform = Gen::uni_real_Dist(Gen::generator);
        // std::cout << "rand: " << rand_uniform << '\n';
        int upper = DimSize[rand_id]-1;
        int lower = -1;
        while (upper - lower > 1) {
                int mid = (upper+lower)/2;
                // std::cout << lower << "  -  " << mid << "  -  " << upper << '\n';
                if (rand_uniform < spDist.at(rand_id)[spDistArrStart+mid]) {
                        upper = mid;
                } else {
                        lower = mid;
                }
        }
        return upper;
}

double Tensor::getISampProb(int rand_id, int* upper_idx) {
        // std::cout << "getISampProb: " << rand_id << " - {" << upper_idx[0] << "," << upper_idx[1] << "," << upper_idx[2] << "}\n";
        if (upper_idx[rand_id] == 0) {
                // std::cout << "upper_idx[rand_id] == " << 0 << '\n';
                return spDist.at(rand_id)[Dist2Arr(upper_idx, DimSize)];
        } else {
                // std::cout << "upper_idx[rand_id] != " << 0 << '\n';
                int *lower_idx = new int[dimension];
                for (size_t i = 0; i < dimension; i++) {
                        lower_idx[i] = upper_idx[i];
                }
                lower_idx[rand_id] -= 1;
                // std::cout << "lower_idx = {" << lower_idx[0] << "," << lower_idx[1] << "," << lower_idx[2] << "}\n";
                int tempEntryPos = Dist2Arr(upper_idx, DimSize);
                double prob = spDist.at(rand_id)[tempEntryPos] - spDist.at(rand_id)[tempEntryPos-1];
                // std::cout << Dist2Arr(upper_idx, DimSize) << "  vs.  " << Dist2Arr(lower_idx, DimSize) << '\n';
                // std::cout << "prob = " << spDist.at(rand_id)[Dist2Arr(upper_idx, DimSize)] << " - " << spDist.at(rand_id)[Dist2Arr(lower_idx, DimSize)] << " = " << prob  << '\n';
                delete[] lower_idx;

                // std::cout << '\n';
                // std::cout << '\n';
                // for (size_t i = 0; i < nbrEntries; i++) {
                //         std::cout << i << " " << spDist.at(rand_id)[i] << '\n';
                // }
                // std::cout << '\n';
                // std::cout << '\n';

                return prob;
        }
}

int Tensor::getUniDistIndex(std::size_t id) {
        return (*uniInt.at(id))(Gen::generator);
}
