Abstract
======
This project work analyzed tensor networks from the second-order M{\o}ller--Plesset (MP2) perturbation theory to calculate their energy contributions more efficiently.
In the first chapter we will translate the terms into tensor networks and further on stochastically evaluate its expected value.
For optimization of the calculation a Monte-Carlo method called inversion method was used. 

For further details please look at the file '[./tex/pwork.pdf](./tex/pwork.pdf)'.
