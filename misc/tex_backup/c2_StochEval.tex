\chapter{Stochastic evaluation of tensor networks}
\label{ch:stocheval}

{
\begin{LARGE}
What is the test system, carbon diamond with \dots
\end{LARGE}
}

For the comparison whether a stochastic evaluation improves over the deterministic evaluation a system with an Intel Core i7-7700 processor with hyper-threading enabled and the optimization level of \texttt{-Ofast} for the \texttt{g++} compiler was used. The deterministic evaluation of $E^{\mathrm{MP2,2}}$ on such a system was estimated to take 25 hours. For $E^{\mathrm{MP2,1}}$ the computing time would have been smaller, since one can exploit the symmetry of this problem.

\section{Monte-Carlo evaluation}
The \emph{stochastic variable} $X$, also known as random variable, assumes a numerical value upon drawing. Such a variable $X$ can be either discrete or continuous. Over the course of an arbitrary amount of drawings the probabilities of an outcome must not change and each drawing must be independent of each other. The probability of an outcome is given by the \emph{cumulative distribution function} (CDF) which is defined as
\begin{equation}
	\mathbb{P} (X \leq x) = P_X(x)  \text{.}
\end{equation}


The function $p_{X}$ is the derivative of the CDF and is called \emph{probability density function} (PDF) which is used to specify the probability density of a certain outcome.
\begin{align}
p_{X}(x) = \dfrac{d}{dx} P_X(x)
\end{align}
For discrete random variables the PDF is a difference rather than a differential
\begin{align}
p_{X}(x) = \dfrac{1}{1}\left( P_{X}(x) - P_{X}(x-1) \right) = P_{X}(x) - P_{X}(x-1) \text{.} 
\end{align}
The CDF is a non-decreasing function with following properties
\begin{align*}
\lim _{x \rightarrow-\infty} P_{X}(x)=0, \quad \lim _{x \rightarrow+\infty} P_{X}(x)=1
\end{align*}
which means for the PDF that $\lim _{x \rightarrow \pm \infty} p_{X}(x)=0$ and that it has to be \emph{normalized}
\begin{equation}
\int_{-\infty}^{\infty}\kern-2.6em\sum dx \  p(x) = 1 \text{.\footnotemark}
\label{eq:PDF_normalized}
\end{equation} 
\footnotetext{The symbol $\int\kern-1.1em\sum$ denotes that the same equation can be used for discrete as well as continuous distributions. If this is not the case only one of the symbols is used.}

The \emph{$n$-th moment} of a stochastic function $O(X)$ for a stochastic variable $X$ distributed according to the PDF $p_{X}$ is defined as 
\begin{equation}
	\mathbb{E}\left[ O(X)^{n}; p_{X} \right] = \int_{-\infty}^{\infty} dP_{X}(x) \ O(x)^{n}
\end{equation}

Of the PDF $p_{X}$ of the CDF $P_{X}$ exists, then the $n$-th moment can be written as
\begin{equation}
	\mathbb{E}\left[ O(X)^{n}; p_{X} \right] = \ \int_{-\infty}^x\kern-2.6em\sum dx \ O(x)^{n}\  p_{X}(x) \text{.}
\end{equation}

For $n=1$ we will get the first moment which is called the \emph{expectation value}.
\begin{equation}
	\mathbb{E}\left[ O(X); p_{X} \right] = \ \int_{-\infty}^x\kern-2.6em\sum dx \ O(x) \ p_{X}(x) = o \text{ \footnotemark}
\label{eq:1stMoment}
\end{equation}
\footnotetext{Deterministic variables are denoted by lower case letters.}
The probability distribution $p_{X}$ in the argument of the expectation value means that the random variable $X$ is distributed according to this PDF, which is also denoted as $X \sim p_{X}$. The reason for this notation will be clarified in the next section \ref{sec:importanceSampling}.\\


Another key aspect of a PDF is the \emph{variance} of a distribution which is defined as
\begin{align}
	\mathbb{V}[O(X); p_X] &= \mathbb{E}\left[\left(O(X) - \mathbb{E}[O(X);p_{X}]\right)^{2};p_{X}\right] \nonumber \\
	 &= \ \int_{-\infty}^{\infty}\kern-2.6em\sum dx \ (O(x) - \mathbb{E}[O(X);p_{X}])^{2} \  p(x)
\end{align}

The variance can be further simplified and expressed only by the first and second moment. By expanding the square and using the normalization of the PDF and the definition of the first moment, we get

\begin{align}
	\mathbb{V}[O(X); p_X] 
	 	&= \ \int_{-\infty}^{\infty}\kern-2.6em\sum dx \ \left(O(x) - \mathbb{E}[O(X);p_{X}]\right)^{2} \  p(x) \nonumber\\
	 	&= \ \int_{-\infty}^{\infty}\kern-2.6em\sum dx \ \left(O(x)^{2} - 2 \ O(x) \ \mathbb{E}[O(X);p_{X}] +  \mathbb{E}[O(X);p_{X}]^{2} \right) \  p_{X}(x) \nonumber\\
		&= \ \int_{-\infty}^{\infty}\kern-2.6em\sum dx \ O(x)^{2} \ p_{X}(x) - \mathbb{E}[O(x);p_{X}]^{2} \nonumber\\
	 	&= \mathbb{E}[O(x)^{2};p_{X}] - \mathbb{E}[O(X);p_{X}]^{2} \text{.}
\label{eq:var}
\end{align}

% %%%% tried proofing it for complex observables but changed my mind then. PROOF NOT FINISHED
%\begin{align}
%	\mathbb{V}[O(X); p_X] 
%	 	&= \mathbb{E}\left[\left|O(X) - \mathbb{E}[O(X);p_{X}]\right|^{2};p_{X}\right] \nonumber\\
%		&= \mathbb{E}\left[\left(O(X) - \mathbb{E}[O(X);p_{X}]\right)\left(O(X) - \mathbb{E}[O(X);p_{X}]\right)^{*};p_{X}\right] \nonumber\\	 
%		&= \mathbb{E}\left[\left|O(X)\right|^{2} - \left|\mathbb{E}[O(X);p_{X}]\right|^{2} -
%		O(X)^{*} \mathbb{E}[O(X);p_{X}]  - O(X) \mathbb{E}[O(X);p_{X}]^{*};p_{X}\right] \nonumber\\	 	
%	 	&= \ \int_{-\infty}^{\infty}\kern-2.6em\sum dx \ \left(\left|O(X)\right|^{2} - \left|\mathbb{E}[O(X);p_{X}]\right|^{2} - O(X)^{*} \mathbb{E}[O(X);p_{X}]  -
%		O(X) \mathbb{E}[O(X);p_{X}]^{*}\right) \  p_{X}(x) \nonumber\\
%	 	&= \ \int_{-\infty}^{\infty}\kern-2.6em\sum dx \ \left(O(x)^{2} - 2 \ O(x) \ \mathbb{E}[O(X);p_{X}] +  \mathbb{E}[O(X);p_{X}]^{2} \right) \  p_{X}(x) \nonumber\\
%		&= \ \int_{-\infty}^{\infty}\kern-2.6em\sum dx \ O(x)^{2} \ p_{X}(x) - \mathbb{E}[O(x);p_{X}]^{2} \nonumber\\
%	 	&= \mathbb{E}[O(x)^{2};p_{X}] - \mathbb{E}[O(X);p_{X}]^{2}
%\label{eq:var}
%\end{align}



By drawing $M$ samples $X_{i} \sim p_{X}$ both the expectation value and the variance can be estimated unbiased by 
\begin{align}
\braket{O}_{p_{X}} = \dfrac{1}{M} \sum_{i}^{M} O(x_i) \qquad \text{and} \qquad 
S^{2}_{O,p_{X}} = \dfrac{1}{M-1} \sum^{M}_{i} (O(x_{i}) - \braket{O}_{p_{X}})^{2} \text{.}
\end{align}

Since these are unbiased estimator we assume, without proof, that the expectation value of these corresponds to the deterministic value.
\begin{equation}
	\mathbb{E}\left[\braket{O}\right] = o
\end{equation}


In the same way the expectation value of a tensor network can be stochastic evaluated where $X_{i}$ corresponds to a combination of indices which are uniformly distributed and will result in 
\begin{equation}
\braket{E^{\mathrm{MP2,1}}} = \dfrac{N}{M} \sum_{m=1}^{M} \left.\Gamma^{*}\right.^{a_{m}G_{m}}_{i_{m}} \left.\Gamma\right.^{b_{m}}_{j_{m}G_{m}} \left.\Gamma\right.^{i_{m}}_{a_{m}F_{m}} \left.\Gamma^{*}\right.^{j_{m}F_{m}}_{b_{m}}
\label{eq:expVal_var_est}
\end{equation} 
We have to multiply the equation with $N$ the total number of indices combinations since we approximate the expectation value of a single combination. This approximation can be optimized by choosing $X_i$ or the indices of a tensor network not uniformly but rather more intelligent by using a different distribution from which the stochastic variables are sampled. This method is called importance sampling and can reduce the variance significantly if done right.




\section{Importance sampling}
\label{sec:importanceSampling}

The main idea behind importance sampling is to prioritize areas which contribute more to the expectation value and weighting them accordingly. That this does not change the expectation value can be easily shown as follows.

\begin{align}
	\mathbb{E}[O(X); p_{X}]
		&= \ \int_{-\infty}^{\infty}\kern-2.6em\sum dx \ O(x) \  p_{X}(x) = \ \int_{-\infty}^{\infty}\kern-2.6em\sum dx \ O(x) \  \underbrace{\dfrac{p_{X}(x)}{f_{X}(x)}}_{=W(x)} f_{X}(x) \nonumber\\
		&= \ \int_{-\infty}^{\infty}\kern-2.6em\sum dx \ O(x) \ W(x) \ f_{X}(x) \nonumber\\
		&= \mathbb{E}[O(X)W(X); f_{X}] = \mathbb{E}\left[ \tilde{O}(X); f_{X} \right]
\label{eq:exVal_is}
\end{align}

Applying a similar method on the variance helps to understand how a reduction of the variance is achieved and which properties we have to demand from $f_{X}$. For that we will start with the transformed observable $\tilde{O}$ and the corresponding PDF $f_{X}$.

\begin{align*}
	\mathbb{V}\left[ \tilde{O}(X); f_{X} \right] 
		&= \mathbb{E}\left[ \tilde{O}(X)^{2}; f_{X}\right] - \mathbb{E}[\tilde{O};f_{X}]^{2} \\
		&= \ \int_{-\infty}^{\infty}\kern-2.6em\sum dx \ \tilde{O}(x)^{2} \ f_{X}(x) - \left( \ \int_{-\infty}^{\infty}\kern-2.6em\sum dx \ \tilde{O}(x) \ f_{X}(x) \right)^{2}
\end{align*}
 
For simplicity's sake we assume that $O(x) \geq 0$, which lets us choose $f_{X}(x) = \frac{O(x) p_{x}(x)}{o}$. By integrating over $x$ and using the definition of the expectation value \eqref{eq:expVal_var_est} we see that the PDF $f_{X}$ is normalized.

\begin{equation*}
	\int_{-\infty}^{\infty}\kern-2.6em\sum dx \ f_{X}(x) = \dfrac{1}{o} \ \int_{-\infty}^{\infty}\kern-2.6em\sum dx \ O(x) \ p_{x}(x) = 1
\end{equation*}
The first moment gives us $\tilde{O}(x) = O(x) W(x) = O(x) \frac{p_{X}(x)}{f_{X}(x)}$  from which we can derive that $\tilde{O}(x) = o$. 
\begin{align*}
	\mathbb{V}\left[ \tilde{O}(X); f_{X} \right] 
		&= o^{2} \ \int_{-\infty}^{\infty}\kern-2.6em\sum dx \ f_{X}(x) - \left( o \ \int_{-\infty}^{\infty}\kern-2.6em\sum dx \ f_{X}(x) \right)^{2} \\
		&= o^{2}-o^{2} = 0
\end{align*}



In reality this is not possible since $o$ is what we want to calculate or rather approximate. Then choosing $f_{X}(x)$ in a way that it behaves similar to $O(x)p_{X}(x)$ resulting in $\tilde{O}(x) \approx const$ and satisfying all other characteristics of a PDF will be sufficient enough. 


The same unbiased estimation of the expectation value and the variance as in equation (\ref{eq:expVal_var_est}) can be applied for the transformed observable $\tilde{O}$. 
\begin{equation}
	\braket{\tilde{O}}_{f_{X}} = \dfrac{1}{M} \sum_{i}^{M} \tilde{O}(x_{i}) \qquad \text{and} \qquad \sigma^{2}_{\tilde{O}, f_{X}} = \dfrac{1}{M} \sum^{M}_{i} \left( \tilde{O}(x_{i}) - \braket{\tilde{O}}_{f_{X}}\right)^{2}
\end{equation}
\cite{AC02552795, AC02445899, AC00103169, rubinstein, AC08207698, HandbookMCMC}


\section{Inversion method}

The \emph{inversion method}, also called inverse transform method, is used to generate pseudo-random numbers from any probability distribution given a CDF. Since $F_{X}: \mathbb{R} \mapsto [0,1)$ its inverse results in a function $F_{X}^{-1}: [0,1) \mapsto \mathbb{R}$. By using random variables $U \sim \mathbb{U}(0,1)$ we are able to generate a random variable $X = F_{X}^{-1}(U)$ which are distributed according to the PDF $f_{X}$.


\subsubsection{Simple Example}


As a simple example we can look at the function $O(x) = \frac{1}{21}\left(\Theta(x + 10) - \Theta(x-10)\right)$ and calculate the expectation value $\mathbb{E}[O(X);p_{X}] = \frac{40}{2121}$ by using discrete uniform distributed variables $p_{X}(x) = \mathcal{U}\{-50,50\}$\footnote{$p_{X}(x) = \dfrac{1}{b-a + 1}$ and $\mathcal{U}\{a,b\}$} and a truncated normal distribution $f_{X}(x) =  N e^{\frac{(x-\mu)^{2}}{2 \cdot \sigma^{2}}}$ where $\mu = 0$, $\sigma=10$ and the normalization constant $N$ so that $1=\sum^{50}_{x=-50} f_{X}(x)$ is satisfied.

\begin{figure}[H]
   \centering
       \subfigure[]{
       	\includegraphics[width=.47\textwidth]{figures/ex_inv/ex_dist.pdf}
       }
       \subfigure[PDF $f_{X}$ and CDF $F_{X}$]{
       	\includegraphics[width=.47\textwidth]{figures/ex_inv/ex_cdf.pdf} 
       }
 \caption{}
 \label{fig:ex_dist-cdf}
\end{figure}

Since we use a different distributed random variable $X \sim f_{X}$, as seen in figure \ref{fig:ex_amount-mody}(a), we have correct for that with a transformation of the observable $O \rightarrow \tilde{O} = O(x)\frac{p_{X}(x)}{f_{X}(x)}$ which can be see in figure \ref{fig:ex_amount-mody}(b).


\begin{figure}[H]
   \centering
       \subfigure[samples of random number $X$ with the dashed line showing the expected distribution of samples]{
       	\includegraphics[width=.47\textwidth]{figures/ex_inv/ex_amount.pdf}
       }
       \subfigure[transformed observable $\tilde{O}$]{
       	\includegraphics[width=.47\textwidth]{figures/ex_inv/ex_mody.pdf} 
       }
 \caption{}
 \label{fig:ex_amount-mody}
\end{figure}

\begin{figure}[H]
   \centering
   \subfigure[importance sampling]{
       	\includegraphics[width=.47\textwidth]{figures/ex_inv/ex_mean.pdf}
       }
       \subfigure[normal distributed sampling]{
       	\includegraphics[width=.47\textwidth]{figures/ex_inv/ex_meanNV.pdf} 
       }
 \caption{}
 \label{fig:ex_mean}
\end{figure}

By comparing the scale of the $\Delta O$ axis of figure \ref{fig:ex_mean}(a) and (b) one can easily see the advantage of importance sampling over uniform sampling.



\section{Applied distributions for importance sampling}

For a tensor different distributions can be generated. The only restriction on which distributions can be used depends on which indices in the configuration $\{g,a,i,f,b,j\}$ are already known. The following examples can be applied on all the permutations of the indices as well.

\subsubsection{Uniform drawing}
For some runs uniform sampling was used to determine the first index, where a random number in the range of e.g.\ in case of $a$ within $[0,N_a)$ is chosen. Another reason for uniform drawing is to reduce the bias when using sampling methods which depend on known indices.
\begin{align*}
	\mathbb{P}(A=a) \propto 1 
\end{align*}


\subsubsection{Tracing}
The tracing distributions are similar to marginal distribution of joint probability distribution. Any summation over an arbitrary number of indices is possible with this method as e.g.
\begin{align}
	\mathbb{P}_a(I=i) = \mathbb{P}(I=i|A=a) \propto \sum_{g} \left| \Gamma^{a}_{ig} \right| \nonumber\\
	\text{or} \qquad	\mathbb{P}(A=a) \propto \sum_{i, g} \left| \Gamma^{a}_{ig} \right|
\end{align}

\subsubsection{Compound drawing}
Compound drawing could be compared to the conditional probability distribution and is able to determine any number of indices of a single tensor at once, where the indices which are not sampled have to be known. With the three dimensional $\Gamma$ tensor these distributions as in 
\begin{align*}
	\mathbb{P}_{a,i}(G=g) = \mathbb{P}(G=g | A=a,I=i) \propto \left| \Gamma^{a}_{ig} \right| \nonumber\\
	\text{or} \quad \mathbb{P}_{g}(A=a,I=i) = \mathbb{P}(A=a,I=i|G=g) \propto \left| \Gamma^{a}_{ig} \right| \nonumber\\
	\text{or} \quad 	\mathbb{P}(G=g,A=a,I=i) \propto \left| \Gamma^{a}_{ig} \right| \nonumber
\end{align*}
are possible including all of its permutation.
