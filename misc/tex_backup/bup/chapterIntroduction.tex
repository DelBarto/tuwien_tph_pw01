\chapter{Introduction}
\label{ch:intro}
The main goal is to write a program which uses a more efficient way to estimates the expected value of an tensor network. How such a tensor network is created and the results are shown here mainly on the example of the second-order M{\o}ller-Plesset perturbation theory (MP2) $E^{\mathrm{MP2}}$, which refers in this case to an energy correction. \cite{doi:10.1063/1.3466765} \\


Due to periodic system and therefore the periodic boundary conditions the electron orbits correspond to Bloch functions. \[ \braket{\mathbf{r} + \mathbf{R} | \psi_a} = \braket{\mathbf{r} | \psi_a} e^{i\mathbf{k_a}\mathbf{R}} \]

For such a system the MP2 energy correction is given by

\begin{equation}
E^{\mathrm{MP2}} = -\dfrac{1}{\Omega_k^4} \int_{\Omega_k} d\mathbf{k}_i \, d\mathbf{k}_j \, d\mathbf{k}_a \, d\mathbf{k}_b \, \sum_{i,j,a,b} \dfrac{\tilde{V}^{a b}_{\ i j} \left(2 \tilde{V}^{ab}_{\ ij} - \tilde{V}^{ba}_{\ ij} \right)^{*} }{\epsilon_a + \epsilon_b - \epsilon_i -  \epsilon_j}
\label{eq:Emp2}
\end{equation}
with $\Omega_k$ the volume of the first Brillouin zone and the coulomb integral

\[
\tilde{V}^{a b}_{\ i j}  = e^2 \int d\mathbf{r'} \, d\mathbf{r} \dfrac{\braket{\psi_i | \mathbf{r}} \braket{\mathbf{r} | \psi_a} \braket{\psi_j | \mathbf{r'}}\braket{\mathbf{r'} | \psi_b} }{\lvert \mathbf{r} - \mathbf{r'} \rvert}
\text{ .} \]

Where $\braket{\mathbf{r} | \psi} = \psi(\mathbf{r})$ is the spacial representation of spin orbital. Furthermore the denominator will be defined as $\Delta^{ab}_{ij} := \epsilon_i +  \epsilon_j - \epsilon_a - \epsilon_b$.

\begin{equation}
E^{\mathrm{MP2}} = \dfrac{1}{\Omega_k^4} \int_{\Omega_k} d\mathbf{k}_i \, d\mathbf{k}_j \, d\mathbf{k}_a \, d\mathbf{k}_b \, \sum_{i,j,a,b} \dfrac{\tilde{V}^{a b}_{\ i j} \left(2 \tilde{V}^{ab}_{\ ij} - \tilde{V}^{ba}_{\ ij} \right)^{*} }{\Delta^{ab}_{ij}}
\label{eq:Emp2}
\end{equation}

The integral over the first Brillouin zone can be rewritten by using the Bloch function to find $\mathbf{k}_b = \mathbf{k}_i + \mathbf{k}_j - \mathbf{k}_a - \mathbf{K}$ where $\mathbf{K}$ is the momentum that brings $\mathbf{k}_b$ back in the first Brillouin zone.
By executing the integral over $\mathbf{K}$, it reduces to 
\begin{equation}
E^{\mathrm{MP2}} = \dfrac{1}{\Omega_k^3} \int_{\Omega_k} d\mathbf{k}_i \, d\mathbf{k}_j \, d\mathbf{k}_a \, \sum_{i,j,a,b} \dfrac{ \tilde{V}^{a b}_{\ i j} \left(2 \tilde{V}^{ab}_{\ ij} - \tilde{V}^{ba}_{\ ij} \right)^{*} }{\Delta^{ab}_{ij} } \text{.}
\end{equation}


Further more by discretization of the integral $1/\Omega_k \int_{\Omega_k} \rightarrow 1/N_k \sum_{k}$, with $N_k$ samples, we get

\begin{equation}
E^{\mathrm{MP2}} = \dfrac{1}{N_{k}^{3}} \sum_{k} \sum_{i,j,a,b} \dfrac{ \tilde{V}^{a b}_{\ i j} \left(2 \tilde{V}^{ab}_{\ ij} - \tilde{V}^{ba}_{\ ij} \right)^{*} }{ \Delta^{ab}_{ij} } \text{.}
\end{equation}

The expansion of the numerator $\tilde{V}^{a b}_{\ i j} \left(2 \tilde{V}^{ab}_{\ ij} - \tilde{V}^{ba}_{\ ij} \right)^{*}$ will result in the two terms 

\begin{align*}
E^{\mathrm{MP2}} &= \dfrac{2}{N_k^3}  \sum_{i,j,a,b} \dfrac{ \tilde{V}^{a b}_{\ i j} \left( \tilde{V}^{ab}_{\ ij} \right)^{*} }{\Delta^{ab}_{ij} } \  - \  \dfrac{1}{N_k^3} \sum_{i,j,a,b} \dfrac{\tilde{V}^{a b}_{\ i j} \left( \tilde{V}^{ba}_{\ ij} \right)^{*} }{\Delta^{ab}_{ij} } = \quad E^{\mathrm{MP2,1}} + E^{\mathrm{MP2,2}} \text{ .} 
\end{align*}

The first term $E^{\mathrm{MP2,1}}$ is much easier to handle and can be calculated much quicker then the second one $E^{\mathrm{MP2,2}}$, which is the reason why the focus here will only be on the second term.

\begin{equation}
E^{\mathrm{MP2,2}} = - \dfrac{1}{N_k^3} \sum_{i,j,a,b} \dfrac{\tilde{V}^{a b}_{\ i j} \left( \tilde{V}^{ba}_{\ ij} \right)^{*} }{\Delta^{ab}_{ij} } 
\end{equation}

By using a Laplace transformation like
\begin{align*}
\sum_{a,b,i,j} \dfrac{1}{\epsilon_i + \epsilon_j - \epsilon_a -  \epsilon_b} = \sum_{a,b,i,j} \dfrac{1}{ \Delta^{ab}_{ij} } &= - \sum_{a,i} \int_0^\infty d\tau e^{ {\Delta^{ab}_{ij}} \, \tau } \\
& \approx - \sum_{a,b,i,j}\sum_n w_n e^{  { \Delta^{ab}_{ij} } \, \tau_n}
\end{align*}
the MP2 energy correction can be also rewritten as

\begin{align}
E^{\mathrm{MP2,2}} &= - \dfrac{1}{N_k^3} \sum_{i,j,a,b} \dfrac{\tilde{V}^{a b}_{\ i j} \left(\tilde{V}^{ab}_{\ ij}\right)^{*} }{ \Delta^{ab}_{ij} } \\
 &= \dfrac{1}{N_k^3} \sum_{a,b,i,j}\sum_n w_n e^{  { \Delta^{ab}_{ij} } \tau_n} \tilde{V}^{a b}_{\ i j} \left(\tilde{V}^{ab}_{\ ij}\right)^{*} \text{.}
\end{align}

Now the prefactor from the Laplace transformation can be split up equally and used to redefine the tensors $V$ like

\begin{align}
E^{\mathrm{MP2,2}}_{n} &= \dfrac{1}{N_k^3} \sum_{a,b,i,j} 
 \underbrace{\left( w_{n}^{\frac{1}{2}} e^{ \frac{1}{2}  { \Delta^{ab}_{ij} } \tau_n} \tilde{V}^{a b}_{\ i j} \right) }_{=: \, \left( V^{a b}_{\ i j} \right)_{n} }
 \underbrace{ \left(  w_{n}^{\frac{1}{2}} e^{ \frac{1}{2}  { \Delta^{ab}_{ij} } \tau_n} \left(\tilde{V}^{ab}_{\ ij}\right)^{*} \right) }_{=: \, \left(V^{ab}_{\ ij}\right)^{*}_{n}}\\
 &= \sum_{i,j,a,b} \left( V^{a b}_{\ i j} \right)_{n} \left(V^{ab}_{\ ij}\right)^{*}_{n} \text{.}
\end{align}

These two newly defined tensors $V^{a b}_{\ i j}$ are also dependent on the indices $n$, which is however omitted for simplicity's sake and since we look at only one specific value of $n$. 

This leaves us with
\begin{equation}
E^{\mathrm{MP2,2}} = \sum_{i,j,a,b}  V^{a b}_{\ i j}  \left(V^{ab}_{\ ij}\right)^{*} \text{ .}
\end{equation}

Since these tensors are fairly large, $V^{ab}_{\ ij}$ can be reduced to two significant smaller tensors with a Tensor Rank Decomposition (TRD), which is described in more detail in \cite{doi:10.1063/1.4977994}. Such a decomposition can be also achieved algebraically by i.g. with the Cholesky decomposition. One specific way of the decomposition, i.g. $V^{ab}_{\ ij} \approx \sum_{G} \left. \Gamma^{*} \right. ^{a}_{iG} \Gamma^{bG}_{j}$, result in
\begin{equation}
E^{\mathrm{MP2,2}} = \sum_{i,j,a,b} \sum_{g,f}  \Gamma^{a}_{ig} \left. \Gamma^{*} \right.^{bg}_{j}\Gamma^{b}_{jf} \left. \Gamma^{*} \right.^{af}_{i} \text{ .}
\label{eq:decompEMP2}
\end{equation}

Such a decomposition can also be represented as a graph, called tensor network, such like this

\begin{figure}[H]
\begin{center}
\begin{tikzpicture}[-,>=stealth',shorten >=1pt,auto,node distance=4cm,
                    semithick]
  \tikzstyle{every state}=[fill=none,draw=black,rectangle,text=black]

  \node[state] (A)	[]					{$\ \Gamma^{a}_{ig} \ $};
  \node[state] (B)	[right of=A] 		{$\left. \Gamma^{*} \right.^{bg}_{j}$};
  \node[state] (C) 	[below of=A]		 	{$\left. \Gamma^{*} \right. ^{af}_{i} $};
  \node[state] (D) 	[right of=C] 		{$\ \Gamma^{b}_{jf} \ $};
  
  \path (A) edge 				node {$g$} (B)
            edge [bend right] 	node {$a$} (C)
            edge [bend left] 	node {$i$} (C)
        (B) edge [bend right]	node {$b$} (D)
	        edge [bend left]		node {$j$} (D)
        (C) edge 			    	node {$f$} (D);
\end{tikzpicture}
\end{center}
\caption{tensor network of the $E^{\mathrm{MP2,2}}_{n}$ term}
\label{fig:tennet1}
\end{figure}

A different decomposition could for example like
\begin{figure}[H]
\begin{center}
\begin{tikzpicture}[-,>=stealth',shorten >=1pt,auto,node distance=4cm,
                    semithick]
  \tikzstyle{every state}=[fill=none,draw=black,rectangle,text=black]

  \node[state] (A)	[]					{$\ \Gamma^{a}_{ig} \ $};
  \node[state] (B)	[right of=A] 		{$\left. \Gamma^{*} \right.^{bg}_{j}$};
  \node[state] (C) 	[below of=A]		 	{$\left. \Gamma^{*} \right. ^{af}_{j} $};
  \node[state] (D) 	[right of=C] 		{$\ \Gamma^{b}_{if} \ $};
  
  \path (A) edge 				node {$g$} (B)
            edge 			 	node {$a$} (C)
            edge 			 	node [pos=0.3] {$i$} (D)
        (B) edge 				node {$b$} (D)
	        edge 				node [pos=0.7] {$j$} (C)
        (C) edge 			    	node {$f$} (D);
\end{tikzpicture}
\end{center}
\caption{}
\label{fig:tennet2}
\end{figure}


As mentioned at the beginning of this chapter, this described process is only valid for the MP2 perturbation theory, but ways to create such tensor networks for other approximation methods can be found i.g. in the book \textit{"Modern Quantum Chemistry"} \cite{AC00655843}.	