\chapter{Solution Approach}\label{ch:solution}

To estimate the in previous chapter \ref{task} described equation (\ref{eq:decompEMP2est}), a Monte Carlo Method has been used. The main idea behind that, is to approximate the sum over all indices as in (\ref{eq:decompEMP2}) by just taking some in a certain way random distributed indices configurations as written in (\ref{eq:decompEMP2est}).

By not picking the indices uniformly but more intelligent, the variance can be lowered significantly which means, the arithmetic mean can be determined with more certainty. The lowering of the variance is influenced by how good the distribution of the taken samples is. To get a feeling for which distributions and combinations work good, different distributions were generated from which the indices were sampled. \cite{AC00103169}


\section{Inversion Method - Basics}

\subsection{Importance sampling}
\label{subsec:importance_sampling}

The expected value of an observable $O(x)$ can be calculated like 
\begin{equation}
\mathbb{E}[O(x);f)] = \overline{O}= \int\kern-1.6em\sum  dx' \, O(x') f(x') \quad \text{\footnotemark}
\label{eq:expValue}
\end{equation}
\footnotetext{The symbol $\int\kern-1.1em\sum$ denotes that the same equation can be used for discrete as well as continues distributions. If this is not the case just a $\sum$ or $\int$ is used.}
where $f(x)$ equals a probability density function (PDF) $\mathbb{P}(X = x) = f(x)$ and $x \sim f(X)$.
If $f(x)$ is not known, an arithmetic mean can be calculated like
\begin{equation}
\mathbb{E}_{M}[O(x);u] = \overline{O}_{M} \approx \dfrac{1}{M} \sum_{i=0}^{M} O\left(x_{i}\right) \qquad \text{where $x_{i} \sim u(x_{i})$}
\label{eq:arithmetic_mean}
\end{equation}
where the samples are chosen uniformly $u$. This approximation works well if sufficient enough samples are taken and for the limit where $M \rightarrow \infty$ the arithmetic mean converges to the real expected value of $O(x)$. But one can easily see that this method will not always work that well, as for example if $x$ is normal distributed we would have to choose a values uniformly within $(-\infty,\infty)$ and therefore include areas where the probability is almost equal to $0$. These areas do not contribute a lot to the mean and therefore are not that important and so at this point importance sampling comes to play. \\


By rewriting the equation (\ref{eq:expValue}) like
\begin{align}
\mathbb{E}[O(x);f] &= \ \int\kern-1.6em\sum  dx' \, O(x')  \dfrac{f(x')}{p(x')} p(x') \\
&= \ \int\kern-1.6em\sum  dx' \, O(x') W(x') p(x') = \mathbb{E}[O(x)W(x);p]
\end{align}
with $p(x)$ as the new distribution after which we want to sample $x$ from and $W(x)$ is the weight with which a sample is corrected, we can take now more samples from ares which are more significant. This means we should choose our $p(x)$ so that values with higher contributions are chosen more often than values with low contribution to the expected value.


The same transformation can be used on the arithmetic mean (\ref{eq:arithmetic_mean}) which gives us
\begin{align}
	\mathbb{E}_{M} \left[ O(x) W(x_i);p \right] &= \dfrac{1}{M} \sum_i^M O(x_{i}) \dfrac{u(x_{i})}{p(x_{i})}  \qquad \text{where $x_{i} \sim  p(x_{i})$} \\
	&= \dfrac{1}{M} \sum_i^M O(x_{i}) W(x_{i}) \nonumber \text{ .}
\end{align}


By sampling from a different distribution the variance changes. As mentioned before $p(x)$ should closer represent the actual distribution of $x$ which we defined as $f(x)$ . This also decreases the sample variance and therefore yields a better approximation with less samples. \cite{AC02552795} \cite{rubinstein} \cite{AC03876809} \cite{PhysRevD.98.030001}

\subsection{Inversion Method}
The function $f(x)$ is usually a uniform distribution which is the easiest way to sample $x$ from as well as most of the random number generators produce uniform distributed numbers. Now for generating a $x \sim p(x)$, different methods can be applied. Here the focus lies on the so called inversion method.\\

So to generate such a distributed $x_i$ with the inversion method a cumulative distribution function (CDF) is needed. The CDF is defined as 
\[ P_X(x) = \mathbb{P}(X \leq x) = \int_{-\infty}^x\kern-2.6em\sum  dx' \, p(x') \] 
with the properties
\begin{align*}\lim _{x \rightarrow-\infty} P_{X}(x)=0, \quad \lim _{x \rightarrow+\infty} P_{X}(x)=1 \quad \text{and} \\
 P_{X}(b)-P_{X}(a)=\mathbb{P}(a<X \leq b)=\int_{a}^{b}\kern-2em\sum p_{X}(x) d x \text{ .}
\end{align*}

In the discrete case this also means that the $p(x_i)$ can be calculated really easy, like
\begin{align*}
p(x_i) = P_X(x_{i+1}) - P_X(x_{i}) \text{.}
\end{align*}

So to finally generate random numbers after which to sample from, the inverse function $P^{-1}(y) = x_i$ is needed, where $y$ is a uniform distributed number within $[0,1)$.  

Again for the discrete case using the inverse function is as simple as searching between which values the number falls. As for the continues case an inverse function can be much harder to find or even impossible, but continues function can always be discretised. \cite{AC00103169} \cite{PhysRevD.98.030001}

\subsubsection{Simple Example}

As a simple example the expected value of a function $y(x) = \Theta(x + 10) - \Theta(x-10)$, where $x \in [-50,50]$, can be calculated. By using a sampling distribution similar to a normal distribution $p(x) = N e^{\frac{(x-\mu)^{2}}{2\sigma^{2}}}$, with $N$ as normalization factor and $\mu = 0, \sigma = 8$.
The function $f(x) = 1/101$, which is the needed for the transformation to Gaussian distributed random numbers. All this functions can be seen in \ref{fig:ex_dist-cdf}(a).


\begin{figure}[H]
   \centering
       \subfigure[]{
       	\includegraphics[width=.47\textwidth]{figures/ex_inv/ex_dist.png}
       }
       \subfigure[CDF $P(x)$ and PDF p(x)]{
       	\includegraphics[width=.47\textwidth]{figures/ex_inv/ex_cdf.png} 
       }
 \caption{}
 \label{fig:ex_dist-cdf}
\end{figure}

With this Gaussian distributed random numbers, as seen in figure \ref{fig:ex_amount-mody}(a), the to function $y(x)$ has to be modified to $y(x)\frac{f(x)}{p(x)}$ which can be seen in figure \ref{fig:ex_amount-mody}(b)

\begin{figure}[H]
   \centering
       \subfigure[]{
       	\includegraphics[width=.47\textwidth]{figures/ex_inv/ex_amount.png}
       }
       \subfigure[]{
       	\includegraphics[width=.47\textwidth]{figures/ex_inv/ex_mody.png} 
       }
 \caption{}
 \label{fig:ex_amount-mody}
\end{figure}

\begin{figure}[H]
   \centering
   \subfigure[importance sampling]{
       	\includegraphics[width=.47\textwidth]{figures/ex_inv/ex_mean.png}
       }
       \subfigure[normal distributed sampling]{
       	\includegraphics[width=.47\textwidth]{figures/ex_inv/ex_meanNV.png} 
       }
 \caption{different runs, which show the how the difference between mean and real exp}
 \label{fig:ex_mean}
\end{figure}

By comparing the scale of the $\Delta x$ axis of fig. \ref{fig:ex_mean} (a) and (b) one can easily see the advantage of importance over uniform sampling, due to the faster convergence of the mean.



\section{Inversion Method applied on Tensor Networks}

To take a sample value of a tensor network, an indices configuration $\{ a,b,i,j,f,g \} $ is needed. This indices have to be sampled in a similar way as mentioned in the example before, where combinations of the following distributions were used and compared against each other.  

\subsection{Distributions}

\subsubsection{Tracing}
The tracing distributions are alike to marginal distribution of the tensor.
\begin{align*}
	\mathbb{P}_a(I=i) = \mathbb{P}(I=i|A=a) \propto \sum_{g} \left| \Gamma^{a}_{ig} \right|
\end{align*}

It is also possible to sum over an arbitrary number of indices. With three dimensions there is only one possible distribution left, where the distribution does not depend on any other indices.

\begin{align*}
	\mathbb{P}(A=a) \propto \sum_{i, g} \left| \Gamma^{a}_{ig} \right|
\end{align*}

This way of generating distributions can be done not only for the shown indices but for any permutation of them.

\subsubsection{Compound drawing}
Compound drawing is able to determine any number of, up to all, indices in one go. In the case of the three dimensional $\Gamma$ tensor, the distributions are proportional to the tensor like

\begin{gather*}
	\mathbb{P}_{a,i}(G=g) = \mathbb{P}(G=g | A=a,I=i) \propto \left| \Gamma^{a}_{ig} \right| \\
	\text{or} \quad \mathbb{P}_{g}(A=a,I=i) = \mathbb{P}(A=a,I=i|G=g) \propto \left| \Gamma^{a}_{ig} \right| \nonumber\\
	\text{or} \quad 	\mathbb{P}(G=g,A=a,I=i) \propto \left| \Gamma^{a}_{ig} \right| \nonumber
\end{gather*}
including all permutations of the indices.

\subsubsection{Uniform drawing}
For some runs a uniform sampling was used to determine the first index, where a random number in the range of i.g. in case of $a$ within $[0,N_a)$ is chosen. 
\begin{align*}
	\mathbb{P}(A=a) \propto \dfrac{1}{N_a} 
\end{align*}


\section{Statistics}

\subsection{Interval estimation}

For an estimator of the mean and deviation the law of large numbers can be used, which states that the average of a repeated experiment behaves normal distribute. \cite{book:11854}
This means that the confidential interval for normal distributions can be used. Since the variance $\sigma^{2}$ is not known, 

\begin{align}
\left[ \overline { X } _ { M } - t _ { M - 1 ; \frac { 1 + \gamma } { 2 } } \sqrt { \frac { S _ { M } ^ { 2 } } { M } } , \overline { X } _ { M } + t _ { M - 1 ; \frac { 1 + \gamma } { 2 } } \sqrt { \frac { S _ { M } ^ { 2 } } { M } } \right]
\label{eq:intervalEstimation}
\end{align}

has to be used which depends on the Student's t-distribution $t_{n-1;\frac{1+\gamma}{2}}$. The difference of the t-distribution and normal distribution is neglect able for a large $n$ as it is in our case.


For a coverage probability of $\alpha = 0.99$ we get there $\dfrac{1-\gamma}{2} = \dfrac{1.99}{2} = 0.995  \quad \Longrightarrow \quad t_{\infty, 0.995} = z_{0.995} = 2.576$. \cite{AC08993515}


%To make an estimation of the sample variance
%$$\left[ \frac { ( n - 1 ) S _ { n } ^ { 2 } } { \chi _ { n - 1 ; \frac { 1 + \gamma } { 2 } } ^ { 2 } } , \frac { ( n - 1 ) S _ { n } ^ { 2 } } { \chi _ { n - 1 ; \frac { 1 - \gamma } { 2 } } ^ { 2 } } \right]$$
%is used, where as ${ \chi _ { n - 1 ; \frac { 1 - \gamma } { 2 } } ^ { 2 } }$ referes to the $\chi^{2}$-distribution.
%
%
%distribution of variance
%$$ \mu^{2} = |\mu|^{2} = \mu \mu^{*}$$
%    $$ \mu^{4} = \dfrac{1}{M} \sum^{M}_{i}   \left(
%    					|x_{i}|^{4} -
%    					4 |x_{i}|^{2} \Re \left \{ x_{i} \mu^{*} \right \}+ 
%    					4|x_{i}|^{2} |\mu|^{2} + 
%					2 \Re \left \{ x_{i}^{2} \left. \mu^{*} \right.^{2} \right \} -	
%    					3 |\mu|^{4} \right) $$
%    $$\mathbb{V}\left[ S^{2} \right] = \dfrac{1}{M} \left( \mu^4 - (S^2)^2 \right)$$



\subsection{Sample Variance Distribution}

Most of the time the true variance $\sigma^{2}$ is unknown or, as in this case, it would take to long to calculate, but it is still needed. For such a case the sample variance $S^{2}$ can be calculated. 

\begin{align}
\mathbb{V}_{M}(X) = S_{M}^{2} = \frac{M}{M-1} \underbrace{\left(\frac{1}{M} \sum_{i=1}^{M} |X_{i}-\overline{X}_{M} |^{2}\right)}_{\text{biased sample variance}} = \frac{1}{M-1} \sum_{i=1}^{M}\left|X_{i}-\overline{X}_{M}\right|^{2} 
\label{eq:sampleVarianceDistribution}
\end{align}


The prefactor $\frac{M}{M-1}$ is the correction term to turn a biased to an unbiased sample variance $S_{M}^{2}$ and is called Bessel's correction. This correction can be understood as the remaining degrees of freedom, since the mean is unknown.  \cite{ADictionaryofStatistics}

Because the sample sizes $M \sim 10^{9}$  will be fairly large, we can neglected in good conscience the Bessel's correction.

\begin{align*}
S_{M}^{2} &= \frac{1}{M} \sum_{i=1}^{M}\left( |X_{i}|^{2} - X_{i}^{*} \overline{X}_{M} - X_{i} \overline{X}_{M}^{*} + |\overline{X}_{M}|^{2} \right) \\
&= \frac{1}{M} \left( \sum_{i=1}^{M} |X_{i}|^{2} - \overline{X}_{M} \sum_{i=1}^{M}X_{i}^{*}  - \overline{X}_{M}^{*} \sum_{i=1}^{M} X_{i}  + \sum_{i=1}^{M} |\overline{X}_{M}|^{2} \right) \\
&= \frac{1}{M} \left( \sum_{i=1}^{M} |X_{i}|^{2} - M \underbrace{\overline{X}_{M} \overline{X}_{M}^{*}}_{= |\overline{X}_{M}|^{2}} - M \underbrace{\overline{X}_{M}^{*} \overline{X}_{M}}_{= |\overline{X}_{M}|^{2}}  + M |\overline{X}_{M}|^{2} \right) \\
&= \frac{1}{M} \left( \sum_{i=1}^{M} |X_{i}|^{2} - M |\overline{X}_{M}|^{2} \right) = \frac{1}{M} \sum_{i=1}^{M}  \left( |X_{i}|^{2} - |\overline{X}_{M}|^{2} \right)
\end{align*}



Since $S^{2}_{M}$ is also an estimation of the true variance, the variance of the sample variance can be calculated. 
\begin{align*}
\mathbb{V} \left(S_{M}^{2} \right) = \frac{1}{M} \left( \frac{1}{M} \sum_{i=1}^{M} \left( |X_{i}|^{4} - 4  |X_{i}|^{2} \Re (X_{i}  \overline{X}_{M}^{*})+ 4 |X_{i}|^{2} |\overline{X}_{M}|^{2} +\right.\right.\\
\left. \left. + 2 \Re ( X_{i}^{2} (\overline{X}_{M}^{*})^{2} ) - 3 |\overline{X}_{M}|^{4} \right) - \left(S_{M}^{2}\right)^{2} \right)
\end{align*}

The formula above can be also derived easily by expending the definition of the variance 
\begin{align*}
\mathbb{V} (S_{M}^{2}) &= S_{M}^{4} - \left( S_{M}^{2} \right)^{2} \\
&=  \frac{1}{M^{2}} \left( \sum_{i=1}^{M}  \left( |X_{i} - \overline{X}_{M}|^{2} \right) \right)^{2} - \left( S_{M}^{2} \right)^{2} 
\end{align*}

The variance of the sample variance is used not to underestimate the interval estimation of the mean.

\cite{book:981959}
\cite{book:13960}

\newpage

\subsection{T-Test}
\label{subsec:t-test}

For the mean there can also be carried out a T-test where the mean $\mu_0$ has to be known, but not the variance $\sigma^{2}$. 

$$T = \frac { \overline { X } _ { M } - \mu _ { 0 } } { \sqrt { S _ { M } ^ { 2 } / M } }$$

This test requires the distribution of the  mean to follow a normal distributed. For this test we assume one of the three cases below as a null hypothesis $H_{0}$ and test it against $H_{1}$.

\begin{itemize}
\item[--] $H _ { 0 } : \mu \leq \mu _ { 0 } $ against $ H _ { 1 } : \mu > \mu _ { 0 } : $ reject, when $ T > t _ { M - 1 ; 1 - \alpha }$

\item[--] $H _ { 0 } : \mu \geq \mu _ { 0 } $ against $ H _ { 1 } : \mu < \mu _ { 0 } : $ reject, when $ T < - t _ { M - 1 ; 1 - \alpha }$


\item[--] $H _ { 0 } : \mu = \mu _ { 0 } $ against $ H _ { 1 } : \mu \neq \mu _ { 0 } : $ reject, when $ | T | > t _ { M - 1 ; 1 - \alpha / 2 }$  

\end{itemize}

Here the last $H_{0}$ was used, to easily verify if the approximated mean is within the fluctuation band. \cite{100stattests}

%F\"ur die Varianz einer Normalverteilung:
%$$T = \frac { ( M - 1 ) S _ { M } ^ { 2 } } { \sigma _ { 0 } ^ { 2 } }$$
%
%$H_{0}: \sigma^{2} \geq \sigma_{0}^{2} $ gegen $ H_{1}: \sigma^{2} < \sigma_{0}^{2}: $ verwerfen, wenn $ T < \chi _ {n-1; \alpha}^{2}$
%
%
%$H _ { 0 } : \sigma ^ { 2 } = \sigma _ { 0 } ^ { 2 } $ gegen $ H _ { 1 } : \sigma ^ { 2 } \neq \sigma _ { 0 } ^ { 2 } : $ verwerfen, wenn $ T > \chi _ { M - 1 ; 1 - \alpha / 2 } ^ { 2 } $ oder $ T < \chi _ { M - 1 ; \alpha / 2 } ^ { 2 }$
%
%
%$H _ { 0 } : \sigma ^ { 2 } \leq \sigma _ { 0 } ^ { 2 } $ gegen $ H _ { 1 } : \sigma ^ { 2 } > \sigma _ { 0 } ^ { 2 } : $ verwerfen, wenn $ T > \chi _ { M - 1 ; 1 - \alpha } ^ { 2 }$


