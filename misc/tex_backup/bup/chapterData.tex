\chapter{Results}

To get a feeling of how different combinations of sampling methods perform, several have been compared to each other and we will further take a look on two of those. 

The first one of these combinations is a simpler one which utilizes uniform and compound drawing. The second one, which has a better conversion to the mean due to way smaller variance, uses different tracing methods and also compound drawing.

For this example the same tensor network as shown in figure \ref{fig:tennet1} was used. Furthermore the runs in the graphs are called after their sampling combination for easier distinction. This naming style uses the order of indices $\{ g,a,i,f,b,j \}$ which then results in names like $uccucc$ for the first and $tttccc$ for the second example.

%\begin{figure}[H]
%   \centering
%       \subfigure[]{
%       	\begin{tikzpicture}[-,>=stealth',shorten >=1pt,auto,node distance=4cm, semithick]
%  				\tikzstyle{every state}=[fill=none,draw=none,rectangle,text=black]
%
% 					\node[state] (A) []				{$\Gamma^{a}_{ig}$};
%  					\node[state] (B) [right of=A] 	{$\left. \Gamma^{*} \right.^{bg}_{j}$};
%					\node[state] (C) [below of=A]	{$\left. \Gamma^{*} \right. ^{af}_{i} $};
%					\node[state] (D) [right of=C]	{$\Gamma^{b}_{jf}$};
%  
% 				 \path 	(A) 	edge 				node {$g$} (B)
%           					edge [bend right] 	node {$a$} (C)
%            					edge [bend left] 	node {$i$} (C)
%        					(B)	edge [bend right]	node {$b$} (D)
%	        					edge [bend left]		node {$j$} (D)
%        					(C)	edge 			    	node {$f$} (D);
%			\end{tikzpicture}
%       }
%       \subfigure[nummeration of tensors]{
%			\begin{tikzpicture}[-,>=stealth',shorten >=1pt,auto,node distance=4cm, semithick]
%  				\tikzstyle{every state}=[fill=none,draw=black,rectangle,text=black]
%
% 				\node[state] (A)		[]				{$1$};
% 				\node[state] (B)		[right of=A] 	{$2$};
%  				\node[state] (C) 	[below of=A]		{$3$};
%  				\node[state] (D) 	[right of=C] 	{$4$};
%  
% 				 \path 	(A) 	edge 				node {} (B)
%           					edge [bend right] 	node {} (C)
%            					edge [bend left] 	node {} (C)
%        					(B)	edge [bend right]	node {} (D)
%	        					edge [bend left]		node {} (D)
%        					(C)	edge 			    	node {} (D);
%			\end{tikzpicture}
%   }
% \caption{}
% \label{fig:tensnet-tensornumberation}
%\end{figure}



\subsubsection{uniform and compound drawing (uccucc)}


The following methods and order were used for sampling. 
\begin{packed_enum}
\item \ $g$ - uniform sampling, $P(G=g) \propto \dfrac{1}{N_g}$
\item \ $a$ and $i$ - compound drawing, $P_{g}(A=a,I=i) \propto |\Gamma^{a}_{ig}|$ 
\item \ $f$ - uniform sampling, $P(F=f) \propto \dfrac{1}{N_f}$
\item \ $b$ and $j$ - compound drawing, $P_{f}(B=b,J=j) \propto |\Gamma^{b}_{jf}|$ 
\end{packed_enum}

Of the two combinations mentioned before this one result in a poorer convergence behavior and a larger variance, which can be easily seen later in a the figure \ref{fig:meanIntervallEst_uccucc-ttTccc} or by comparing the values in the table \ref{tab:statval}.

The following plots show how different statistical parameter evolve with an increase in the sample size.

\newpage
\vspace{-1cm}
\begin{figure}[H]
  \makebox[\linewidth][c]{
\subfigure[]{
	       	\includegraphics[width=0.65\textwidth]{figures/uccucc/mean-uccucc.png}
	       }  
  
  \subfigure[small sample sizes are cut off]{
	       	\includegraphics[width=0.65
  \textwidth]{figures/uccucc/mean_lim-uccucc.png} 
	       }
}  
  \caption{evolution of the mean while sample size increases}
  \label{fig:mean-uccucc}
\end{figure}

\vspace{-1.5cm}
\begin{figure}[htp!]
  \makebox[\linewidth][c]{
\subfigure[estimation of the variance as in eq. (\ref{eq:intervalEstimation})]{
	       	\includegraphics[width=0.6\textwidth]{figures/uccucc/meanIntervallUnderest-uccucc-3.png}
	       }  
  
  \subfigure[including the sample variance distribution]{
	       	\includegraphics[width=0.6\textwidth]{figures/uccucc/meanIntervallEstZoomLim-uccucc-3.png} 
	       }
}  
  \caption{evolution of the mean including the estimated confidence interval}
  \label{fig:meanIntervallEst-uccucc}
\end{figure}
\newpage
With a closer look at figure \ref{fig:meanIntervallEst-uccucc}(b) a grey line can be seen where the endcaps of the errorbars are. This is the envelope of variance which is proportional to $\dfrac{1}{\sqrt{M}}$.




\begin{figure}[H]
\centering
\includegraphics[width=0.65\textwidth]{figures/uccucc/t-Test-uccucc-3.png}
\caption{T-test, with a conditional probability $\alpha=1\%$ }
\label{fig:t-Test-uccucc}
\end{figure}

Since the $T$-value is within the dashed line we can accept our assumption of the mean, as mentioned before in \ref{subsec:t-test}. 


\newpage

\subsubsection{tracing and compound drawing (tttccc)}


Here the sampling sequence and methods are slightly different and do not relay on any predetermined uniform sampled indices. 


The order in which the indices are sampled is as follows:
\begin{packed_enum}
\item \ $i$ - tracing over $g$ and $a$, $P(I=i) \propto \sum_{g,a} |\Gamma^{ag}_{i}|$
\item \ $a$ - tracing over $f$, $P_{i}(A=a) \propto \sum_{f} | \left. \Gamma^{*} \right.^{af}_{i}|$
\item \ $g$ - tracing over $i$, $P_{a}(G=g) \propto \sum_{i} | \left. \Gamma \right.^{a}_{ig}|$
\item \ $b \ \text{\&} \ j$ - compound drawing, $P_{g}(B=b,J=j) \propto |\left. \Gamma^{*} \right.^{bg}_{j}|$
\item \ $f$ - compound drawing, $P_{b,j}(F=f) \propto |\left. \Gamma \right.^{bf}_{j}|$
\end{packed_enum}

\vspace{-1cm}
\begin{figure}[H]
  \makebox[\linewidth][c]{
\subfigure[]{
	       	\includegraphics[width=0.55\textwidth]{figures/ttTccc/mean-ttTccc.png}
	       }  
  
  \subfigure[small sample sizes are cut off]{
	       	\includegraphics[width=0.55
  \textwidth]{figures/ttTccc/mean_lim-ttTccc.png} 
	       }
}  
  \caption{evolution of the mean while sample size increases}
  \label{fig:mean-ttTccc}
\end{figure}




\begin{figure}[H]
  \makebox[\linewidth][c]{
\subfigure[estimation of the variance]{
	       	\includegraphics[width=0.6\textwidth]{figures/ttTccc/meanIntervallUnderest-ttTccc-3.png}
	       }  
  
  \subfigure[considering the sample variance distribution]{
	       	\includegraphics[width=0.6\textwidth]{figures/ttTccc/meanIntervallEstLim-ttTccc-3.png} 
	       }
}  
  \caption{evolution of the mean including the estimated confidence interval}
  \label{fig:meanIntervallEst-ttTccc}
\end{figure}

\vspace*{-1.5cm}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.65\textwidth]{figures/ttTccc/t-Test-ttTccc-3.png}
	\caption{T-test, with a conditional probability $\alpha=1\%$ }
	\label{fig:t-Test-ttTccc}
\end{figure}
Here once again are the values contained within the dashed lines. For that we can assume the arithmetic mean converges to the genuine expected value. This also may let us assume that the program works correctly.




\subsubsection{comparison}
\vspace{-1cm}
\begin{figure}[H]
  \makebox[\linewidth][c]{
\subfigure[]{
	       	\includegraphics[width=0.6\textwidth]{figures/uccucc-ttTccc/mean-uccucc-ttTccc.png}
	       }  
  
  \subfigure[more visible on one run]{
	       	\includegraphics[width=0.6\textwidth]{figures/uccucc-ttTccc/mean_lim-uccucc-ttTccc.png} 
	       }
}  
  \caption{evolution of the mean of two different sampling methods}
  \label{fig:mean-uccucc-ttTccc}
\end{figure}
\vspace{-1cm}
The tendency to converge to the mean can be seen well in the figures \ref{fig:mean-uccucc-ttTccc}. This argument is also supported if looked at the T-test in figure \ref{fig:t-Test-uccucc} and \ref{fig:t-Test-ttTccc}, where for the null hypothesis the genuine mean, which is known, was used.
\vspace{-1cm}
\begin{figure}[H]
\centering
	       	\includegraphics[width=0.45\textwidth]{figures/uccucc-ttTccc/variance-uccucc-ttTccc-6.png}  
  \caption{evolution of the variance of the two sampling methods}
  \label{fig:variance-uccucc-ttTccc}
\end{figure}

\vspace{-1cm}
The in subsection \ref{subsec:importance_sampling} stated argument, that better chosen sampling distribution decreases the variance, can also be seen quite well in the figure \ref{fig:variance-uccucc-ttTccc} or figure \ref{fig:meanIntervallEst_uccucc-ttTccc}.


\begin{figure}[H]
  \makebox[\linewidth][c]{
\subfigure[estimation of the variance as in eq. (\ref{eq:intervalEstimation})]{
	       	\includegraphics[width=0.6\textwidth]{figures/uccucc-ttTccc/meanIntervallUnderest-uccucc-ttTccc-2.png}
	       }  
  
  \subfigure[taking the sample variance distribution into account]{
	       	\includegraphics[width=0.6\textwidth]{figures/uccucc-ttTccc/meanIntervallEstZoomLim-uccucc-ttTccc-2.png} 
	       }
}  
  \caption{for better visibility only the first two runs of previous }
  \label{fig:meanIntervallEst_uccucc-ttTccc}
\end{figure}


For the tensor network with the known mean $ \mu_0  = 1.96197 \cdot 10^{-13}$ and a sample size of $M = 1 167 998 976$, the two above described procedure will results in \vspace{-5mm}
\begin{table}[h!]
\makebox[\linewidth][c]{
		\begin{tabular}{c|c|c|c|c}
			 & $ \bar{X}_{M} \quad \left[ 10^{-13} \right] $ & $ \Delta x_{M} = \bar{X}_{M}-\mu_{0} \quad \left [10^{-16} \right]$ & $S^{2} \quad \left[ 10^{-21} \right]$ & $t_{M-1; \frac{1+\gamma}{2}} \sqrt{\frac{S_{n}^{2}}{M}} \quad \left[ 10^{-16} \right]$ \\ 
			\hline \hline
	
			\multirow{3}{*}{\rotatebox{90}{uccucc}} & 1.97727 & 15.3 & 3.183 & 42.527\\ 
			\cline{2-5} 
			 & 1.96547 & 3.5 & 3.15 & 42.304\\ 
			\cline{2-5}  
			 & 1.97326 & 11.29 & 3.189 & 42.564\\ 
%			\hhline{~====} 
%			avg. & 1.972 & 10.03 & 3.174 & 42.465\\ 

		
			 
			\hline \hline
			\multirow{3}{*}{\rotatebox{90}{tttccc}} & 1.96186 & -0.11 & 9.68273 & 7.417\\ 
			\cline{2-5} 
			& 1.96107 & -0.9 & 9.65759 & 7.407\\ 
			\cline{2-5} 
			& 1.96233 & 0.36 & 9.65554 & 7.406\\ 
%			\hhline{~====} 
%			avg. & 1.96175 & -0.2167 & 9.66529 & 7.41\\ 
		\end{tabular}

}
\caption{statistical values of the sampling methods}
\label{tab:statval}
\end{table}


\subsubsection{different tensor network}

In the same way as previous discussed other tensor networks can be approximated. So similar combinations as before have been used for the network shown in figure \ref{fig:tensnet-3}.

\begin{figure}[H]
   \centering
       	\begin{tikzpicture}[-,>=stealth',shorten >=1pt,auto,node distance=2cm, semithick]
  				\tikzstyle{every state}=[fill=none,draw=black,rectangle,text=black]

 					\node[state]	(A)	[]					{$A$};
  					\node[state]	(B)	[right of = A] 		{$A$};
					\node[state]	(E)	[below right of = B]	{$B$};
					\node[state]	(F)	[below of = E] 		{$B$};
  					\node[state]	(D)	[below left of = F]	{$A$};
					\node[state]	(C)	[left of = D]		{$A$};
					\node[state]	(G)	[right of = E]		{$A$};
					\node[state]	(H)	[right of = F]		{$A$};
					
 				 \path 	(A) 	edge 					node {} 	(B)
           					edge 	[bend right] 	node {} 	(C)
            					edge		[bend left] 		node {} 	(C)
        					(B)	edge		[bend left]		node {} 	(E)
	        					edge						node {} 	(D)
        					(C)	edge 			    		node {} 	(D)
        					(D)	edge		[bend right]		node {} 	(F)
        					(E)	edge						node	 {}	(F)	
        						edge						node	 {}	(G)	
        					(F) edge						node {}	(H)
        					(G) edge		[bend right]		node {}	(H)
        						edge		[bend left]		node {}	(H)		
        					;
			\end{tikzpicture}
 \caption{}
 \label{fig:tensnet-3}
\end{figure}