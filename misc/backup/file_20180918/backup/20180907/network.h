// includes
#include "tensor.h"
#include <vector>

// variables

// functions
struct TensorNet {
        std::vector<Tensor*> tensors;
        unsigned int **network=NULL;
        size_t nbrTensors;
        size_t nbrIndices;

        TensorNet();
        TensorNet(size_t nbrT, size_t nbrI, unsigned int **Tnet, std::vector<Tensor*> tens);
        ~TensorNet();

        void setTnet(size_t nbrT, size_t nbrI, unsigned int **Tnet);
        void setTlist(std::vector<Tensor*> tens);
        unsigned int getNetwork(size_t tens, size_t ind);
        float calcExpVal();

};

TensorNet::TensorNet(){
}

TensorNet::TensorNet(size_t nbrT, size_t nbrI, unsigned int **Tnet, std::vector<Tensor*> tens){
        setTnet(nbrT, nbrI, Tnet);
        setTlist(tens);
}

TensorNet::~TensorNet(){
        if (network != NULL) {
                for (size_t i = 0; i < nbrTensors; i++) {
                        delete[] network[i];
                }
                delete[] network;
        }
        for (size_t i = 0; i < tensors.size(); i++) {
                delete tensors.at(i);
        }
}

void TensorNet::setTnet(size_t nbrT, size_t nbrI, unsigned int **Tnet){
        nbrTensors = nbrT;
        tensors.resize(nbrTensors);
        nbrIndices = nbrI;
        network = Tnet;
}

void TensorNet::setTlist(std::vector<Tensor*> tens){
        tensors = tens;
}

unsigned int TensorNet::getNetwork(size_t tens, size_t idx){
        return network[tens][idx];
}

float TensorNet::calcExpVal(){
        size_t maxi = 1; // one could ask why not unsigned long int -> reason long stores two chunks of 16 bit where as size_t does not do that therefore it is more efficient to read in a size_t variable from the storage
        size_t maxID[nbrIndices];
        size_t indices[nbrIndices];

        double Tsum_real=0;
        double Tsum_compelx=0;

        size_t indicesCounter = 0;
        for (size_t j = 0; j < nbrIndices; j++) {
                indices[j]=1;
                for (size_t k = 0; k < nbrTensors; k++) {
                        if(network[k][j] != 0) {
                                maxID[indicesCounter] = tensors.at(k)->DimSize[getNetwork(k,j)-1];
                                std::cout << "/* " << k << " "<<  j <<" */" << " " << indicesCounter << ": " << maxID[indicesCounter]  << '\n';
                                maxi  *= maxID[indicesCounter];
                                indicesCounter++;
                                break;
                        }
                }
        }

        std::cout << "maxi: " << maxi<< '\n';
        std::cout << "maxID: " << "[" << maxID[0] << "]" << "[" << maxID[1] << "]";
        std::cout << "[" << maxID[2] << "]" << "[" << maxID[3] << "]" << "[" << maxID[4] << "]" << "[" << maxID[5] << "] \n";


        // Sum over all indices [i][j][a][b][g][f] like SUM_{i,j,a,b,g,f} C(g,a,i) * CONJG( C(g,b,j) ) * C(f,b,j) * CONJG( C(f,a,i) )
        for (size_t i = 0; i < maxi; i++) {

                // multiply Tensors for one indices combination [i][j][a][b][g][f] like C(g,a,i) * CONJG( C(g,b,j) ) * C(f,b,j) * CONJG( C(f,a,i) )
                double sumpart_real = 1;
                double sumpart_complex = 1;
                for (size_t j = 0; j < nbrTensors; j++) {
                        int idx[tensors.at(j)->dimension];

                        // generate indices to grab the Tensor entry
                        for (size_t k = 0; k < nbrIndices; k++) {
                                if (network[j][k] != 0) {
                                        idx[network[j][k]-1] = indices[k];
                                }
                        }

                        // multyply tensors for one part of the sum
                        sumpart_real *= tensors.at(j)->get(idx,0);
                        sumpart_complex *= tensors.at(j)->get(idx,1);
                }
                // add part of the sum to the sum
                Tsum_real += sumpart_real;
                Tsum_compelx += sumpart_complex;


                // print out ~1000 indices combinations
                if ((i%10000000==0) || ((i+100) > maxi)) {
                        std::cout << "[" << indices[0] << "]" << "[" << indices[1] << "]";
                        std::cout << "[" << indices[2] << "]" << "[" << indices[3] << "]" << "[" << indices[4] << "]" << "[" << indices[5] << "]  -->  ";
                        std::cout << Tsum_real << "  " << Tsum_compelx << "  -->  " << sumpart_real << "  " << sumpart_complex << " \n";
                }



                // increase indices, to loop through them
                for (size_t j = 0; j < nbrIndices; j++) {
                        if(indices[j]+1 <= maxID[j]) {
                                indices[j]++;
                                break;
                        }else{
                                indices[j] = 1;
                        }
                }

        }
        std::cout << "Tsum_real = "<< Tsum_real << "\n";
        std::cout << "Tsum_compelx = "<< Tsum_compelx << "\n";





        return 0.0;
}
