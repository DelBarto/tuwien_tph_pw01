//==================================
// include guard
#ifndef TENSOR_H   // if x.h hasn't been included yet...
#define TENSOR_H   //   #define this so the compiler knows it has been included
//==================================


struct Tensor
{
        std::string filename="";
        std::string identification="";
        double* Tens =NULL;
        double* TMag =NULL;
        size_t size_TMag; //size of TMag
        std::size_t dimension=0;
        int *DimSize = NULL;
        // cache last Tens access
        int *cacheIndices;
        double cacheTens[2]; //real and complex Value
        bool isComplexConj = false;

        std::vector<double*> spDist;


        Tensor();
        Tensor(std::string identifier, bool isCC);
        Tensor(std::string identifier, std::string fn, int dim, bool isCC);
        ~Tensor();

        void setDimProbaties(int *DSize, int dim);
        void setDimension(int dim);
        void setDimSize(int *DSize);
        int Ten2Arr(int *idx, bool comp);
        int* Arr2Ten(int pos);
        void file2tensor(std::string fn);
        double get(int *idx, bool complex);
        double mag(int *idx);
        void printTenIdx(int *idx, bool comp, bool compcon);
        void Arr2Dist(int pos, int *idx, int* DimensionSize);
        int Dist2Arr( int* idx, int* DimensionSize);
        void genSpliceDistribution();
        void saveSliceDistribution(int index);
        double getISampIndex(int rand_id, int* idx);
        int getUniDistIndex(int index);

};


Tensor::Tensor(){
}

Tensor::Tensor(std::string identifier, bool isCC){
        identification = identifier;
        isComplexConj = isCC;
        dimension = 0;
}

Tensor::Tensor(std::string identifier, std::string fn, int dim, bool isCC){
        isComplexConj = isCC;
        identification = identifier;
        setDimension(dim);
        file2tensor(fn);

}

Tensor::~Tensor(){
        // pointer array must be deleted
        if(Tens != NULL) delete[] Tens;
        if(TMag != NULL) delete[] TMag;
        if(DimSize != NULL) delete[] DimSize;
        if (spDist.size() != 0) {
                for (size_t i = 0; i < dimension; i++) {
                        if (spDist.at(i) != NULL) delete[] spDist.at(i);
                }
        }
}


//=================================
#endif
//=================================
