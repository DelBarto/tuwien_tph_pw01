// includes
#include "tensor.cpp"
// variables

// functions
struct TensorNet {
        std::vector<Tensor*> tensors;
        unsigned int **network=NULL;
        size_t nbrTensors;
        size_t nbrIndices;

        TensorNet();
        TensorNet(size_t nbrT, size_t nbrI, unsigned int **Tnet, std::vector<Tensor*> tens);
        ~TensorNet();

        void setTnet(size_t nbrT, size_t nbrI, unsigned int **Tnet);
        void setTlist(std::vector<Tensor*> tens);
        unsigned int getNetwork(size_t tens, size_t ind);
        double calcVal();
        double calcExpVal();
        double sampleTNet(int* id_order, int* choose_uni);

};

TensorNet::TensorNet(){
}

TensorNet::TensorNet(size_t nbrT, size_t nbrI, unsigned int **Tnet, std::vector<Tensor*> tens){
        setTnet(nbrT, nbrI, Tnet);
        setTlist(tens);
}

TensorNet::~TensorNet(){
        if (network != NULL) {
                for (size_t i = 0; i < nbrTensors; i++) {
                        delete[] network[i];
                }
                delete[] network;
        }
        for (size_t i = 0; i < tensors.size(); i++) {
                delete tensors.at(i);
        }
}
