#include <iostream>
#include <list>
#include <string>
using namespace std;


struct ao {
        int *arr;
        ao(size_t dim){
                arr = new int[dim];
        }
        ao(int array[]){
                arr = array;
        }
        void setArr(int array[]){
                arr = array;
        }
};


int main(int argc, char const *argv[]) {
        int arr[3];
        arr[0]=1;
        arr[1]=2;
        arr[2]=3;

        ao obj = ao(arr);
        obj.setArr(arr);

        std::cout << obj.arr[1] << '\n';
}
