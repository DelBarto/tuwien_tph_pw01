
#include "../tensor.h"
#include <iostream>
#include <list>
#include <string>
using namespace std;


int main(int argc, char const *argv[]) {
        Tensor t = Tensor('C');
        int dims[3] = {500,30,30};
        t.setDimensions(dims, sizeof(dims)/sizeof(*dims));

        int idx[3] = {100,10,5};
        int testPos = t.Ten2Arr(idx, false);
        std::cout << idx[0] << ", " << idx[1] << ", "  << idx[2] << '\n';
        std::cout << testPos << '\n';
        std::cout << testPos%500 << '\n';
        int *testTen = t.Arr2Ten(testPos);
        std::cout << testTen[0] << ", " << testTen[1] << ", " << testTen[2] << ", " << testTen[3] << '\n';
}
