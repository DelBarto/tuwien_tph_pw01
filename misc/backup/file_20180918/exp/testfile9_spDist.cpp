#include <iostream>
#include <vector>
#include <string>
#include <sstream>

#include "../network.cpp"



int main() {
        size_t nTensors = 4;
        size_t nIndices = 6;

        // E= \sum_{i,j,a,b,g,f} C(g,a,i) * CONJG( C(g,b,j) ) * C(f,b,j) * CONJG( C(f,a,i) )
        //                                 g,a,i,f,b,j
        int netw[nTensors][nIndices] =  { {1,2,3,0,0,0},    //C(g,a,i)
                                          {1,0,0,0,2,3},    //CONJG( C(g,b,j) )
                                          {0,0,0,1,2,3},    //C(f,b,j)
                                          {0,2,3,1,0,0} };  //CONJG( C(f,a,i))

        unsigned int **net;
        net = new unsigned int*[nTensors];
        for (size_t i = 0; i < nTensors; i++) {
                net[i] = new unsigned int[nIndices];
                for (size_t j = 0; j < nIndices; j++) {
                        net[i][j]=netw[i][j];
                }
        }
        Tensor C("C", "../TenNet_1/FTODDUMP_MC",3, false);
        Tensor C1("C1", "../TenNet_1/FTODDUMP_MC",3, true);

/*       // Arr2Dist and Dist2Arr test
        int idx[] = {10,2,1};
        int DS[] = {32,534,32};
        int pos = C.Dist2Arr(idx,DS);
        std::cout << pos << '\n';

        int* idx_back = new int[C.dimension];
        C.Arr2Dist(pos, idx_back, DS);

        for (size_t i = 0; i < C.dimension; i++) {
                std::cout << "[" << idx_back[i] << "]";
        }
        std::cout << '\n';
 */

        // std::cout << "Complex: " << (C.isComplexConj ? "true" : "false") << "    Val:" << C.get(idx, true) << '\n';
        // std::cout << "Complex: " << (C1.isComplexConj ? "true" : "false") << "    Val:" << C1.get(idx, true) << '\n';

        //
        //
        //
        C.genSpliceDistribution();
        int uniID[C.dimension-1] = {16,19};
        std::cout << C.getISampIndex(0,uniID) << '\n';



        // C.saveSliceDistribution(0);
        // C.saveSliceDistribution(1);
        // C.saveSliceDistribution(2);

        return 0;
}
