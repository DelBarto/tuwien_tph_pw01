#include <vector>
#include <iostream>

#include "network.h"

void TensorNet::setTnet(size_t nbrT, size_t nbrI, unsigned int **Tnet){
        nbrTensors = nbrT;
        tensors.resize(nbrTensors);
        nbrIndices = nbrI;
        network = Tnet;
}

void TensorNet::setTlist(std::vector<Tensor*> tens){
        tensors = tens;
}

unsigned int TensorNet::getNetwork(size_t tens, size_t idx){
        return network[tens][idx];
}

double TensorNet::calcVal(){
        size_t maxi = 1; // one could ask why not unsigned long int -> reason long stores two chunks of 16 bit where as size_t does not do that therefore it is more efficient to read in a size_t variable from the storage
        size_t maxID[nbrIndices];
        size_t indices[nbrIndices];

        double Tsum_real=0;
        double Tsum_compelx=0;

        size_t indicesCounter = 0;
        for (size_t j = 0; j < nbrIndices; j++) {
                indices[j]=1;
                for (size_t k = 0; k < nbrTensors; k++) {
                        if(network[k][j] != 0) {
                                maxID[indicesCounter] = tensors.at(k)->DimSize[getNetwork(k,j)-1];
                                std::cout << "/* " << k << " "<<  j <<" */" << " " << indicesCounter << ": " << maxID[indicesCounter]  << '\n';
                                maxi  *= maxID[indicesCounter];
                                indicesCounter++;
                                break;
                        }
                }
        }

        std::cout << "maxi: " << maxi<< '\n';
        std::cout << "maxID: " << "[" << maxID[0] << "]" << "[" << maxID[1] << "]";
        std::cout << "[" << maxID[2] << "]" << "[" << maxID[3] << "]" << "[" << maxID[4] << "]" << "[" << maxID[5] << "] \n";


        // Sum over all indices [i][j][a][b][g][f] like SUM_{i,j,a,b,g,f} C(g,a,i) * CONJG( C(g,b,j) ) * C(f,b,j) * CONJG( C(f,a,i) )
        for (size_t i = 0; i < maxi; i++) {

                // multiply Tensors for one indices combination [i][j][a][b][g][f] like C(g,a,i) * CONJG( C(g,b,j) ) * C(f,b,j) * CONJG( C(f,a,i) )
                double sumpart_real = 1;
                double sumpart_complex = 1;
                for (size_t j = 0; j < nbrTensors; j++) {
                        int idx[tensors.at(j)->dimension];

                        // generate indices to grab the Tensor entry
                        for (size_t k = 0; k < nbrIndices; k++) {
                                if (network[j][k] != 0) {
                                        idx[network[j][k]-1] = indices[k];
                                }
                        }

                        // multyply tensors for one part of the sum
                        sumpart_real *= tensors.at(j)->get(idx,0);
                        sumpart_complex *= tensors.at(j)->get(idx,1);
                }
                // add part of the sum to the sum
                Tsum_real += sumpart_real;
                Tsum_compelx += sumpart_complex;


                // print out ~1000 indices combinations
                if ((i%10000000==0) || ((i+100) > maxi)) {
                        std::cout << "[" << indices[0] << "]" << "[" << indices[1] << "]";
                        std::cout << "[" << indices[2] << "]" << "[" << indices[3] << "]" << "[" << indices[4] << "]" << "[" << indices[5] << "]  -->  ";
                        std::cout << Tsum_real << "  " << Tsum_compelx << "  -->  " << sumpart_real << "  " << sumpart_complex << " \n";
                }



                // increase indices, to loop through them
                for (size_t j = 0; j < nbrIndices; j++) {
                        if(indices[j]+1 <= maxID[j]) {
                                indices[j]++;
                                break;
                        }else{
                                indices[j] = 1;
                        }
                }

        }
        std::cout << "Tsum_real = "<< Tsum_real << "\n";
        std::cout << "Tsum_compelx = "<< Tsum_compelx << "\n";





        return 0.0;
}


double TensorNet::calcExpVal(){

}


double TensorNet::sampleTNet(int* id_order, int* tensor_order, int* choose_uni, int size_choose_uni){
        int choosen_id[nbrIndices];
        for (size_t i = 0; i < nbrIndices; i++) {
                choosen_id[i] = -1;
        }

        // TODO: choose indices which have to be choosen by uniform distibution
        int counter_choose_uni = 0;
        for (size_t i = 0; i < nbrTensors; i++) { // need tensor to get DimSize for choosing uniform distributed index
                for (size_t j = 0; j < nbrIndices; j++) { // loop over all indices for linking indices order to the indices of a tensor
                        if(network[i][j] != 0) { // link indices to tensor
                                for (size_t k = 0; k < size_choose_uni; k++) { // look if indices has to be choosen uniformly
                                        if(choose_uni[k] == j) { //
                                                choosen_id[j] = tensors.at(i).getUniDistIndex(network[i][j]);
                                                counter_choose_uni++;
                                                if (counter_choose_uni == size_choose_uni) goto BREAK_OUT;
                                        }
                                }
                        }
                }
        }

BREAK_OUT:

// ANHAND VON WELCHEN TENSOR IMORTANCE SAMPLE ICH UND WIRD ZUSÄTLICH AUCH DURCH DEN FAKTOR DIVIDIERT!!!!
        // TODO: importance sample getNetwork
        for (size_t i = 0; i < nbrTensors; i++) {
                int is_ID[tensors.at(i)];
                for (size_t j = 0; j < nbrIndices; j++) {
                        if (choosen_id[j] == -1) {
                                if(network[tesnsor_order[i]][j] != 0) {

                                }
                        }
                }
        }

        // TODO: calculate expected value - HIER NOCH DIE GEWICHTUNGSFAKTOREN BERÜCKSICHTIGEN!!!!
        double val[2];
        val[0] = 1;//real
        val[1] = 1;//complex
        for (size_t i = 0; i < nbrTensors; i++) {
                int tensor_ID[tensors.at(i).dimension];
                for (size_t j = 0; j < nbrIndices; j++) {
                        if(network[i][j] != 0) tensor_ID[network[i][j]] = ID[j];
                }
                val[0]*=tensors.at(i).get(tensor_ID,0);
                val[1]*=tensors.at(i).get(tensor_ID,1);

        }

        return val;
}


void TensorNet::IsampleID(int* id_order){

        return 0;
}
