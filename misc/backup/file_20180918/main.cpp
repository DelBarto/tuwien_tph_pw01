#include "network.h"
// #include "tensor.h"
// #include <iostream>
// #include <fstream>
#include <string>
#include <list>
#include <iterator>
// #include <cstdio>
// #include <cmath>


int main() {
        size_t nTensors = 4;
        size_t nIndices = 6;

        // E= \sum_{i,j,a,b,g,f} C(g,a,i) * CONJG( C(g,b,j) ) * C(f,b,j) * CONJG( C(f,a,i) )
        //                                 g,a,i,f,b,j
        int netw[nTensors][nIndices] =  { {1,2,3,0,0,0},   //C(g,a,i)
                                          {1,0,0,0,2,3},   //CONJG( C(g,b,j) )
                                          {0,0,0,1,2,3},   //C(f,b,j)
                                          {0,2,3,1,0,0} }; //CONJG( C(f,a,i))

        int **net;
        net = new int*[nTensors];
        for (size_t i = 0; i < nTensors; i++) {
                net[i] = new int[nIndices];
                for (size_t j = 0; j < nIndices; j++) {
                        net[i][j]=netw[i][j];
                }
        }

        int compcon[nTensors] = {0,1,0,1};

        std::list<Tensor*> Tlist;
        Tlist.push_back(new Tensor('C', "./TenNet_1/FTODDUMP_MC"));
        int idx[] = {60,6,10};
        Tlist.front()->printTenIdx(idx, 0, 0);

        char T2Net[nTensors] = {'C','C','C','C'};

        TensorNet Tnet = TensorNet(nTensors, nIndices, net, compcon, Tlist, T2Net);
        // TensorNet Tnet = TensorNet();
        // Tnet.setTnet(net,nTensors,nIndices);
        std::cout << "/* message */" << '\n';

        return 0;
}
