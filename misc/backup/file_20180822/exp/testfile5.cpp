
#include "../network.h"
#include <iostream>
#include <list>
#include <iterator>
#include <string>
using namespace std;


int main(int argc, char const *argv[]) {
        // std::list<Tensor*> Tlist;
        // std::cout << "/* message */" << '\n';
        // Tlist.push_back(new Tensor('C', "../TenNet_1/FTODDUMP_MC",3));
        // std::cout << "/* message */" << '\n';
        // Tlist.push_back(new Tensor('D', "../TenNet_1/FTODDUMP_MC",3));
        // Tlist.push_back(new Tensor('D', "../TenNet_1/FTODDUMP_MC",3));
        // std::cout << "/* message */" << '\n';
        Tensor *t1 = new Tensor("C", "../TenNet_1/FTODDUMP_MC",3);
        Tensor *t2 = new Tensor("D", "../TenNet_1/FTODDUMP_MC",3);
        std::list<Tensor*> Tlist;
        Tlist.push_back(t1);
        Tlist.push_back(t2);

        for (auto const& i : Tlist) {
                std::cout << i->id << '\n';
        }

        // for (std::list<Tensor*>::iterator it = Tlist.begin(); it != Tlist.end(); ++it) {
        //         std::cout << it->dimension << '\n';
        // }
}
