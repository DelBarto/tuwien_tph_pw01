//==================================
// include guard
#ifndef TENSOR_H   // if x.h hasn't been included yet...
#define TENSOR_H   //   #define this so the compiler knows it has been included
//==================================
#include <iostream>
#include <fstream>
#include <string>
#include <cstdio>
#include <cmath>

struct Tensor
{
        std::string filename="";
        std::string id="";
        float* Tens =NULL;
        std::size_t dimension=0;
        int *DimSize = NULL;

        Tensor(std::string identifier);
        Tensor(std::string identifier, std::string fn, int dim);
        ~Tensor();

        void setDimensions(int *DSize, int dimension);
        int Ten2Arr(int *idx, bool comp);
        int* Arr2Ten(int pos);
        void file2tensor(std::string fn);
        float get(int *idx, bool comp,bool compcon);
        float mag(int *idx);
        void printTenIdx(int *idx, bool comp, bool compcon);

};

//=================================
#endif
//=================================


Tensor::Tensor(std::string identifier){
        id = identifier;
        dimension = 0;
}

Tensor::Tensor(std::string identifier, std::string fn, int dim){
        dimension = dim;
        DimSize = new int[dimension];
        file2tensor(fn);
        id = identifier;
}

Tensor::~Tensor(){
        // pointer array must be deleted
        if(Tens != NULL) delete[] Tens;
        if(DimSize != NULL) delete[] DimSize;
}

void Tensor::setDimensions(int *DSize, int dim){
        dimension = dim;
        DimSize = new int[dimension];
        for (size_t i = 0; i < dimension; i++) {
                DimSize[i] = DSize[i];
        }
}

int Tensor::Ten2Arr(int *idx, bool comp){ // dimension has to be passed too because arrays decay to pointers when passed as function parameters
        int index[dimension];
        for (std::size_t i = 0; i < dimension; i++) {
                index[i] = idx[i] - 1;
        }

        int pos = 0;
        int shift = 1;
        // 3D: x+DimSize[0]*y+DimSize[0]*DimSize[1]*z+DimSize[0]*DimSize[1]*DimSize[2]*comp;
        for (std::size_t i = 0; i < dimension; i++) {
                pos += shift*index[i];
                shift *= DimSize[i];
        }
        pos += shift*(comp ? 1 : 0); //complex shift
        return pos;
}

int* Tensor::Arr2Ten(int pos){
        int *idx = new int[dimension+1];
        idx[0] = pos%DimSize[0];
        std::cout << idx[0] << '\n';
        int mod = DimSize[0];

        for (size_t i = 1; i < dimension; i++) {
                idx[i] = (pos%(mod*DimSize[i])-pos%mod)/mod;
                mod *= DimSize[i];
        }

        idx[dimension] = (int) pos/mod;
        for (size_t i = 0; i < dimension; i++) {
                idx[i] += 1;
        }
        return idx;
}

void Tensor::file2tensor(std::string fn){
        filename = fn;
        std::ifstream in;
        in.open(filename);

        if(!in.is_open())
        {
                std::cout << "READ went wrong!!!";
                return;
        }

        float** inData;
        int** index;
        if (!in) {
                std::cout << "Cannot open file.\n";
                return;
        }

        //================ has to be changed for dynamic dimensions ==================
        std::string line;
        getline(in, line);
        // std::cout << line << "\n";
        DimSize = new int[3];
        in >> DimSize[0] >> DimSize[1] >> DimSize[2];
        // std::cout << DimSize[0] << "*" << DimSize[1] << "*" << DimSize[2] << " = "  << DimSize[0]*DimSize[1]*DimSize[2] << "\n";
        //==================================

        dimension = sizeof(DimSize)/sizeof(*DimSize);
        getline(in, line);
        // std::cout << line << "\n";
        getline(in, line);
        // std::cout << line << "\n";

        DimSize = new int[dimension];

        std::size_t size_a = 1;
        for (std::size_t i = 0; i < dimension; i++) size_a*=DimSize[i];
        // std::cout << size_a << "\n";

        //================ read in Tensor ================
        inData = new float*[size_a];
        index = new int*[size_a];
        for (std::size_t i = 0; i < size_a; i++) {
                inData[i] = new float[2];
                index[i] = new int[3];
                in >> inData[i][0] >> inData[i][1] >> index[i][0] >> index[i][1] >> index[i][2];
        }
        in.close();
        //=================================


        //================ format Tensor ================
        Tens = new float[size_a*2];
        for (std::size_t i = 0; i < size_a; i++) {
                Tens[Ten2Arr(index[i],false)] = inData[i][0];
                Tens[Ten2Arr(index[i],true)] = inData[i][1];
        }
        //================================


        //Free each sub-array
        for(std::size_t i = 0; i < size_a; i++) {
                delete[] inData[i];
                delete[] index[i];
        }

        //Free the array of pointers
        delete [] DimSize;
        delete [] inData;
        delete [] index;
}

float Tensor::get(int *idx, bool comp,bool compcon){
        return Tens[Ten2Arr(idx,comp)]*(compcon ? -1 : 1);
}

float Tensor::mag(int *idx){
        return pow(Tens[Ten2Arr(idx,false)],2) + pow(Tens[Ten2Arr(idx,true)],2);
}

void Tensor::printTenIdx(int *idx, bool comp, bool compcon){
        std::cout << "{x,y,z,comp} = {" << idx[0] << "," << idx[1] << "," << idx[2] << "," << comp << "} --> " << Tens[Ten2Arr(idx,comp)]*(compcon ? -1 : 1) << '\n';
        return;
}
