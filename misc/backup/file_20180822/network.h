// includes
#include "tensor.h"
#include <list>
#include <iterator>

// variables

// functions
struct TensorNet {
        std::list <Tensor*> tensors;
        int **network=NULL;
        int *compcon=NULL;
        char *T2Net=NULL;
        size_t nbrTensors;
        size_t nbrIndices;

        TensorNet();
        TensorNet(size_t nbrT, size_t nbrI, int **Tnet, int *cc, std::list<Tensor*>& tens, char *T2N);
        ~TensorNet();

        void setTnet(size_t nbrT, size_t nbrI, int **Tnet);
        void setCompCon(int *cc);
        void setT2N(char *T2N);
        void setTlistn(std::list<Tensor*>& tens);

        Tensor* mulitplyTensors(std::list<Tensor*>& tens, std::size_t SumID[], std::size_t compConTen[], std::size_t nbr);

};

TensorNet::TensorNet(){
}

TensorNet::TensorNet(size_t nbrT, size_t nbrI, int **Tnet, int *cc, std::list<Tensor*>& tens, char *T2N){
        nbrTensors = nbrT;
        nbrIndices = nbrI;
        network = Tnet;
        compcon = cc;
        tensors = tens;
        T2Net = T2N;
}

TensorNet::~TensorNet(){
        if (network != NULL) {
                for (size_t i = 0; i < nbrTensors; i++) {
                        delete[] network[i];
                }
                delete[] network;
        }
        // if (compcon != NULL) delete[] compcon;
        // if (T2Net != NULL) delete[] T2Net;
        // for (std::list<Tensor*>::iterator it = tensors.begin(); it != tensors.end(); ++it) {
        //         delete it;
        // }
}

void TensorNet::setTnet(size_t nbrT, size_t nbrI, int **Tnet){
        nbrTensors = nbrT;
        nbrIndices = nbrI;
        network = Tnet;
}
void TensorNet::setCompCon(int *cc){
        compcon = cc;
}
void TensorNet::setT2N(char *T2N){
        T2Net = T2N;
}
void TensorNet::setTlistn(std::list<Tensor*>& tens){
        tensors = tens;
}

Tensor* TensorNet::mulitplyTensors(std::list<Tensor*>& tens, std::size_t SumID[], std::size_t compConTen[], std::size_t nbr){ //later change to a array/list of tensors
        //C_xz = Sum_y(A_xy * B_yz)

        std::string newID = "";
        for (auto const& it : tens) {
                newID += it->id;
        }
        Tensor *newT = new Tensor(newID);

        bool IDequal = true;
        //check if the summation dimension is equal
        for (std::size_t i = 0; i < nbr-1; i++) {
                if (SumID[i]!=SumID[i+1]) IDequal = false;
        }
        if (!IDequal) {
                std::cout << "Indices have not the same size!!!" << '\n';
                return;
        }

        // determine new Tensor dim
        for (auto const& it : tens) {
                newT->dimension += it->dimension;
        }
        newT->dimension -= nbr;

        // set new DimSize
        size_t counterNewDim=0; //dimension counter of the new Tensor
        size_t newD[newT->dimension]; //
        for (auto const& it : tens) {
                for (size_t i = 0; i < it->dimension; i++) {//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!check
                        if(i != SumID[counterNewDim]) {
                                newD[counterNewDim] = it->DimSize[i];
                        }
                }
                counterNewDim++;
        }

        // calc size for peudomultidim Tensor
        size_t size_a = 1;
        for (size_t i = 0; i < newT->dimension; i++) {
                size_a *= newD[i];
        }

        // intialize peudomultidim Tensor
        newT->Tens = new float[size_a];

        // calc new Tensor entries
        for (std::size_t j = 0; j < size_a; j++) {
                // for every indices

                int *idx = newT->Arr2Ten(j);

                // summation for one tensorentry
                newT->Tens[j] = 0;
                for (std::size_t i = 0; i < tens.front()->DimSize[SumID[0]]; i++) {
                        float multiTens = 1;

                        int newID2oldIDMappingCounter = 0; // counter which maps the index of the new Tensor to the old one
                        int TensorListCounter = 0; // increses by 1 for every Tensor in list, to know the position of the SumID
                        for (auto const& it : tens) {

                                //build array to get the entries of the old tensors (depending on the tensor)
                                int oldIdx[it->dimension];
                                for (size_t k = 0; k < it->dimension; k++) {
                                        if (k == SumID[TensorListCounter]) {
                                                oldIdx[k] = i;
                                        }
                                        else {
                                                oldIdx[k] = idx[newID2oldIDMappingCounter];
                                        }
                                }
                                multiTens *= it->get(oldIdx,idx[newT->dimension],compConTen[TensorListCounter]);
                                TensorListCounter += 1;
                        }
                        newT->Tens[j]+=multiTens;
                }
        }

        return newT;
}
