#include <random>

struct Singelton {
        std::random_device rd;
        std::mt19937 gen;

        Singelton() {
                rd();
                gen(rd);
        }
}
