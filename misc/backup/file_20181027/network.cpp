#include "./network.h"

//
// TensorNet::TensorNet() :
//         gen(rd())
// {
// }

// TensorNet::TensorNet(const TensorNet &other) :
//         gen(rd()),
//         tensors(other.tensors),
//         network(other.network),
//         nbrTensors(other.nbrTensors),
//         nbrIndices(other.nbrIndices)
// {
// }
//
// TensorNet::TensorNet(size_t nbrT, size_t nbrI, unsigned int **Tnet, std::vector<Tensor*> tens) :
//         gen(rd())
// {
//         setTnet(nbrT, nbrI, Tnet);
//         setTlist(tens);
// }

TensorNet::TensorNet() {
}

TensorNet::TensorNet(const TensorNet &other) :
        tensors(other.tensors),
        network(other.network),
        nbrTensors(other.nbrTensors),
        nbrIndices(other.nbrIndices)
{
}

TensorNet::TensorNet(size_t nbrT, size_t nbrI, unsigned int **Tnet, std::vector<Tensor*> tens) {
        setTnet(nbrT, nbrI, Tnet);
        setTlist(tens);
}

TensorNet::~TensorNet() {
        if (network != NULL) {
                for (size_t i = 0; i < nbrTensors; i++) {
                        delete[] network[i];
                }
                delete[] network;
        }
        for (size_t i = 0; i < tensors.size(); i++) {
                // delete tensors.at(i);
        }
}


void TensorNet::setTnet(size_t nbrT, size_t nbrI, unsigned int **Tnet) {
        nbrTensors = nbrT;
        tensors.resize(nbrTensors);
        nbrIndices = nbrI;
        network = Tnet;
}


void TensorNet::setTlist(std::vector<Tensor*> tens) {
        tensors = tens;
}


unsigned int TensorNet::getNetwork(size_t tens, size_t idx) {
        return network[tens][idx];
}


double TensorNet::calcVal() {
        size_t maxi = 1;  // one could ask why not unsigned long int -> reason long stores two chunks of 16 bit where as size_t does not do that therefore it is more efficient to read in a size_t variable from the storage
        size_t maxID[nbrIndices];
        size_t indices[nbrIndices];

        double Tsum_real = 0;
        double Tsum_compelx = 0;

        size_t indicesCounter = 0;
        for (int i = 0; i < static_cast<int>(nbrIndices); i++) {
                indices[i] = 1;
                for (size_t k = 0; k < nbrTensors; k++) {
                        if (network[k][i] != 0) {
                                maxID[indicesCounter] = tensors.at(k)->DimSize[getNetwork(k, i)-1];
                                // std::cout << "/* " << k << " "<<  i <<" */";
                                // std::cout << " " << indicesCounter << ": ";
                                // std::cout << maxID[indicesCounter]  << '\n';
                                maxi  *= maxID[indicesCounter];
                                indicesCounter++;
                                break;
                        }
                }
        }

        std::cout << "maxi: " << maxi<< '\n';
        std::cout << "maxID: " << "[" << maxID[0] << "]" << "[" << maxID[1] << "]";
        std::cout << "[" << maxID[2] << "]" << "[" << maxID[3] << "]" << "[" << maxID[4] << "]" << "[" << maxID[5] << "] \n";


        // Sum over all indices [i][j][a][b][g][f] like SUM_{i,j,a,b,g,f} C(g,a,i) * CONJG( C(g,b,j) ) * C(f,b,j) * CONJG( C(f,a,i) )
        for (size_t i = 0; i < maxi; i++) {
                // multiply Tensors for one indices combination [i][j][a][b][g][f] like C(g,a,i) * CONJG( C(g,b,j) ) * C(f,b,j) * CONJG( C(f,a,i) )
                double sumpart_real = 1;
                double sumpart_complex = 0;
                for (size_t j = 0; j < nbrTensors; j++) {
                        int idx[tensors.at(j)->dimension];

                        // generate indices to grab the Tensor entry
                        for (size_t k = 0; k < nbrIndices; k++) {
                                if (network[j][k] != 0) {
                                        idx[network[j][k]-1] = indices[k];
                                }
                        }
                        // std::cout << tensors.at(j)->identification << ": " << "{ ";
                        // for (size_t l = 0; l < tensors.at(j)->dimension; l++) {
                        //         std::cout << idx[l] << " ";
                        // }

                        // multyply tensors for one part of the sum
                        double temp_sumpart_real = sumpart_real;
                        double temp_sumpart_complex = sumpart_complex;

                        sumpart_real = temp_sumpart_real*tensors.at(j)->get(idx, 0) - temp_sumpart_complex*tensors.at(j)->get(idx, 1);
                        sumpart_complex = temp_sumpart_real*tensors.at(j)->get(idx, 1) + temp_sumpart_complex*tensors.at(j)->get(idx, 0);

                        // std::cout << "}   " << tensors.at(j)->get(idx, 0) << "   " << tensors.at(j)->get(idx, 1) << '\n';
                }
                // add part of the sum to the sum
                Tsum_real += sumpart_real;
                Tsum_compelx += sumpart_complex;

                // std::cout << "{" << indices[0] << "," << indices[1] << "," << indices[2] << "," << indices[3] << "," << indices[4] << "," << indices[5] << "} " << sumpart_real << " " << sumpart_complex << '\n';

                // print out ~1000 indices combinations
                // if ((i%50000000 == 0) || ((i+100) > maxi)) {
                //         std::cout << "[" << indices[0] << "]" << "[" << indices[1] << "]";
                //         std::cout << "[" << indices[2] << "]" << "[" << indices[3] << "]" << "[" << indices[4] << "]" << "[" << indices[5] << "]  -->  ";
                //         std::cout << Tsum_real << "  " << Tsum_compelx << "  -->  " << sumpart_real << "  " << sumpart_complex << " \n";
                // }
                // std::cout << sumpart_real << "   " << sumpart_complex << '\n';


                // increase indices, to loop through them
                for (size_t j = 0; j < nbrIndices; j++) {
                        if (indices[j]+1 <= maxID[j]) {
                                indices[j]++;
                                break;
                        } else {
                                indices[j] = 1;
                        }
                }
        }

        std::cout << "Tsum_real = "<< Tsum_real << "\n";
        std::cout << "Tsum_compelx = "<< Tsum_compelx << "\n";

        return 0.0;
}


double TensorNet::calcExpVal() {
        return 0.0;
}


// double *TensorNet::sampleTNet(int* id_order, int* tensor_order, int* isample, std::size_t size_isample) {
void TensorNet::sampleTNet(int* tensor_order, int* isample, std::size_t size_isample) {
        std::cout << "----- 0 ";
        int choosen_id[nbrIndices];
        double weight[nbrTensors];

        for (size_t i = 0; i < nbrIndices; i++) {
                choosen_id[i] = -1;
        }

        for (size_t i = 0; i < nbrTensors; i++) {
                weight[i] = 1;
        }
        std::cout << "1 ";
        // choose uniform distibuted indices
        for (int *t = tensor_order; t < tensor_order+nbrTensors; t++) {  // search tensor to sample inidces from
                for (int i = 0; i < static_cast<int>(nbrIndices); i++) {  // loop over all indices with are going to be uniform sampled
                        bool is_i_in_issample = false;
                        for (int *is = isample; is < isample+size_isample; is++) {
                                if (i == *is) {
                                        is_i_in_issample = true;
                                }
                        }
                        if (!is_i_in_issample && network[*t][i] != 0) {
                                choosen_id[i] = tensors.at(*t)->getUniDistIndex(network[*t][i]-1);
                        }
                }
        }
        std::cout << "2 ";
        std::cout << "--> {" << choosen_id[0] << "," << choosen_id[1] << "," << choosen_id[2];
        std::cout << "," << choosen_id[3] << "," << choosen_id[4] << "," << choosen_id[5] << "}" << '\n';


        // call function for importance sampling
        for (int *i = isample; i < isample+size_isample; i++) {
                for (int *t = tensor_order; t < tensor_order+nbrTensors; t++) {
                        if (choosen_id[*i] == -1 && network[*t][*i] != 0) {
                                int *sampledTensor_id = new int(tensors.at(*t)->dimension);  // the to sampled id is ignored
                                for (size_t id = 0; id < nbrIndices; id++) {  // get indices for importance sampling
                                        if (*i != static_cast<int>(id) && network[*t][id] != 0) {
                                                sampledTensor_id[network[*t][id]-1] = choosen_id[id];
                                        }
                                }

                                choosen_id[*i] = tensors.at(*t)->getISampIndex(network[*t][*i]-1, sampledTensor_id);

                                sampledTensor_id[network[*t][*i]-1] = choosen_id[*i];
                                weight[*t] *= tensors.at(*t)->getISampProb(network[*t][*i]-1, sampledTensor_id);
                                delete[] sampledTensor_id;
                                break;
                        }
                }
        }
        std::cout << "3 ";

        // std::cout << "{ ";
        // for (size_t i = 0; i < nbrIndices; i++) {
        //         std::cout << choosen_id[i] << " ";
        // }
        // std::cout << "}" << '\n';
        // std::cout << "Weight: { ";
        // for (size_t i = 0; i < nbrTensors; i++) {
        //         std::cout << weight[i] << " ";
        // }
        // std::cout << "}" << '\n';


        // TODO(me): calculate expected value - HIER NOCH DIE GEWICHTUNGSFAKTOREN BERÜCKSICHTIGEN!!!!
        double *T_part = new double(2);
        T_part[0] = 1;
        T_part[1] = 0;
        for (int t = 0; t < static_cast<int>(nbrTensors); t++) {
                int *sampledTensor_id = new int(tensors.at(t)->dimension);
                for (size_t i = 0; i < nbrIndices; i++) {
                        if (network[t][i] != 0) {
                                sampledTensor_id[network[t][i]] = choosen_id[i];
                        }
                }

                double temp_real = T_part[0];
                double temp_complex = T_part[1];
                T_part[0] = temp_real*tensors.at(t)->get(sampledTensor_id, 0) - temp_complex*tensors.at(t)->get(sampledTensor_id, 1);
                T_part[1] = temp_real*tensors.at(t)->get(sampledTensor_id, 1) + temp_complex*tensors.at(t)->get(sampledTensor_id, 0);

                delete[] sampledTensor_id;
                // std::cout << "weight: " << weight[t] << '\n';
                // std::cout << "bevor: " << T_part[0] << "   " << T_part[1] << '\n';
                T_part[0] /= weight[t];
                T_part[1] /= weight[t];
                // std::cout << "after: " << T_part[0] << "   " << T_part[1] << '\n';
        }
        std::cout << "T_part: "<< T_part[0] << "   " << T_part[1] << '\n';

        // return T_part;
        lastSampledValue[0] = T_part[0];
        lastSampledValue[1] = T_part[1];

        return;
}


void TensorNet::IsampleID() {
        return;
}
