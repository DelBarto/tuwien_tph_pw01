#include <iostream>
#include <vector>
#include <string>
#include <sstream>

#include "../network.cpp"



int main() {
        size_t nTensors = 4;
        size_t nIndices = 6;

        // E= \sum_{i,j,a,b,g,f} C(g,a,i) * CONJG( C(g,b,j) ) * C(f,b,j) * CONJG( C(f,a,i) )
        //                                 g, a, i, f, b, j
        int netw[nTensors][nIndices] = { {1, 2, 3, 0, 0, 0},  // C(g,a,i)
                                         {1, 0, 0, 0, 2, 3},  // CON(C(g,b,j))
                                         {0, 0, 0, 1, 2, 3},  // C(f,b,j)
                                         {0, 2, 3, 1, 0, 0} };  // CON(C(f,a,i))

        unsigned int **net;
        net = new unsigned int*[nTensors];
        for (size_t i = 0; i < nTensors; i++) {
                net[i] = new unsigned int[nIndices];
                for (size_t j = 0; j < nIndices; j++) {
                        net[i][j] = netw[i][j];
                }
        }



        return 0;
}
