

#include <iostream>
#include <vector>
#include <string>
#include <sstream>



int main() {
        size_t nbrTensors = 4;
        size_t nbrIndices = 6;
        int net[nbrTensors][nbrIndices] =  { {1,2,3,0,0,0},   //C(g,a,i)
                                             {1,0,0,0,2,3},  //CONJG( C(g,b,j) )
                                             {0,0,0,1,2,3},  //C(f,b,j)
                                             {0,2,3,1,0,0} };  //CONJG( C(f,a,i))
        size_t tensors[nbrTensors] = {3,3,3,3};
        size_t DimSize[3] = {547,32,32};
        size_t maxi = 1;                     // one could ask why not unsigned long int -> reason long stores two chunks of 16 bit where as size_t does not do that therefore it is more efficient to read in a size_t variable from the storage
        size_t maxID[nbrIndices];
        size_t indices[nbrIndices];
        size_t indicesCounter = 0;

        // for (size_t j = 0; j < nbrTensors; j++) {
        //         for (size_t k = 0; k < tensors[j]; k++) {
        //                 // maxID[indicesCounter] = DimSize[k];
        //                 // maxi *= DimSize[k];
        //                 // indices[indicesCounter] = 1;
        //                 indicesCounter++;
        //                 std::cout << "/* " << j << " "<< k << " */" << " " << indicesCounter << '\n';
        //         }
        // }

        for (size_t j = 0; j < nbrIndices; j++) {
                for (size_t k = 0; k < nbrTensors; k++) {
                        if(net[k][j] != 0) {
                                maxID[indicesCounter] = DimSize[net[k][j]-1];
                                std::cout << "/* " << k << " "<<  j <<" */" << " " << indicesCounter << ": " << maxID[indicesCounter]  << '\n';
                                indicesCounter++;
                                break;
                        }
                }
        }


        // for (size_t i = 0; i < maxi; i++) {
        //
        //         for (size_t j = 0; j < nbrIndices; j++) {
        //                 if(indices[j]+1 < maxID[j]) {
        //                         indices[j]++;
        //                         break;
        //                 }
        //         }
        //         std::cout << indices[0] << " " << indices[1] << " " << indices[2] << " " << indices[3] << " " << indices[4] << " " << indices[5] << '\n';
        // }
}
