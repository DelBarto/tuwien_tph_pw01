#include <iostream>
#include <random>



int main() {
        std::random_device rd;
        std::default_random_engine gen(rd());
        std::uniform_real_distribution<double> double_uni_Dist(0.0, 1.0);
        std::uniform_int_distribution<int> int_uni_dist(0, 10);
        std::cout << double_uni_Dist(gen) << '\n';
        std::cout << int_uni_dist(gen) << '\n';
}
