#include <iostream>
#include <vector>
#include <random>
#include "./randomGen.h"


std::random_device seed_generator;
unsigned seed;
std::mt19937 generator;
std::uniform_real_distribution<double> uni_real_Dist;



void printOut(const int a) {
        std::cout << a << '\n';
}

int main() {
        // Gen g();
        std::cout << uni_real_Dist(generator) << '\n';

        return 0;
}
