#include "./tensor.h"

Tensor::Tensor() {
}


Tensor::Tensor(std::string identifier, bool isCC) {
        identification = identifier;
        isComplexConj = isCC;
        dimension = 0;
}


Tensor::Tensor(std::string identifier, std::string fn, int dim, bool isCC) {
        isComplexConj = isCC;
        identification = identifier;
        setDimension(dim);
        file2tensor(fn);
}


Tensor::~Tensor() {
        // pointer array must be deleted
        if (Tens != NULL) delete[] Tens;
        if (TMag != NULL) delete[] TMag;
        if (DimSize != NULL) delete[] DimSize;
        if (spDist.size() != 0) {
                for (size_t i = 0; i < dimension; i++) {
                        if (spDist.at(i) != NULL) delete[] spDist.at(i);
                }
        }
}


void Tensor::setDimProbaties(int *DSize, int dim) {
        setDimension(dim);
        setDimSize(DSize);

        size_TMag = 1;
        for (size_t j = 0; j < dimension; j++) {
                size_TMag *= DimSize[j];
        }
        Tens = new double[size_TMag*2];
        TMag = new double[size_TMag];
        for (size_t i = 0; i < dimension; i++) {
                spDist.at(i) = new double[size_TMag];
                for (size_t j = 0; j < size_TMag; j++) {
                        spDist.at(i)[size_TMag] = 0;
                }
        }
}


void Tensor::setDimension(int dim) {
        dimension = dim;
        spDist.resize(dimension);
}


void Tensor::setDimSize(int *DSize) {
        DimSize = new int[dimension];
        cacheIndices = new int[dimension];

        uniInt.resize(dimension);
        for (size_t i = 0; i < dimension; i++) {
                DimSize[i] = DSize[i];
                uniInt.at(i) = new std::uniform_int_distribution<int>(0, DimSize[i]-1);
        }
}


int Tensor::Ten2Arr(int *idx, bool comp) {
        // dimension has to be passed too because arrays decay to pointers when passed as function parameters
        int pos = 0;
        int shift = 1;
        // 3D: x+DimSize[0]*y+DimSize[0]*DimSize[1]*z+DimSize[0]*DimSize[1]*DimSize[2]*comp;
        for (std::size_t i = 0; i < dimension; i++) {
                pos += shift*idx[i];
                shift *= DimSize[i];
        }
        pos += shift*(comp ? 1 : 0);  // complex shift
        return pos;
}


int* Tensor::Arr2Ten(int pos) {
        int *idx = new int[dimension+1];
        idx[0] = pos%DimSize[0];
        // std::cout << idx[0] << '\n';
        int mod = DimSize[0];

        for (size_t i = 1; i < dimension; i++) {
                idx[i] = (pos%(mod*DimSize[i])-pos%mod)/mod;
                mod *= DimSize[i];
        }

        idx[dimension] = static_cast<int>(pos/mod);

        return idx;
}


void Tensor::setEntry(int *idx, bool complex, double val) {
        Tens[Ten2Arr(idx, complex)] = val;
}


void Tensor::file2tensor(std::string fn) {
        filename = fn;
        std::ifstream in;
        in.open(filename.c_str());
        if (!in.is_open()) {
                std::cout << "READ went wrong!!!" << "\n";
                return;
        }

        double** inData;
        int** index;
        if (!in) {
                std::cout << "Cannot open file.\n";
                return;
        }

        //============= has to be changed for dynamic dimensions =============
        std::string line;
        getline(in, line);
        // std::cout << line << "\n";
        // vector can be resized and dynamicly changed !!!!!!
        int *DimensionSize = new int[3];
        in >> DimensionSize[0] >> DimensionSize[1] >> DimensionSize[2];
        int dim = sizeof(DimensionSize)/sizeof(*DimensionSize)+1;
        //====================================================================
        setDimProbaties(DimensionSize, dim);

        getline(in, line);
        // std::cout << line << "\n";
        getline(in, line);
        // std::cout << line << "\n";

        std::size_t size_a = 1;
        for (std::size_t i = 0; i < dimension; i++) size_a*=DimSize[i];
        // std::cout << size_a << "\n";

        //================ read in Tensor ================
        inData = new double*[size_a];
        index = new int*[size_a];
        for (std::size_t i = 0; i < size_a; i++) {
                inData[i] = new double[2];
                index[i] = new int[3];
                in >> inData[i][0] >> inData[i][1] >> index[i][0];
                in >> index[i][1] >> index[i][2];
        }
        in.close();
        //================================================

        //================ format Tensor ================
        for (std::size_t i = 0; i < size_a; i++) {
                Tens[Ten2Arr(index[i], false)] = inData[i][0];
                Tens[Ten2Arr(index[i], true)] = inData[i][1]*(isComplexConj ? -1 : 1);
        }
        //===============================================

        // Free each sub-array
        for (std::size_t i = 0; i < size_a; i++) {
                delete[] inData[i];
                delete[] index[i];
        }

        // Free the array of pointers
        delete [] DimensionSize;
        delete [] inData;
        delete [] index;

        genSpliceDistribution();
}


double Tensor::get(int *idx, bool complex) {
        return Tens[Ten2Arr(idx, complex)];
}


double Tensor::mag(int *idx) {
        return sqrt(pow(Tens[Ten2Arr(idx, false)], 2) + pow(Tens[Ten2Arr(idx, true)], 2));
}


void Tensor::printTenIdx(int *idx, bool comp) {
        std::cout << "{x,y,z,comp} = {" << idx[0] << "," << idx[1] << ",";
        std::cout << idx[2] << "," << comp << "} --> ";
        std::cout << Tens[Ten2Arr(idx, comp)] << '\n';
        return;
}


void Tensor::Arr2Dist(int pos, int* idx, int* DimensionSize) {
        idx[0] = pos%DimensionSize[0];
        int mod = DimensionSize[0];
        std::cout << dimension << '\n';
        for (size_t i = 1; i < dimension; i++) {
                std::cout << idx[i] << '\n';
                idx[i] = (pos%(mod*DimensionSize[i])-pos%mod)/mod;
                mod *= DimensionSize[i];
        }
}


int Tensor::Dist2Arr(int* idx, int* DimensionSize) {
        int pos = 0;
        int multiplier = 1;
        for (size_t i = 0; i < dimension; i++) {
                pos += multiplier*idx[i];
                multiplier *= DimensionSize[i];
        }
        return pos;
}


void Tensor::saveSliceDistribution(int id) {
        std::ofstream myfile;
        std::ostringstream Dist_fname;
        Dist_fname << "dist_" << identification << "_" << id << ".dat";
        myfile.open(Dist_fname.str());

        int new_DimSize[dimension];
        new_DimSize[0] = DimSize[id];
        for (size_t i = 0; i < dimension; i++) {
                new_DimSize[i+1] = DimSize[(i < (size_t) id ? i : i+1)];
        }

        int new_idx[dimension];
        for (size_t i = 0; i < dimension; i++) {
                new_idx[i] = 0;
        }


        for (size_t i = 0; i < size_TMag; i++) {
                Arr2Dist(i, new_idx, new_DimSize);
                myfile << i << " ";
                int idx[dimension];
                idx[id] = new_idx[0];
                for (size_t j = 1; j < dimension; j++) {
                        idx[(j <= (size_t) id ? j-1 : j)] = new_idx[j];
                        myfile << idx[j-1] << " ";
                }
                myfile << idx[dimension-1] << " ";
                myfile << spDist.at(id)[i] << '\n';
                if ((i+1)%DimSize[id] == 0) {
                        myfile << '\n';
                }
        }

        myfile.close();
        std::cout << "spliceDist -> written" << '\n';
}


void Tensor::genSpliceDistribution() {
        std::cout << identification << "  ->  " << filename << '\n';
        for (size_t id = 0; id < dimension; id++) {
                std::cout << "<id: " << id << '\n';

                // rearranging size of dimensions
                int new_DimSize[dimension];
                new_DimSize[0] = DimSize[id];
                for (size_t i = 0; i < dimension; i++) {
                        new_DimSize[i+1] = DimSize[(i < (size_t) id ? i : i+1)];
                }

                // index array for splice distribution
                int new_idx[dimension];
                for (size_t i = 0; i < dimension; i++) {
                        new_idx[i] = 0;
                }

                // generating splice distribution
                double summation = 0;
                for (size_t i = 0; i < size_TMag; i++) {
                        // rearrang indices to original order
                        int idx[dimension];
                        idx[id] = new_idx[0];
                        for (size_t j = 1; j < dimension; j++) {
                                idx[(j <= id ? j-1 : j)] = new_idx[j];
                        }
                        summation += mag(idx);
                        spDist.at(id)[i] = summation;
                        if ((i+1)%new_DimSize[0] == 0) {
                                // normalize splice distribution
                                for (int k = 0; k < DimSize[id]; k++) {
                                        spDist.at(id)[i-k] /= summation;
                                }
                                summation = 0;
                        }

                        for (size_t j = 0; j < dimension; j++) {
                                if (new_idx[j]+1 < new_DimSize[j]) {
                                        new_idx[j]++;
                                        break;
                                } else {
                                        new_idx[j] = 0;
                                }
                        }
                }
                std::cout << "id: " << id  << ">" << '\n';
        }

}


// TODO: ES MUSS NOCH DIE WHS DIESEN PARAMETER ZU BEKOMMEN ZURÜCKGEGEBEN WERDEN --- also upper-lower in double[2]
int Tensor::getISampIndex(int rand_id, int* idx) {
        // std::cout << rand_id << ":  DimSize " << DimSize[rand_id] << '\n';

        int *new_DimSize = new int[dimension];
        new_DimSize[0] = DimSize[rand_id];
        for (size_t i = 1; i < dimension; i++) {
                new_DimSize[i] = DimSize[(i < (size_t) rand_id ? i-1 : i)];
        }

        int *new_idx = new int[dimension];
        new_idx[0] = 1;  // 1 to get the start of spDistArr
        for (size_t i = 1; i < dimension; i++) {
                new_idx[i] = idx[(i < (size_t) rand_id ? i-1 : i)];
        }

        // search index to probability
        int spDistArrStart = Dist2Arr(new_idx, new_DimSize)-1;
        double rand_uniform = Gen::uni_real_Dist(Gen::generator);
        // rand_uniform = 8.5108e-14;
        // rand_uniform = 0.9999995;
        // std::cout << rand_uniform << '\n';
        int upper = new_DimSize[0];
        int lower = 0;
        while (upper - lower > 1) {
                int mid = (upper+lower)/2;
                // std::cout << upper << "  -  " << mid << "  -  " << lower<< '\n';
                if (rand_uniform < spDist.at(rand_id)[spDistArrStart+mid]) {
                        upper = mid;
                } else {
                        lower = mid;
                }
        }
        return upper;
}


double Tensor::getISampProb(int rand_id, int* upper_idx) {
        // std::cout << "{" << upper_idx[0] << "," << upper_idx[1] << "," << upper_idx[2] << "}" << '\n';
        if (upper_idx[rand_id] == 1) {
                return spDist.at(rand_id)[Dist2Arr(upper_idx, DimSize)];
        } else {
                int *lower_idx = new int[dimension];
                for (size_t i = 0; i < dimension; i++) {
                        lower_idx[i] = upper_idx[i];
                }
                lower_idx[rand_id] -= 1;
                double prob = spDist.at(rand_id)[Dist2Arr(upper_idx, DimSize)] - spDist.at(rand_id)[Dist2Arr(lower_idx, DimSize)];
                delete[] lower_idx;
                return prob;
        }
}


int Tensor::getUniDistIndex(int index) {
        // std::uniform_int_distribution<int> uni_int_Dist(1, DimSize[index]);

        return (*uniInt.at(index))(Gen::generator);
}
