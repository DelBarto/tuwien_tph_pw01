# Fragen 20.09.2018

- [x] importance sampling mit welchen Tensor
- [x] Gewichtungsfaktor auch bei anderen tensoren aufmultiplizieren
- [x] berechneter Wert: 0.00672938  + i0.0255581 ?

# Erwartungswerte

## test1

{4,4,4,4}
1
V: E_real = 1167.14
V: E_comp = 0
dt = 0.00141
{8,8}
2
K: E_real = 1167.14
K: E_comp = 0
dt = 0.000248

C: E_real = 1167.14
C: E_comp = 3.8937e-15
dt = 0.010735

<E_real> = 0.07123657226

## test2

{4,4,4,4}
1
V: E_real = 459.039
V: E_comp = -0.17687
dt = 0.001241
{8,8}
2
K: E_real = 459.039
K: E_comp = -0.17687
dt = 0.000199

C: E_real = 459.039
C: E_comp = -0.17687
dt = 0.012825

<E_real> = 0.02801751708

## test3
erst Fehler im Datenfile danach neu generriert


{4,4,4,4}
1
V: E_real = 585.983
V: E_comp = 0.357549
dt = 0.001253

{8,8}
2
K: E_real = 585.983
K: E_comp = 0.357549
dt = 0.000222

C: E_real = 585.983
C: E_comp = 0.357549
dt = 0.016373

<E_real> = 0.03576556396

## test4

{4,4,4,4}
1
V: E_real = 1.46242e+07
V: E_comp = 0
dt = 0.001538

{8,8}
2
K: E_real = 1.46242e+07
K: E_comp = 0
dt = 0.000238

C: E_real = 1.46242e+07
C: E_comp = -3.25596e-10
dt = 0.011276

<E_real> = 892.590332031

## FTODDUMP_MC

{32,32,32,32}
1
V: E_real = 0.0586645
V: E_comp = 0
dt = 100.065

{534,534}
2
K: E_real = 0.0586645
K: E_comp = 0
dt = 78.6463

#TODO

- [x] uniform sampling konvergiert immer um berechneten Erwartungswert
- [ ] Importance sampling überprüfen da falsche ergenbisse geliefert werden
- [ ] `weight` überprüfen ob richtiges genommen wird
- [ ] Fehler suchen warum bei `FTODDUMP_MC` "random" segmentation fault Errors auftreten


# Fragen 18.10

- [ ] zur Überprüfung Tensor erzeugen und Verteilung der Inversion Methode ploten
- [ ] verwendeten Random Number Generator?
- [ ] Erwartungswerte (da komplex?)
\[ {\overline {X}}={\frac {1}{n}}\sum _{i=1}^{n}X_{i} \]
- [ ] Varianz (da komplex?)
\[ V_{n}^{*}(X)={\frac {1}{n-1}}\sum _{i=1}^{n}(X_{i}-{\overline {X}})^{2} \]
- [ ] "random" segmentation fault errors --> beim aufruf von `Tnet.sampleTNet(...)`
- [ ]
