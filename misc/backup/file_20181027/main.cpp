#include <iostream>
#include <vector>
#include <string>
#include <sstream>

// #include "./tensor.h"
#include "./network.h"



int main() {

        size_t nTensors = 4;
        size_t nIndices = 6;

        // E= \sum_{i,j,a,b,g,f} C(g,a,i) * CONJG( C(g,b,j) ) * C(f,b,j) * CONJG( C(f,a,i) )
        //                                 g, a, i, f, b, j
        int netw[nTensors][nIndices] =  { {1, 2, 3, 0, 0, 0},    // C(g,a,i)
                                          {1, 0, 0, 0, 2, 3},    // CONJG( C(g,b,j) )
                                          {0, 0, 0, 1, 2, 3},    // C(f,b,j)
                                          {0, 2, 3, 1, 0, 0} };  // CONJG( C(f,a,i))

        unsigned int **net;
        net = new unsigned int*[nTensors];
        for (size_t i = 0; i < nTensors; i++) {
                net[i] = new unsigned int[nIndices];
                for (size_t j = 0; j < nIndices; j++) {
                        net[i][j] = netw[i][j];
                }
        }

        std::vector<Tensor*> Tlist(nTensors);
        std::string filename = "./TenNet_1/test1";
        // filename = "./TenNet_1/FTODDUMP_MC";
        Tlist.at(0) = new Tensor("C1", filename, 3, false);
        Tlist.at(1) = new Tensor("C2", filename, 3, true);
        Tlist.at(2) = new Tensor("C3", filename, 3, false);
        Tlist.at(3) = new Tensor("C4", filename, 3, true);

        // TensorNet *Tnet =  new TensorNet(nTensors, nIndices, net, Tlist);
        TensorNet Tnet =  TensorNet(nTensors, nIndices, net, Tlist);
        // Tnet.calcVal();

        int tensor_ord[nTensors] = {0, 1, 2, 3};
        int nbr_is_idx = 0;
        int is_idx[nbr_is_idx] = {0};
        // int *tensor_ord = new int(nTensors);
        // tensor_ord[0] = 0;
        // tensor_ord[1] = 1;
        // tensor_ord[2] = 2;
        // tensor_ord[3] = 3;
        // int *indices_ord = new int(nIndices);
        // indices_ord[0] = 0;
        // indices_ord[1] = 1;
        // indices_ord[2] = 2;
        // indices_ord[3] = 3;
        // indices_ord[4] = 4;
        // indices_ord[5] = 5;
        // int nbr_is_idx = 0;
        // int *is_idx = new int(nbr_is_idx);
        // is_idx[0] = {0};

        int nbrOfSamples = 1000;
        double sampledValues[nbrOfSamples][2];

        // double *val;
        for (int i = 0; i < nbrOfSamples; i++) {

                Tnet.sampleTNet(tensor_ord, is_idx, nbr_is_idx);
                sampledValues[i][0] = Tnet.lastSampledValue[0];
                sampledValues[i][1] = Tnet.lastSampledValue[1];
        }

        /*

           // std::cout << "finisched sampling" << '\n';
           double val[2] = {0,0};
           for (int i = 0; i < nbrOfSamples; i++) {
                double variance = 0;
                val[0] += sampledValues[i][0];
                val[1] += sampledValues[i][1];

                // std::cout << '\n';
                for (int j = 0; j < i; j++) {
                        double tempVar = sampledValues[j][0] - val[0]/(i+1);
                        // std::cout << sampledValues[j][0] << " - " << val[0]/(i+1) << '\n';
                        variance += tempVar*tempVar;
                }

                std::cout << i << " " << val[0]/(i+1) << " " << val[1]/(i+1) << " " <<  variance/i<< '\n';
           }
         */





        return 0;
}
