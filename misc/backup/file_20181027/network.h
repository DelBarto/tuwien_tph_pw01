//==================================
// include guard
#ifndef FILE_NETWORK_H_  // if x.h hasn't been included yet...
#define FILE_NETWORK_H_  // #define this so the compiler knows it has been included
//==================================

// includes
#include <vector>
#include <iostream>
#include <random>
#include "./tensor.h"
#include "./randomGen.h"

// variables

// functions
struct TensorNet {
        std::vector<Tensor*> tensors;
        unsigned int **network = NULL;
        std::size_t nbrTensors;
        std::size_t nbrIndices;
        double lastSampledValue[2];

        TensorNet();
        TensorNet(const TensorNet &);
        TensorNet(size_t nbrT, size_t nbrI, unsigned int **Tnet, std::vector<Tensor*> tens);
        ~TensorNet();

        void setTnet(size_t nbrT, size_t nbrI, unsigned int **Tnet);
        void setTlist(std::vector<Tensor*> tens);
        unsigned int getNetwork(size_t tens, size_t ind);
        double calcVal();
        double calcExpVal();
        // double *sampleTNet(int* id_order, int* tensor_order, int* choose_uni, std::size_t size_choose_uni);
        void sampleTNet(int* tensor_order, int* choose_uni, std::size_t size_choose_uni);
        void IsampleID();
};


//=================================
#endif  // FILE_NETWORK_H_
//=================================
