//==================================
// include guard
#ifndef TENSOR_H   // if x.h hasn't been included yet...
#define TENSOR_H   //   #define this so the compiler knows it has been included
//==================================
#include <iostream>
#include <fstream>
#include <string>
#include <cstdio>
#include <cmath>

struct Tensor
{
        std::string filename="";
        std::string id="";
        float* Tens =NULL;
        std::size_t dimension=0;
        int *DimSize = NULL;
        // cache last Tens access
        int *cacheIndices;
        float cacheTens[2]; //real and complex Value
        bool isComplexConj = false;

        // std::vector<int*> spDist(0);


        Tensor();
        Tensor(std::string identifier, bool isCC);
        Tensor(std::string identifier, std::string fn, int dim, bool isCC);
        ~Tensor();

        void setDimProbaties(int *DSize, int dim);
        void setDimension(int dim);
        void setDimSize(int *DSize);
        int Ten2Arr(int *idx, bool comp);
        int* Arr2Ten(int pos);
        void file2tensor(std::string fn);
        float get(int *idx, bool complex);
        float mag(int *idx);
        void printTenIdx(int *idx, bool comp, bool compcon);
        void calcSpliceDistribution();

};

//=================================
#endif
//=================================

Tensor::Tensor(){
}

Tensor::Tensor(std::string identifier, bool isCC){
        id = identifier;
        isComplexConj = isCC;
        dimension = 0;
}

Tensor::Tensor(std::string identifier, std::string fn, int dim, bool isCC){
        setDimension(dim);
        file2tensor(fn);
        id = identifier;
        isComplexConj = isCC;
}

Tensor::~Tensor(){
        // pointer array must be deleted
        if(Tens != NULL) delete[] Tens;
        if(DimSize != NULL) delete[] DimSize;
        // for (size_t i = 0; i < dimension; i++) {
        //         if (spDist.at(i) != NULL) delete[] spDist.at(i);
        // }
}

void Tensor::setDimProbaties(int *DSize, int dim){
        setDimension(dim);
        setDimSize(DSize);
}
void Tensor::setDimension(int dim){
        dimension = dim;
}
void Tensor::setDimSize(int *DSize){
        DimSize = new int[dimension];
        cacheIndices = new int[dimension];
        for (size_t i = 0; i < dimension; i++) {
                DimSize[i] = DSize[i];
        }
}

int Tensor::Ten2Arr(int *idx, bool comp){ // dimension has to be passed too because arrays decay to pointers when passed as function parameters
        // bool check = true;
        // for (size_t i = 0; i < dimension; i++) {
        //         if(idx[i] > DimSize[i]) check = false;
        // }
        // if(check) {

        int index[dimension];
        for (std::size_t i = 0; i < dimension; i++) {
                index[i] = idx[i] - 1;
        }

        int pos = 0;
        int shift = 1;
        // 3D: x+DimSize[0]*y+DimSize[0]*DimSize[1]*z+DimSize[0]*DimSize[1]*DimSize[2]*comp;
        for (std::size_t i = 0; i < dimension; i++) {
                pos += shift*index[i];
                shift *= DimSize[i];
        }
        pos += shift*(comp ? 1 : 0);         //complex shift
        return pos;
        // }else{
        //         std::cout << "outside the Tensor boundries" << '\n';
        //         for (size_t i = 0; i < dimension; i++) {
        //                 std::cout << "["<< idx[i] << "]";
        //         }
        //         std::cout << "  not within  ";
        //         for (size_t i = 0; i < dimension; i++) {
        //                 std::cout << "["<< DimSize[i] << "]";
        //         }
        //         exit(EXIT_FAILURE);
        // }
}

int* Tensor::Arr2Ten(int pos){
        int *idx = new int[dimension+1];
        idx[0] = pos%DimSize[0];
        std::cout << idx[0] << '\n';
        int mod = DimSize[0];

        for (size_t i = 1; i < dimension; i++) {
                idx[i] = (pos%(mod*DimSize[i])-pos%mod)/mod;
                mod *= DimSize[i];
        }

        idx[dimension] = (int) pos/mod;
        for (size_t i = 0; i < dimension; i++) {
                idx[i] += 1;
        }
        return idx;
}

void Tensor::file2tensor(std::string fn){
        filename = fn;
        std::ifstream in;
        in.open(filename.c_str());
        if(!in.is_open())
        {
                std::cout << "READ went wrong!!!";
                return;
        }

        float** inData;
        int** index;
        if (!in) {
                std::cout << "Cannot open file.\n";
                return;
        }

        //================ has to be changed for dynamic dimensions ==================
        std::string line;
        getline(in, line);
        // std::cout << line << "\n";
        //vector can be resized and dynamicly changed !!!!!!
        int *DimensionSize = new int[3];
        in >> DimensionSize[0] >> DimensionSize[1] >> DimensionSize[2];
        int dim = sizeof(DimensionSize)/sizeof(*DimensionSize)+1;
        //==================================
        setDimProbaties(DimensionSize, dim);



        getline(in, line);
        // std::cout << line << "\n";
        getline(in, line);
        // std::cout << line << "\n";


        std::size_t size_a = 1;
        for (std::size_t i = 0; i < dimension; i++) size_a*=DimSize[i];
        // std::cout << size_a << "\n";


        //================ read in Tensor ================
        inData = new float*[size_a];
        index = new int*[size_a];
        for (std::size_t i = 0; i < size_a; i++) {
                inData[i] = new float[2];
                index[i] = new int[3];
                in >> inData[i][0] >> inData[i][1] >> index[i][0] >> index[i][1] >> index[i][2];
        }
        in.close();
        //=================================


        //================ format Tensor ================
        Tens = new float[size_a*2];
        for (std::size_t i = 0; i < size_a; i++) {
                Tens[Ten2Arr(index[i],false)] = inData[i][0];
                Tens[Ten2Arr(index[i],true)] = inData[i][1];
        }
        //================================


        //Free each sub-array
        for(std::size_t i = 0; i < size_a; i++) {
                delete[] inData[i];
                delete[] index[i];
        }


        //Free the array of pointers
        delete [] DimensionSize;
        delete [] inData;
        delete [] index;
}

// float Tensor::get(int *idx, bool complex){
//         bool cached = true;
//         for (size_t i = 0; i < dimension; i++) {
//                 if (idx[i] != cacheIndices[i]) cached = false;
//         }
//         if (!cached) {
//                 cacheTens[0] = Tens[Ten2Arr(idx,0)];
//                 cacheTens[1] = Tens[Ten2Arr(idx,1)];
//         }
//         return cacheTens[(complex ? 1 : 0)];
// }
float Tensor::get(int *idx, bool complex){
        return Tens[Ten2Arr(idx,complex)];
}


// float Tensor::get(int *idx, bool complex){
//         return Tens[Ten2Arr(idx,complex)];
// }

float Tensor::mag(int *idx){
        return pow(Tens[Ten2Arr(idx,false)],2) + pow(Tens[Ten2Arr(idx,true)],2);
}

void Tensor::printTenIdx(int *idx, bool comp, bool compcon){
        std::cout << "{x,y,z,comp} = {" << idx[0] << "," << idx[1] << "," << idx[2] << "," << comp << "} --> " << Tens[Ten2Arr(idx,comp)]*(compcon ? -1 : 1) << '\n';
        return;
}

void Tensor::calcSpliceDistribution(){
        // spDist.resize(dimension);
        //
        // std::size_t size_a = 1;
        // for (size_t i = 0; i < dimension; i++) {
        //         size_a = DimSize[i];
        // }
        // for (size_t i = 0; i < dimension; i++) {
        //         spDist.at(i) = new int[size_a];
        // }
        //
        // for (size_t d = 0; d < dimension; d++) {
        //         for (size_t i = 0; i < size_a; i++) {
        //                 spDist.at(d) = pow(get(/* hier weiter*/ ),2);
        //         }
        // }

}
